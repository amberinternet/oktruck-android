package com.amberinternet.oktruck

const val TITLE_KEY = "title_key"
const val TITLE_RES_KEY = "title_res_key"
const val MESSAGE_KEY = "message_key"
const val MESSAGE_RES_KEY = "message_res_key"
const val ACTION_KEY = "action_key"
const val ACTION_RES_KEY = "action_res_key"
const val PRICE_KEY = "price_key"
const val IMAGE_PATH_KEY = "image_path_key"
const val IMAGE_NAME_KEY = "image_name_path_key"
const val PROVINCE_KEY = "province_key"
const val PROVINCE_ARRAY_KEY = "province_array_key"
const val NAVIGATION_TAB_KEY = "navigation_tab_key"
const val DATE_KEY = "date_key"
const val MONTH_KEY = "month_key"
const val YEAR_KEY = "year_key"

/* model */
const val TRUCK_TYPES_KEY = "truck_types_key"
const val TRUCK_TYPE_KEY = "truck_type_key"
const val ADDITIONAL_SERVICES_KEY = "additional_services_key"
const val SELECTED_ADDITIONAL_SERVICES_KEY = "selected_additional_services_key"
const val WORK_KEY = "work_key"
const val NEWS_KEY = "news_key"
const val SOURCE_ADDRESS_HISTORY_LIST_KEY = "source_address_history_list_key"
const val DESTINATION_ADDRESS_HISTORY_LIST_KEY = "destination_address_history_list_key"

/* network */
const val HEADER_KEY = "Authorization"
const val TOKEN_TYPE = "Bearer "
const val TOKEN_KEY = "token_key"

/* api */
const val API_PATH = "api/v1/"

const val PRODUCTION_URL = "https://backoffice.oktruck.com"
const val PRODUCTION_API_URL = "$PRODUCTION_URL/$API_PATH"

const val BASE_URL = PRODUCTION_URL
const val BASE_API_URL = PRODUCTION_API_URL

/* user manager */
const val IS_ENABLE_NOTIFICATION_KEY = "is_enable_notification_key"

/* adapter */
const val VIEW_TYPE_NO_DATA = 0
const val VIEW_TYPE_LOAD_MORE = 1
const val VIEW_TYPE_DATA = 2
const val VIEW_TYPE_CAMERA = 3

/* work status */
const val WAITING_DRIVER = 1
const val CONFIRMED = 2
const val ARRIVED_SOURCE = 3
const val SIGNED_SOURCE = 4
const val ARRIVED_DESTINATION = 5
const val SIGNED_DESTINATION = 6
const val FINISH = 7
const val CANCEL = 8

/* permission */
const val REQUEST_CODE_CALL_PERMISSION = 1
const val REQUEST_CODE_WRITE_PERMISSION = 2
const val REQUEST_CODE_LOCATION_PERMISSION = 3
const val REQUEST_CODE_GPS_SERVICE = 4

/* result code */
const val WORK_DETAIL_REQUEST = 1

/* init spinnerValue */
const val INITIAL_STRING = ""
const val INITIAL_INT = 0
const val INITIAL_DOUBLE = 0.0
const val UNKNOWN = "ไม่ระบุ"

/* omise */
const val OMISE_PUBLIC_KEY = "pkey_test_5ezt65lmm4vt9n9q1dy"
const val AUTHORIZING_PAYMENT_REQUEST_CODE = 1

