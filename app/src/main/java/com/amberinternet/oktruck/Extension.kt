package com.amberinternet.oktruck

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Paint
import android.os.Build
import android.os.Environment
import android.text.*
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.Spinner
import android.widget.TextView
import androidx.databinding.Observable
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import id.zelory.compressor.Compressor
import com.amberinternet.oktruck.util.glide.GlideHeader
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.io.ByteArrayOutputStream
import java.io.FileOutputStream
import java.math.RoundingMode
import java.text.DecimalFormat


inline fun <reified T : Any> Any.cast(): T {
    return this as T
}

fun String.toSpanned() : Spanned {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        return Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
    } else {
        @Suppress("DEPRECATION")
        return Html.fromHtml(this)
    }
}

fun TextView.underline() {
    this.paintFlags = this.paintFlags or Paint.UNDERLINE_TEXT_FLAG
}

fun EditText.isValidTelephone(): Boolean {
    return !this.text.isNullOrEmpty() && this.text.length == 10
}

fun EditText.confirmPassword(passwordEditText: EditText): Boolean {
    return this.string() == passwordEditText.string()
}

fun EditText.isEmpty(): Boolean {
    return this.text.isNullOrEmpty()
}

fun EditText.length(): Int {
    return this.text.length
}

fun EditText.validateDay(month: Int, year: Int): Boolean {
    return !when (month) {
        1, 3, 5, 7, 8, 10, 12 -> {
            this.string().toInt() in 1..31
        }
        4, 6, 9, 11 -> {
            this.string().toInt() in 1..30
        }
        else -> {
            (year % 4 != 0 && this.string().toInt() in 1..28) || (year % 4 == 0 && this.string().toInt() in 1..29)
        }
    }
}

fun TextView.isEmpty(): Boolean {
    return this.text.isNullOrEmpty()
}

fun TextView.string(): String = this.text.toString()

fun EditText.isValidEmail(): Boolean {
    return !TextUtils.isEmpty(this.text) && android.util.Patterns.EMAIL_ADDRESS.matcher(this.text).matches()
}

fun Spinner.isNotSelected(): Boolean {
    return this.selectedItemPosition == 0
}

fun SharedPreferences.putString(key: String, value: String) {
    this.edit().putString(key, value).apply()
}

fun SharedPreferences.putBoolean(key: String, value: Boolean) {
    this.edit().putBoolean(key, value).apply()
}

fun <T : Observable> T.addOnPropertyChanged(callback: (T) -> Unit) =
        object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(observable: Observable?, i: Int) =
                    callback(observable as T)
        }.also { addOnPropertyChangedCallback(it) }

fun ImageView.setImageUrl(path: String?) {
    Glide.with(this)
            .load(BASE_API_URL + path)
            .centerInside()
            .into(this)
}

fun ImageView.setImageUrl(path: String?, token: String) {
    Glide.with(this)
            .load(GlideHeader.getUrlWithHeaders(BASE_API_URL + path, token))
            .centerInside()
            .into(this)
}

fun ImageView.setTruckTypeImage(path: String?) {
    Glide.with(this)
        .load(BASE_URL + path)
        .centerInside()
        .into(this)
}

fun ImageView.setProfileImage(path: String?) {
    Glide.with(this)
            .load(BASE_URL + path)
            .centerCrop()
            .into(this)
}

fun Bitmap.toImageFile(context: Context): File {
    val file = File(context.filesDir, "signature.jpg")
    file.createNewFile()

    val bos = ByteArrayOutputStream()
    this.compress(Bitmap.CompressFormat.JPEG, 80, bos)

    val fos = FileOutputStream(file)
    fos.write(bos.toByteArray())
    fos.flush()
    fos.close()
    return file
}

fun String.toServerYear(): String = (this.toInt() - 543).toString()

fun Date.toDateServerFormat(): String = SimpleDateFormat("yyyy-MM-dd", Locale.US).format(this)

fun Date.toDateTimeServerFormat(): String = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(this)

fun String.fromDate(): Date = SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(this)

fun String.fromDateTime(): Date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).parse(this)

fun Date.toDateWithFullDayAppFormat(): String = String.format("%s %s", this.toDayMonthWithFullDay(), this.toYear())

fun Date.toDateAppFormat(): String = String.format("%s %s", this.toDayMonth(), this.toYear())

fun Date.toDateTimeAppFormat(): String = String.format("%s/%s %s", this.toDayMonthWithoutWeekDay(), this.toYear(), this.toTime())

fun Date.toYear(): String = (SimpleDateFormat("yyyy", Locale.getDefault()).format(this).toInt()/* + 543*/).toString()

fun Date.toDayMonthWithoutWeekDay(): String = SimpleDateFormat("dd/MM", Locale.getDefault()).format(this)

fun Date.toDayMonthWithFullDay(): String = SimpleDateFormat("EEEE d MMM", Locale.getDefault()).format(this)

fun Date.toDayMonth(): String = SimpleDateFormat("dd MMM", Locale.getDefault()).format(this)

fun Date.toTime(): String = SimpleDateFormat("HH:mm", Locale.getDefault()).format(this)

fun Date.toDay(): String = SimpleDateFormat("dd", Locale.getDefault()).format(this)

fun Date.toMonth(): String = SimpleDateFormat("MM", Locale.getDefault()).format(this)

fun Date.toHour(): String = SimpleDateFormat("HH", Locale.getDefault()).format(this)

fun Date.toMinute(): String = SimpleDateFormat("mm", Locale.getDefault()).format(this)

fun Date.toMonthYearFormat(): String = SimpleDateFormat("yyyy-mm", Locale.getDefault()).format(this)

fun Calendar.getDay(): Int = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)

fun Calendar.getHour(): Int = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)

fun Calendar.getMinute(): Int = Calendar.getInstance().get(Calendar.MINUTE)

fun Calendar.getMonth(): Int = Calendar.getInstance().get(Calendar.MONTH) + 1   // is zero based

fun Calendar.getYear(): Int = Calendar.getInstance().get(Calendar.YEAR)

fun Calendar.setDateTime(year: Int, month: Int, day: Int, hour: Int, minute: Int): Date = Calendar.getInstance().apply {
    set(Calendar.YEAR, year)
    set(Calendar.MONTH, month - 1)
    set(Calendar.DAY_OF_MONTH, day)
    set(Calendar.HOUR_OF_DAY, hour)
    set(Calendar.MINUTE, minute)
    set(Calendar.SECOND, 0)
}.time

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }
    })
}

fun File.resize(context: Context, size: Int): File {
    return Compressor(context)
            .setMaxWidth(size)
            .setMaxHeight(size)
            .setQuality(80)
            .setCompressFormat(Bitmap.CompressFormat.JPEG)
            .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).absolutePath)
            .compressToFile(this)
}

fun FragmentActivity.hideKeyboard() {
    val view = this.currentFocus
    if (view != null) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}

fun List<String>.getError(): String {
    var error = ""
    for (s in this) {
        when (s) {
            "The username has already been taken." -> error += "เบอร์โทรศัพท์นี้มีผู้ใช้งานแล้ว\n"
            "The email has already been taken." -> error += "อีเมล์นี้มีผู้ใช้งานแล้ว\n"
            "The email must be a valid email address." -> error += "อีเมล์ไม่ถูกต้อง\n"
            "The password confirmation does not match." -> error += "รหัสผ่านไม่ตรงกัน\n"
            "The selected driver_type_id is invalid." -> error += "ประเภทผู้ขับไม่ถูกต้อง\n"
            "The selected driver_license_type is invalid." -> error += "$s\n"
            "The selected car_type_id is invalid." -> error += "$s\n"
            "The selected topup_type is invalid." -> error += "$s\n"
            "The selected price is invalid." -> error += "$s\n"
            "The selected :attribute is invalid." -> error += "$s\n"
            else -> error += "$s\n"
        }
//        error += "$s\n"
    }
    return error
}

fun Double.decimalFormat(): String {
    val df = DecimalFormat("#,###,###.##")
    return df.format(this)
}

fun Double.roundDecimalFormat(): String {
    val df = DecimalFormat("#,###,###")
    df.roundingMode = RoundingMode.HALF_UP
    return df.format(this)
}