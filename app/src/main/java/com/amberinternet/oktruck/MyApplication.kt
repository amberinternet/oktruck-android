package com.amberinternet.oktruck

import android.content.res.Resources
import androidx.multidex.MultiDexApplication
import pl.aprilapps.easyphotopicker.EasyImage

class MyApplication: MultiDexApplication() {

    companion object {
        private lateinit var instance: MyApplication

        val resources: Resources
            get() = instance.resources
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        /* image */
        EasyImage.configuration(this).setImagesFolderName("OKTruck")
    }

}