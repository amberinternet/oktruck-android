package com.amberinternet.oktruck.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.model.AdditionalService
import com.amberinternet.oktruck.ui.viewholder.AdditionalServiceViewHolder

class AdditionalServicesAdapter(private var additionalServices: Array<AdditionalService>, private val selectedAdditionalServices: ArrayList<AdditionalService>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_view_additional_service_checklist, parent, false)
        return AdditionalServiceViewHolder(view)
    }

    override fun getItemCount(): Int {
        return additionalServices.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is AdditionalServiceViewHolder -> {
                val additionalService = additionalServices[position]

                holder.additionalService = additionalService
                holder.initView()

                if (selectedAdditionalServices.contains(additionalService)) { holder.highlight() }
            }
        }
    }
}