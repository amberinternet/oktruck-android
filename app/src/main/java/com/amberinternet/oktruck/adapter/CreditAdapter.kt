package com.amberinternet.oktruck.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.roundDecimalFormat
import com.amberinternet.oktruck.ui.viewholder.CreditViewHolder

class CreditAdapter(private var creditList: IntArray) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_view_credit, parent, false)
        return CreditViewHolder(view)
    }

    override fun getItemCount(): Int {
        return creditList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is CreditViewHolder -> {
                holder.credit = creditList[position].toDouble().roundDecimalFormat()
                holder.initView()
            }
        }
    }
}