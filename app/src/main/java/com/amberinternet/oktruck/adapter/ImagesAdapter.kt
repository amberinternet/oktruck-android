package com.amberinternet.oktruck.adapter

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.VIEW_TYPE_DATA
import com.amberinternet.oktruck.VIEW_TYPE_CAMERA
import com.amberinternet.oktruck.ui.viewholder.CameraViewHolder
import com.amberinternet.oktruck.ui.viewholder.ImageViewHolder

class ImagesAdapter(private var imagesBitmap: ArrayList<Bitmap>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_CAMERA -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_view_camera, parent, false)
                CameraViewHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_view_image, parent, false)
                ImageViewHolder(view)
            }
        }
    }

    override fun getItemCount(): Int {
        return imagesBitmap.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ImageViewHolder -> {
                holder.bitmap = imagesBitmap[position-1]
                holder.initView()
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0) {
            return VIEW_TYPE_CAMERA
        }
        return VIEW_TYPE_DATA
    }
}