package com.amberinternet.oktruck.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.VIEW_TYPE_DATA
import com.amberinternet.oktruck.VIEW_TYPE_LOAD_MORE
import com.amberinternet.oktruck.VIEW_TYPE_NO_DATA
import com.amberinternet.oktruck.model.Work
import com.amberinternet.oktruck.model.subscriber.LoadMoreSubscriber
import com.amberinternet.oktruck.models.WorkPagination
import com.amberinternet.oktruck.ui.viewholder.LoadMoreViewHolder
import com.amberinternet.oktruck.ui.viewholder.NoDataViewHolder
import com.amberinternet.oktruck.ui.viewholder.WorkViewHolder
import org.greenrobot.eventbus.EventBus

class InProgressWorkAdapter(private var workList: List<Work>, var workPagination: WorkPagination? = null) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_DATA -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_view_work, parent, false)
                WorkViewHolder(view)
            }
            VIEW_TYPE_LOAD_MORE -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_view_load_more, parent, false)
                LoadMoreViewHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_view_no_data, parent, false)
                NoDataViewHolder(view)
            }
        }
    }

    override fun getItemCount(): Int {
        return if (workList.isNotEmpty()) workList.size + 1 else 1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is WorkViewHolder -> {
                holder.work = workList[position]
                holder.initView()
            }
            is LoadMoreViewHolder -> {
                when {
                    workPagination == null -> {
                        holder.showProgressBar()
                        loadMore(1)
                    }
                    workPagination?.total == workList.size -> holder.hideProgressBar()
                    else -> {
                        holder.showProgressBar()
                        loadMore(workPagination!!.currentPage + 1)
                    }
                }
            }
            is NoDataViewHolder -> {

            }
        }
    }

    private fun loadMore(nextPage: Int) {
        EventBus.getDefault().post(LoadMoreSubscriber().apply {
            isLoadMoreMyInProgressWork = true
            page = nextPage
        })
    }

    override fun getItemViewType(position: Int): Int {
        if (workList.isNotEmpty() && position != workList.size) {
            return VIEW_TYPE_DATA
        } else if (workPagination?.total == 0) {
            return VIEW_TYPE_NO_DATA
        }
        return VIEW_TYPE_LOAD_MORE
    }
}