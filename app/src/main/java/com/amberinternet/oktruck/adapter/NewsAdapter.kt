package com.amberinternet.oktruck.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.VIEW_TYPE_DATA
import com.amberinternet.oktruck.VIEW_TYPE_LOAD_MORE
import com.amberinternet.oktruck.VIEW_TYPE_NO_DATA
import com.amberinternet.oktruck.model.News
import com.amberinternet.oktruck.model.subscriber.LoadMoreSubscriber
import com.amberinternet.oktruck.models.NewsPagination
import com.amberinternet.oktruck.ui.viewholder.LoadMoreViewHolder
import com.amberinternet.oktruck.ui.viewholder.NewsViewHolder
import com.amberinternet.oktruck.ui.viewholder.NoDataViewHolder
import org.greenrobot.eventbus.EventBus

class NewsAdapter(private var newsList: List<News>, var newsPagination: NewsPagination? = null) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_DATA -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_view_news, parent, false)
                NewsViewHolder(view)
            }
            VIEW_TYPE_LOAD_MORE -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_view_load_more, parent, false)
                LoadMoreViewHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_view_no_data, parent, false)
                NoDataViewHolder(view)
            }
        }
    }

    override fun getItemCount(): Int {
        return if (newsList.isNotEmpty()) newsList.size + 1 else 1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is NewsViewHolder -> {
                holder.news = newsList[position]
                holder.initView()
            }
            is LoadMoreViewHolder -> {
                when {
                    newsPagination == null -> {
                        holder.showProgressBar()
                        loadMore(1)
                    }
                    newsPagination?.total == newsList.size -> holder.hideProgressBar()
                    else -> {
                        holder.showProgressBar()
                        loadMore(newsPagination!!.currentPage + 1)
                    }
                }
            }
            is NoDataViewHolder -> {

            }
        }
    }

    private fun loadMore(nextPage: Int) {
        EventBus.getDefault().post(LoadMoreSubscriber().apply {
            isLoadMoreNews = true
            page = nextPage
        })
    }

    override fun getItemViewType(position: Int): Int {
        if (newsList.isNotEmpty() && position != newsList.size) {
            return VIEW_TYPE_DATA
        } else if (newsPagination?.total == 0) {
            return VIEW_TYPE_NO_DATA
        }
        return VIEW_TYPE_LOAD_MORE
    }
}