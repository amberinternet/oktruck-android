package com.amberinternet.oktruck.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.model.Source
import com.amberinternet.oktruck.ui.viewholder.AddressHistoryViewHolder

class SourceAddressHistoryAdapter(private var addresses: ArrayList<Source>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_view_address_history, parent, false)
        return AddressHistoryViewHolder(view)
    }

    override fun getItemCount(): Int {
        return addresses.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is AddressHistoryViewHolder -> {
                holder.source = addresses[position]
                holder.initSourceView()
            }
        }
    }
}