package com.amberinternet.oktruck.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.amberinternet.oktruck.R
import kotlinx.android.synthetic.main.item_view_spinner_transaction_request.view.*

class TransactionRequestSpinnerAdapter : BaseAdapter() {

    lateinit var stringArray: Array<String>

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_view_spinner_transaction_request, parent, false)
        view.textView.text = stringArray[position]
        return view
    }

    override fun getItem(position: Int): Any {
        return stringArray[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return stringArray.size
    }
}