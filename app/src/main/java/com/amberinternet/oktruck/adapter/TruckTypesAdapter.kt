package com.amberinternet.oktruck.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.model.TruckType
import com.amberinternet.oktruck.ui.viewholder.TruckTypeViewHolder

class TruckTypesAdapter(private var truckTypes: Array<TruckType>, private val selectedTruckType: TruckType?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_view_truck_type, parent, false)
        return TruckTypeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return truckTypes.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is TruckTypeViewHolder -> {
                holder.truckType = truckTypes[position]
                holder.initView()

                if (selectedTruckType != null && truckTypes[position].id == selectedTruckType.id) holder.highlight()
            }
        }
    }
}