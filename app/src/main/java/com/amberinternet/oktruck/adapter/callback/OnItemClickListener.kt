package com.amberinternet.oktruck.adapter.callback

interface OnItemClickListener {
    fun clickAtIndex(position: Int)
}