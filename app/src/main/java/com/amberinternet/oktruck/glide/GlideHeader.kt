package com.amberinternet.oktruck.util.glide

import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.load.model.GlideUrl
import com.amberinternet.oktruck.HEADER_KEY
import com.amberinternet.oktruck.TOKEN_TYPE

internal object GlideHeader {

    fun getUrlWithHeaders(url: String, token: String): GlideUrl {
        return GlideUrl(url, LazyHeaders.Builder()
                .addHeader(HEADER_KEY, TOKEN_TYPE + token)
                .build())
    }
}