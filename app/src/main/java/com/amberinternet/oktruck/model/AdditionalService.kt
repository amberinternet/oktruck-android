package com.amberinternet.oktruck.model

import android.os.Parcel
import android.os.Parcelable
import com.amberinternet.oktruck.INITIAL_DOUBLE
import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.INITIAL_INT
import com.amberinternet.oktruck.INITIAL_STRING
import com.amberinternet.oktruck.roundDecimalFormat

data class AdditionalService(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("additional_service_name")
        var name: String = INITIAL_STRING,

        @SerializedName("pivot")
        private var pivot: Pivot = Pivot(),

        @SerializedName("price")
        private var charge: Double = INITIAL_DOUBLE

): Parcelable {

        data class Pivot(@SerializedName("price") var charge: Double = INITIAL_DOUBLE): Parcelable {
                constructor(parcel: Parcel) : this(parcel.readDouble())

                override fun writeToParcel(parcel: Parcel, flags: Int) {
                        parcel.writeDouble(charge)
                }

                override fun describeContents(): Int {
                        return 0
                }

                companion object CREATOR : Parcelable.Creator<Pivot> {
                        override fun createFromParcel(parcel: Parcel): Pivot {
                                return Pivot(parcel)
                        }

                        override fun newArray(size: Int): Array<Pivot?> {
                                return arrayOfNulls(size)
                        }
                }
        }

        constructor(parcel: Parcel) : this(
                parcel.readInt(),
                parcel.readString(),
                parcel.readParcelable(Pivot::class.java.classLoader),
                parcel.readDouble()
        )

        fun getCharge(): String {
                return if (charge != INITIAL_DOUBLE) {
                        charge.roundDecimalFormat()
                } else {
                        pivot.charge.roundDecimalFormat()
                }
        }

        fun getChargeValue(): Double {
                return if (charge != INITIAL_DOUBLE) {
                        charge
                } else {
                        pivot.charge
                }
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeInt(id)
                parcel.writeString(name)
                parcel.writeParcelable(pivot, flags)
                parcel.writeDouble(charge)
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<AdditionalService> {
                override fun createFromParcel(parcel: Parcel): AdditionalService {
                        return AdditionalService(parcel)
                }

                override fun newArray(size: Int): Array<AdditionalService?> {
                        return arrayOfNulls(size)
                }
        }

}