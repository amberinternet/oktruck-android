package com.amberinternet.oktruck.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.INITIAL_INT
import com.amberinternet.oktruck.INITIAL_STRING

data class Company(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("company_name")
        var name: String = INITIAL_STRING,

        @SerializedName("company_telephone_number")
        var telephoneNumber: String = INITIAL_STRING,

        @SerializedName("company_tax_id")
        var taxId: String = INITIAL_STRING,

        @SerializedName("company_address")
        var address: String = INITIAL_STRING

): Parcelable {
        constructor(parcel: Parcel) : this(
                parcel.readInt(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString()
        )
        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeInt(id)
                parcel.writeString(name)
                parcel.writeString(telephoneNumber)
                parcel.writeString(taxId)
                parcel.writeString(address)
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<Company> {
                override fun createFromParcel(parcel: Parcel): Company {
                        return Company(parcel)
                }

                override fun newArray(size: Int): Array<Company?> {
                        return arrayOfNulls(size)
                }
        }
}