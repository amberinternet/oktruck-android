package com.amberinternet.oktruck.model

import com.amberinternet.oktruck.*
import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.util.enum.DepositType
import com.amberinternet.oktruck.util.enum.TransactionRequestStatus

data class Deposit(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("topup_type")
        var type: Int = INITIAL_INT,

        @SerializedName("price")
        var price: Double = INITIAL_DOUBLE,

        @SerializedName("status")
        var status: Int = INITIAL_INT,

        @SerializedName("approved_time")
        var approvedTime: String = INITIAL_STRING,

        @SerializedName("approved_note")
        var remark: String = INITIAL_STRING,

        @SerializedName("pay_slip_image")
        var transferSlip: String = INITIAL_STRING,

        @SerializedName("pay_time")
        var payTime: String = INITIAL_STRING,

        @SerializedName("formatted_topup_id")
        var readableId: String = INITIAL_STRING,

        @SerializedName("created_at")
        var createdAt: String = INITIAL_STRING
) {

    fun getCredit() = price.roundDecimalFormat()

    fun getReadableCredit(): String {
        val credit = getCredit()
        return "฿$credit"
    }

    fun getReadableStatus(): String {
        return when (status) {
            TransactionRequestStatus.APPROVED.status -> TransactionRequestStatus.APPROVED.depositMessage
            TransactionRequestStatus.REJECT.status -> TransactionRequestStatus.REJECT.depositMessage
            TransactionRequestStatus.WAITING.status -> TransactionRequestStatus.WAITING.depositMessage
            else -> UNKNOWN
        }
    }

    fun getReadablePaymentMethod(): String {
        return when (type) {
            DepositType.OMISE.value -> DepositType.OMISE.method
            DepositType.TRANSFER.value -> DepositType.TRANSFER.method
            else -> UNKNOWN
        }
    }

    fun getSlipImagePath(): String {
        return "image/topup/pay-slip-image/$transferSlip"
    }
}