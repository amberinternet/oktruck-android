package com.amberinternet.oktruck.model

import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.INITIAL_STRING

data class DepositOmise(

        @SerializedName("topup")
        var deposit: Deposit = Deposit(),

        @SerializedName("authorize_uri")
        var authorizeUri: String = INITIAL_STRING
)