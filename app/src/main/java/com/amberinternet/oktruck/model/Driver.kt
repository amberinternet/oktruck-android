package com.amberinternet.oktruck.model

import android.os.Parcel
import android.os.Parcelable
import com.amberinternet.oktruck.INITIAL_DOUBLE
import com.amberinternet.oktruck.INITIAL_INT
import com.amberinternet.oktruck.INITIAL_STRING
import com.google.gson.annotations.SerializedName

data class Driver(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("driver_type_id")
        var driverTypeId: Int = INITIAL_INT,

        @SerializedName("company_name")
        var companyName: String = INITIAL_STRING,

        @SerializedName("citizen_id")
        var citizenId: String = INITIAL_STRING,

        @SerializedName("driver_license_id")
        var driverLicenseId: String = INITIAL_STRING,

        var driverLicenseTypePosition: Int = INITIAL_INT,

        @SerializedName("driver_license_type")
        var driverLicenseType: String = INITIAL_STRING,

        @SerializedName("created_at")
        var createdAt: String = INITIAL_STRING,

        @SerializedName("formatted_driver_id")
        var readableDriverId: String = INITIAL_STRING,

        @SerializedName("user")
        var user: User = User(),

        @SerializedName("car")
        var truck: Truck = Truck()

): Parcelable {

        data class User(

                @SerializedName("id")
                var id: Int = INITIAL_INT,

                @SerializedName("first_name")
                var firstName: String = INITIAL_STRING,

                @SerializedName("last_name")
                var lastName: String = INITIAL_STRING,

                @SerializedName("email")
                var email: String = INITIAL_STRING,

                @SerializedName("telephone_number")
                var telephoneNumber: String = INITIAL_STRING,

                @SerializedName("formatted_user_id")
                var readableUserId: String = INITIAL_STRING,

                @SerializedName("company")
                var company: Company = Company()

        ): Parcelable {
                constructor(parcel: Parcel) : this(
                        parcel.readInt(),
                        parcel.readString(),
                        parcel.readString(),
                        parcel.readString(),
                        parcel.readString(),
                        parcel.readString(),
                        parcel.readParcelable(Company::class.java.classLoader)
                )

                fun getFullName() : String {
                        return "$firstName $lastName"
                }

                override fun writeToParcel(parcel: Parcel, flags: Int) {
                        parcel.writeInt(id)
                        parcel.writeString(firstName)
                        parcel.writeString(lastName)
                        parcel.writeString(email)
                        parcel.writeString(telephoneNumber)
                        parcel.writeString(readableUserId)
                        parcel.writeParcelable(company, flags)
                }

                override fun describeContents(): Int {
                        return 0
                }

                companion object CREATOR : Parcelable.Creator<User> {
                        override fun createFromParcel(parcel: Parcel): User {
                                return User(parcel)
                        }

                        override fun newArray(size: Int): Array<User?> {
                                return arrayOfNulls(size)
                        }
                }
        }

        constructor(parcel: Parcel) : this(
                parcel.readInt(),
                parcel.readInt(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readInt(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readParcelable(User::class.java.classLoader),
                parcel.readParcelable(Truck::class.java.classLoader)
        )

        override fun toString(): String {
                return "Driver(id=$id, driverTypeId=$driverTypeId, companyName='$companyName', citizenId='$citizenId', driverLicenseId='$driverLicenseId', driverLicenseTypePosition=$driverLicenseTypePosition, driverLicenseType='$driverLicenseType', user=$user, truck=$truck)"
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeInt(id)
                parcel.writeInt(driverTypeId)
                parcel.writeString(companyName)
                parcel.writeString(citizenId)
                parcel.writeString(driverLicenseId)
                parcel.writeInt(driverLicenseTypePosition)
                parcel.writeString(driverLicenseType)
                parcel.writeString(createdAt)
                parcel.writeString(readableDriverId)
                parcel.writeParcelable(user, flags)
                parcel.writeParcelable(truck, flags)
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<Driver> {
                override fun createFromParcel(parcel: Parcel): Driver {
                        return Driver(parcel)
                }

                override fun newArray(size: Int): Array<Driver?> {
                        return arrayOfNulls(size)
                }
        }
}