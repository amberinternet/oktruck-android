package com.amberinternet.oktruck.model

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.net.Uri
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.INITIAL_INT
import com.amberinternet.oktruck.INITIAL_STRING

@SuppressLint("ParcelCreator")
data class Item(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("item_name")
        var name: String = INITIAL_STRING,

        @SerializedName("item_quantity")
        var quantity: String = INITIAL_STRING,

        @SerializedName("item_weight")
        var weight: String = INITIAL_STRING,

        @SerializedName("item_image")
        var imageFileName: String = INITIAL_STRING,

        var imageUri: Uri? = null

): Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(Bitmap::class.java.classLoader)
    )

    fun getImagePath(): String {
        return "image/item/$imageFileName"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeString(quantity)
        parcel.writeString(weight)
        parcel.writeString(imageFileName)
        parcel.writeParcelable(imageUri, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Item> {
        override fun createFromParcel(parcel: Parcel): Item {
            return Item(parcel)
        }

        override fun newArray(size: Int): Array<Item?> {
            return arrayOfNulls(size)
        }
    }

}