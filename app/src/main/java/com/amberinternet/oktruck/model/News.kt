package com.amberinternet.oktruck.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.INITIAL_INT
import com.amberinternet.oktruck.INITIAL_STRING

data class News(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("title")
        var title: String = INITIAL_STRING,

        @SerializedName("short_content")
        var shortContent: String = INITIAL_STRING,

        @SerializedName("full_content")
        var fullContent: String = INITIAL_STRING,

        @SerializedName("is_pinned")
        var isPinned: Int = INITIAL_INT,

        @SerializedName("posted_at")
        var postAt: String = INITIAL_STRING
): Parcelable {
        constructor(parcel: android.os.Parcel) : this(
                parcel.readInt(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readInt(),
                parcel.readString()
        )

        override fun writeToParcel(parcel: android.os.Parcel, flags: Int) {
                parcel.writeInt(id)
                parcel.writeString(title)
                parcel.writeString(shortContent)
                parcel.writeString(fullContent)
                parcel.writeInt(isPinned)
                parcel.writeString(postAt)
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<News> {
                override fun createFromParcel(parcel: android.os.Parcel): News {
                        return News(parcel)
                }

                override fun newArray(size: Int): Array<News?> {
                        return arrayOfNulls(size)
                }
        }
}