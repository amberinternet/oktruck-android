package com.amberinternet.oktruck.model

import android.os.Parcel
import android.os.Parcelable
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.MyApplication.Companion.resources
import com.google.gson.annotations.SerializedName

data class Source(

    @SerializedName("id")
        var id: Int = INITIAL_INT,

    @SerializedName("source_latitude")
        var latitude: Double = INITIAL_DOUBLE,

    @SerializedName("source_longitude")
        var longitude: Double = INITIAL_DOUBLE,

    @SerializedName("source_full_address")
        var address: String = INITIAL_STRING,

    @SerializedName("source_address_subdistrict")
        var subdistrict: String = INITIAL_STRING,

    @SerializedName("source_address_district")
        var district: String = INITIAL_STRING,

    @SerializedName("source_address_province")
        var province: String = INITIAL_STRING,

    @SerializedName("source_contact_name")
        var contactName: String = INITIAL_STRING,

    @SerializedName("source_contact_telephone_number")
        var contactTelephoneNumber: String = INITIAL_STRING,

    @SerializedName("source_shop_name")
        var name: String = INITIAL_STRING,

    @SerializedName("source_landmark")
        var landmark: String = INITIAL_STRING,

    @SerializedName("autograph")
        var signatureFileName: String = INITIAL_STRING,

    @SerializedName("received_date")
        var date: String = INITIAL_STRING,

    @SerializedName("received_time")
        var time: String = INITIAL_STRING

): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    fun getShortAddress(): String {
        return "$district, $province"
    }

    fun getReadableTime(): String {
        return if (time == "เวลาใดก็ได้") resources.getString(R.string.anytime) else "$time${resources.getString(R.string.o_clock)}"
    }

    fun getArrivalDateTime(): String {
        return "${date.fromDate().toDateWithFullDayAppFormat()} | ${getReadableTime()}"
    }

    fun getReadableLandmark(): String {
        return "${resources.getString(R.string.landmark)}: $landmark"
    }

    fun getReadableContactName(): String {
        return "${resources.getString(R.string.contact)}: $contactName"
    }

    fun getSignatureImagePath(): String {
        return "image/autograph/$signatureFileName"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeDouble(latitude)
        parcel.writeDouble(longitude)
        parcel.writeString(address)
        parcel.writeString(subdistrict)
        parcel.writeString(district)
        parcel.writeString(province)
        parcel.writeString(contactName)
        parcel.writeString(contactTelephoneNumber)
        parcel.writeString(name)
        parcel.writeString(landmark)
        parcel.writeString(signatureFileName)
        parcel.writeString(date)
        parcel.writeString(time)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Source> {
        override fun createFromParcel(parcel: Parcel): Source {
            return Source(parcel)
        }

        override fun newArray(size: Int): Array<Source?> {
            return arrayOfNulls(size)
        }
    }
}