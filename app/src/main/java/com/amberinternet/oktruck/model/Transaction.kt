package com.amberinternet.oktruck.model

import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.MyApplication.Companion.resources
import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.util.enum.TransactionType

data class Transaction(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("credit_log_type")
        var type: Int = INITIAL_INT,

        @SerializedName("description")
        var description: String = INITIAL_STRING,

        @SerializedName("old_credit_balance")
        var oldCreditBalance: String = INITIAL_STRING,

        @SerializedName("new_credit_balance")
        var newCreditBalance: String = INITIAL_STRING,

        @SerializedName("credit")
        var credit: Double = INITIAL_DOUBLE,

        @SerializedName("created_at")
        var createdAt: String = INITIAL_STRING,

        @SerializedName("work")
        var work: Work = Work(),

        @SerializedName("topup")
        var deposit: Deposit = Deposit(),

        @SerializedName("withdraw")
        var withdraw: Withdraw = Withdraw()
) {

    fun getReadableType(): String {
        return when (type) {
            TransactionType.DEPOSIT.value  -> "<b>${resources.getString(R.string.transaction_deposit)}</b>"
            TransactionType.CREATE_WORK.value -> "<b>${resources.getString(R.string.transaction_create_work)}</b> ${resources.getString(R.string.transaction_number)} ${work.getReadableWorkId()}"
            TransactionType.CANCEL_WORK.value, TransactionType.CANCEL_WORK_OMISE.value -> "<b>${resources.getString(R.string.transaction_cancel_work)}</b> ${resources.getString(R.string.transaction_number)} ${work.getReadableWorkId()}"
            TransactionType.ADMIN_ADD.value -> "<b>${resources.getString(R.string.transaction_admin_add)}</b>"
            TransactionType.ADMIN_DELETE.value -> "<b>${resources.getString(R.string.transaction_admin_delete)}</b>"
            TransactionType.WITHDRAW.value -> "<b>${resources.getString(R.string.transaction_withdraw)}</b> ${resources.getString(R.string.transaction_number)} ${work.getReadableWorkId()}"
            else -> "ไม่ระบุ"
        }
    }

    fun getCredit() = credit.roundDecimalFormat()

    fun getCreditTextColor(): Int {
        return when (type) {
            TransactionType.DEPOSIT.value, TransactionType.CANCEL_WORK.value, TransactionType.CANCEL_WORK_OMISE.value, TransactionType.ADMIN_ADD.value -> R.color.depositColor
            TransactionType.CREATE_WORK.value, TransactionType.ADMIN_DELETE.value, TransactionType.WITHDRAW.value -> R.color.withdrawColor
            else -> R.color.textColor
        }
    }
}