package com.amberinternet.oktruck.model

import android.os.Parcel
import android.os.Parcelable
import com.amberinternet.oktruck.INITIAL_INT
import com.amberinternet.oktruck.INITIAL_STRING
import com.google.gson.annotations.SerializedName

data class Truck(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("car_type_id")
        var truckTypeId: Int = INITIAL_INT,

        @SerializedName("license_plate")
        var licensePlate: String = INITIAL_STRING,

        var truckProvinceId: Int = INITIAL_INT,

        @SerializedName("car_province")
        var truckProvince: String = INITIAL_STRING,

        @SerializedName("car_year")
        var truckYear: String = INITIAL_STRING,

        @SerializedName("car_model")
        var truckModel: String = INITIAL_STRING,

        @SerializedName("car_brand")
        var truckBrand: String = INITIAL_STRING,

        @SerializedName("latitude")
        var latitude: String = INITIAL_STRING,

        @SerializedName("longitude")
        var longitude: String = INITIAL_STRING,

        @SerializedName("car_type")
        var truckType: TruckType = TruckType()
): Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(TruckType::class.java.classLoader)
    )

    fun getFullLicensePlate(): String {
        return "$licensePlate $truckProvince"
    }

    fun getBrandAndModel(): String {
        return "$truckBrand $truckModel"
    }

    override fun toString(): String {
        return "Truck(id=$id, truckTypeId=$truckTypeId, licensePlate='$licensePlate', truckProvinceId=$truckProvinceId, truckProvince='$truckProvince', truckYear='$truckYear', truckModel='$truckModel', truckBrand='$truckBrand', latitude='$latitude', longitude='$longitude', truckType=$truckType)"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(truckTypeId)
        parcel.writeString(licensePlate)
        parcel.writeInt(truckProvinceId)
        parcel.writeString(truckProvince)
        parcel.writeString(truckYear)
        parcel.writeString(truckModel)
        parcel.writeString(truckBrand)
        parcel.writeString(latitude)
        parcel.writeString(longitude)
        parcel.writeParcelable(truckType, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Truck> {
        override fun createFromParcel(parcel: Parcel): Truck {
            return Truck(parcel)
        }

        override fun newArray(size: Int): Array<Truck?> {
            return arrayOfNulls(size)
        }
    }
}