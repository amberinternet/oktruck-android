package com.amberinternet.oktruck.model

import android.os.Parcel
import android.os.Parcelable
import com.amberinternet.oktruck.INITIAL_DOUBLE
import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.INITIAL_INT
import com.amberinternet.oktruck.INITIAL_STRING

data class TruckType(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("car_type_name")
        var name: String = INITIAL_STRING,

        @SerializedName("car_type_image")
        var imageFileName: String = INITIAL_STRING,

        @SerializedName("car_type_description")
        var description: String = INITIAL_STRING,

        @SerializedName("price_for_first_3_km")
        var priceFirst3Km: Double = INITIAL_DOUBLE,

        @SerializedName("price_per_km_at_3_15")
        var price3To15Km: Double = INITIAL_DOUBLE,

        @SerializedName("price_per_km_at_15_100")
        var price15To100Km: Double = INITIAL_DOUBLE,

        @SerializedName("price_per_km_at_100up")
        var price100UpKm: Double = INITIAL_DOUBLE,

        @SerializedName("additional_services")
        var additionalServices: List<AdditionalService> = mutableListOf()

): Parcelable {
        constructor(parcel: Parcel) : this(
                parcel.readInt(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readDouble(),
                parcel.readDouble(),
                parcel.readDouble(),
                parcel.readDouble(),
                parcel.createTypedArrayList(AdditionalService)
        )

        fun getImagePath(): String {
                return "/img/truck/$imageFileName"
        }

        override fun toString(): String {
                return "TruckType(id=$id, name='$name', imageURI='$imageFileName', description='$description', priceFirst3Km=$priceFirst3Km, price3To15Km=$price3To15Km, price15To100Km=$price15To100Km, price100UpKm=$price100UpKm)"
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeInt(id)
                parcel.writeString(name)
                parcel.writeString(imageFileName)
                parcel.writeString(description)
                parcel.writeDouble(priceFirst3Km)
                parcel.writeDouble(price3To15Km)
                parcel.writeDouble(price15To100Km)
                parcel.writeDouble(price100UpKm)
                parcel.writeTypedList(additionalServices)
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<TruckType> {
                override fun createFromParcel(parcel: Parcel): TruckType {
                        return TruckType(parcel)
                }

                override fun newArray(size: Int): Array<TruckType?> {
                        return arrayOfNulls(size)
                }
        }
}