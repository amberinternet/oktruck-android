package com.amberinternet.oktruck.model

import android.util.Log
import com.amberinternet.oktruck.INITIAL_DOUBLE
import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.INITIAL_INT
import com.amberinternet.oktruck.INITIAL_STRING
import com.amberinternet.oktruck.roundDecimalFormat

data class User(

    @SerializedName("id")
        var id: Int = INITIAL_INT,

    @SerializedName("first_name")
        var firstName: String = INITIAL_STRING,

    @SerializedName("last_name")
        var lastName: String = INITIAL_STRING,

    @SerializedName("email")
        var email: String = INITIAL_STRING,

    @SerializedName("profile_image")
        var profileImageURI: String = INITIAL_STRING,

    @SerializedName("current_address")
        var address: String = INITIAL_STRING,

    @SerializedName("telephone_number")
        var telephoneNumber: String = INITIAL_STRING,

    @SerializedName("credit_balance")
        var creditBalance: Double = INITIAL_DOUBLE,

    @SerializedName("is_verified")
        var isVerify: Int = INITIAL_INT,

    @SerializedName("created_at")
        var createdAt: String = INITIAL_STRING,

    @SerializedName("formatted_user_id")
        var readableUserId: String = INITIAL_STRING,

    @SerializedName("company")
        var company: Company = Company(),


    var password: String = INITIAL_STRING,

    var confirmPassword: String = INITIAL_STRING

) {

    private object Holder {
        val INSTANCE = User()
    }

    companion object {
        val instance: User by lazy { Holder.INSTANCE }
    }

    fun init(user: User) {
        instance.id = user.id
        instance.firstName = user.firstName
        instance.lastName = user.lastName
        instance.email = user.email
        instance.profileImageURI = user.profileImageURI
        instance.address = user.address
        instance.telephoneNumber = user.telephoneNumber
        instance.creditBalance = user.creditBalance
        instance.isVerify = user.isVerify
        instance.createdAt = user.createdAt
        instance.readableUserId = user.readableUserId
        instance.company = user.company
    }

    fun clear() {
        instance.id = INITIAL_INT
        instance.firstName = INITIAL_STRING
        instance.lastName = INITIAL_STRING
        instance.email = INITIAL_STRING
        instance.profileImageURI = INITIAL_STRING
        instance.address = INITIAL_STRING
        instance.telephoneNumber = INITIAL_STRING
        instance.creditBalance = INITIAL_DOUBLE
        instance.isVerify = INITIAL_INT
        instance.createdAt = INITIAL_STRING
        instance.readableUserId = INITIAL_STRING
        instance.company = Company()

        instance.password = INITIAL_STRING
        instance.confirmPassword = INITIAL_STRING
    }

    fun getFullName() : String {
        return "$firstName $lastName"
    }

    fun getCredit() = creditBalance.roundDecimalFormat()

    fun getReadableCredit(): String {
        var credit = getCredit()
        Log.d("sssssss", creditBalance.toString())
        return "฿$credit"
    }

    override fun toString(): String {
        return "User(id=$id, firstName='$firstName', lastName='$lastName', email='$email', profileImage='$profileImageURI', address='$address', telephoneNumber='$telephoneNumber', credit='$creditBalance', isVerify=$isVerify, readableUserId='$readableUserId', password='$password', confirmPassword='$confirmPassword')"
    }
}
