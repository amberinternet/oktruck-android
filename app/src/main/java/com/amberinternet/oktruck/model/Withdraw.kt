package com.amberinternet.oktruck.model

import com.amberinternet.oktruck.*
import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.util.enum.TransactionRequestStatus

data class Withdraw(

        @SerializedName("id")
        var id: Int = INITIAL_INT,

        @SerializedName("price")
        var price: Double = INITIAL_DOUBLE,

        @SerializedName("status")
        var status: Int = INITIAL_INT,

        @SerializedName("approved_time")
        var approvedTime: String = INITIAL_STRING,

        @SerializedName("approved_note")
        var remark: String = INITIAL_STRING,

        @SerializedName("pay_slip_image")
        var transferSlip: String = INITIAL_STRING,

        @SerializedName("pay_time")
        var payTime: String = INITIAL_STRING,

        @SerializedName("formatted_withdraw_id")
        var readableId: String = INITIAL_STRING,

        @SerializedName("created_at")
        var createdAt: String = INITIAL_STRING
) {

    fun getCredit() = price.toDouble().roundDecimalFormat()

    fun getReadableCredit(): String {
        val credit = getCredit()
        return "฿$credit"
    }

    fun getReadableStatus(): String {
        return when (status) {
            TransactionRequestStatus.WAITING.status -> TransactionRequestStatus.WAITING.withdrawMessage
            TransactionRequestStatus.APPROVED.status -> TransactionRequestStatus.APPROVED.withdrawMessage
            TransactionRequestStatus.REJECT.status -> TransactionRequestStatus.REJECT.withdrawMessage
            else -> UNKNOWN
        }
    }

    fun getReadableTransferDate(): String {
        return when (status) {
            TransactionRequestStatus.WAITING.status -> TransactionRequestStatus.WAITING.withdrawMessage
            TransactionRequestStatus.APPROVED.status -> payTime.fromDateTime().toDateTimeAppFormat()
            TransactionRequestStatus.REJECT.status -> TransactionRequestStatus.REJECT.withdrawMessage
            else -> UNKNOWN
        }
    }

    fun getSlipImagePath(): String {
        return "image/withdraw/pay-slip-image/$transferSlip"
    }
}