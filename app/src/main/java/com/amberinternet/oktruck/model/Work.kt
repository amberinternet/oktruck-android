package com.amberinternet.oktruck.model

import android.os.Parcel
import android.os.Parcelable
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.MyApplication.Companion.resources
import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.util.enum.PaymentMethod
import com.amberinternet.oktruck.util.enum.WorkStatus

data class Work(

    @SerializedName("id")
    var id: Int = INITIAL_INT,

    @SerializedName("commission")
    var commission: Int = INITIAL_INT,

    @SerializedName("distance")
    var distance: Int = INITIAL_INT,

    @SerializedName("distance_price")
    var transportationFee: String = INITIAL_STRING,

    @SerializedName("full_price")
    var totalFee: String = INITIAL_STRING,

    @SerializedName("status")
    var status: Int = INITIAL_INT,

    @SerializedName("payment_method")
    var paymentMethod: Int = INITIAL_INT,

    @SerializedName("user_name")
    var userName: String = INITIAL_STRING,

    @SerializedName("user_telephone_number")
    var userTelephoneNumber: String = INITIAL_STRING,

    @SerializedName("user_note")
    var note: String = INITIAL_STRING,

    @SerializedName("is_driver_paid")
    var driverPaid: Int = INITIAL_INT,

    @SerializedName("formatted_work_id")
    var formattedWorkId: String = INITIAL_STRING,

    @SerializedName("commission_price")
    var commissionPrice: Double = INITIAL_DOUBLE,

    @SerializedName("driver")   // The server returned a null value when the work canceled before the driver accepted the work.
    var driver: Driver? = Driver(),

    @SerializedName("item")
    var item: Item = Item(),

    @SerializedName("car_type")
    var truckType: TruckType = TruckType(),

    @SerializedName("additional_services")
    var additionalServices: List<AdditionalService> = mutableListOf(),

    @SerializedName("source")
    var source: Source = Source(),

    @SerializedName("destination")
    var destination: Destination = Destination()

): Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readDouble(),
        parcel.readParcelable(Driver::class.java.classLoader),
        parcel.readParcelable(Item::class.java.classLoader),
        parcel.readParcelable(TruckType::class.java.classLoader),
        parcel.createTypedArrayList(AdditionalService),
        parcel.readParcelable(Source::class.java.classLoader),
        parcel.readParcelable(Destination::class.java.classLoader)
    )

    fun getReadableWorkId(): String {
        return "#$formattedWorkId"
    }

    fun getReadableDistance(): String {
        val km = distance.toDouble() / 1000.0
        return "${km.roundDecimalFormat()} ${resources.getString(R.string.km)}"
    }

    fun getReadablePaymentMethod(): String {
        return when (paymentMethod) {
            PaymentMethod.CASH_SOURCE.status -> PaymentMethod.CASH_SOURCE.pay
            PaymentMethod.CASH_DESTINATION.status -> PaymentMethod.CASH_DESTINATION.pay
            PaymentMethod.WALLET.status -> PaymentMethod.WALLET.pay
            PaymentMethod.OMISE.status -> PaymentMethod.OMISE.pay
            else -> UNKNOWN
        }
    }

    fun getReadableReceiveMethod(): String {
        return when (paymentMethod) {
            PaymentMethod.CASH_SOURCE.status -> PaymentMethod.CASH_SOURCE.receive
            PaymentMethod.CASH_DESTINATION.status -> PaymentMethod.CASH_DESTINATION.receive
            PaymentMethod.WALLET.status, PaymentMethod.OMISE.status -> PaymentMethod.WALLET.receive
            else -> UNKNOWN
        }
    }

    fun getReadableWorkStatus(): String {
        return when (status) {
            WorkStatus.PAYMENT_FAIL.status -> WorkStatus.PAYMENT_FAIL.readableStatus
            WorkStatus.FIND_TRUCK.status -> WorkStatus.FIND_TRUCK.readableStatus
            WorkStatus.WAITING_CONFIRM_WORK.status -> WorkStatus.WAITING_CONFIRM_WORK.readableStatus
            WorkStatus.WAITING_RECEIVE_ITEM.status -> WorkStatus.WAITING_RECEIVE_ITEM.readableStatus
            WorkStatus.ARRIVED_SOURCE.status -> WorkStatus.ARRIVED_SOURCE.readableStatus
            WorkStatus.WAITING_SENT_ITEM.status -> WorkStatus.WAITING_SENT_ITEM.readableStatus
            WorkStatus.ARRIVED_DESTINATION.status -> WorkStatus.ARRIVED_DESTINATION.readableStatus
            WorkStatus.COMPLETE.status -> WorkStatus.COMPLETE.readableStatus
            WorkStatus.CANCEL.status -> WorkStatus.CANCEL.readableStatus
            else -> UNKNOWN
        }
    }

    fun getPrice() = totalFee.toDouble().roundDecimalFormat()

    fun getReadablePrice(): String {
        val price = getPrice()
        return "฿$price"
    }

    override fun toString(): String {
        return "Work(id=$id, commission=$commission, distance=$distance, distancePrice='$transportationFee', fullPrice='$totalFee', status=$status, paymentMethod=$paymentMethod, userName='$userName', userTelephoneNumber='$userTelephoneNumber', note='$note', driverPaid=$driverPaid, formattedWorkId='$formattedWorkId', commissionPrice=$commissionPrice, driver=$driver, item=$item, truckType=$truckType, additionalServices=$additionalServices, source=$source, destination=$destination)"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(commission)
        parcel.writeInt(distance)
        parcel.writeString(transportationFee)
        parcel.writeString(totalFee)
        parcel.writeInt(status)
        parcel.writeInt(paymentMethod)
        parcel.writeString(userName)
        parcel.writeString(userTelephoneNumber)
        parcel.writeString(note)
        parcel.writeInt(driverPaid)
        parcel.writeString(formattedWorkId)
        parcel.writeDouble(commissionPrice)
        parcel.writeParcelable(driver, flags)
        parcel.writeParcelable(item, flags)
        parcel.writeParcelable(truckType, flags)
        parcel.writeTypedList(additionalServices)
        parcel.writeParcelable(source, flags)
        parcel.writeParcelable(destination, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Work> {
        override fun createFromParcel(parcel: Parcel): Work {
            return Work(parcel)
        }

        override fun newArray(size: Int): Array<Work?> {
            return arrayOfNulls(size)
        }
    }
}