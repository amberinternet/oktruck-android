package com.amberinternet.oktruck.model

import com.amberinternet.oktruck.INITIAL_STRING
import com.google.gson.annotations.SerializedName

data class WorkOmise(

    @SerializedName("work")
    var work: Work = Work(),

    @SerializedName("authorize_uri")
    var authorizeUri: String = INITIAL_STRING

)