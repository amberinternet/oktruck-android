package com.amberinternet.oktruck.models

import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.model.News

data class NewsPagination(@SerializedName("data") var newsList: MutableList<News>): Pagination() {

    override fun toString(): String {
        return "NewsPagination(newsList=$newsList)"
    }
}