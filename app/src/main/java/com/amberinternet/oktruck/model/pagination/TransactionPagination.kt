package com.amberinternet.oktruck.models

import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.model.Transaction

data class TransactionPagination(@SerializedName("data") var transactionList: MutableList<Transaction>): Pagination() {

    override fun toString(): String {
        return "NewsPagination(newsList=$transactionList)"
    }
}