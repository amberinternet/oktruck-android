package com.amberinternet.oktruck.models

import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.model.Work

data class WorkPagination(@SerializedName("data") var workList: MutableList<Work>): Pagination() {

    override fun toString(): String {
        return "WorkPagination(bookList=$workList)"
    }
}