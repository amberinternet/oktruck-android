package com.amberinternet.oktruck.model.subscriber

import com.amberinternet.oktruck.INITIAL_INT

data class FilterTransactionSubscriber(
        var month: Int = INITIAL_INT,
        var year: Int = INITIAL_INT
)