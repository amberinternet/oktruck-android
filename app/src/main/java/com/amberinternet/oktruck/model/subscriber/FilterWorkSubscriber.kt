package com.amberinternet.oktruck.model.subscriber

import com.amberinternet.oktruck.INITIAL_STRING

data class FilterWorkSubscriber(
        var province: String = INITIAL_STRING
)