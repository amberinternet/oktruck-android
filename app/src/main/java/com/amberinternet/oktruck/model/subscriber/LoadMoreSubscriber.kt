package com.amberinternet.oktruck.model.subscriber

data class LoadMoreSubscriber(
        var isLoadMoreWork: Boolean = false,
        var isLoadMoreMyInProgressWork: Boolean = false,
        var isLoadMoreMyCompleteWork: Boolean = false,
        var isLoadMoreNews: Boolean = false,
        var isLoadMoreTransaction: Boolean = false,
        var isLoadMoreDeposit: Boolean = false,
        var isLoadMoreWithdraw: Boolean = false,
        var page: Int = 1
)