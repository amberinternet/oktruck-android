package com.amberinternet.oktruck.model.subscriber

import com.amberinternet.oktruck.INITIAL_DOUBLE

data class LocationSubscriber(
        var latitude: Double = INITIAL_DOUBLE,
        var longitude: Double = INITIAL_DOUBLE
)