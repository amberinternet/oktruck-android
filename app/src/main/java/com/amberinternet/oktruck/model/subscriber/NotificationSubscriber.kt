package com.amberinternet.oktruck.model.subscriber

import com.amberinternet.oktruck.INITIAL_STRING

data class NotificationSubscriber(
        var message: String = INITIAL_STRING
)