package com.amberinternet.oktruck.model.subscriber

data class PermissionSubscriber(
        var isEnable: Boolean
)