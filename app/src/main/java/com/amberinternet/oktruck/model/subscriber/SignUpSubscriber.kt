package com.amberinternet.oktruck.model.subscriber

data class SignUpSubscriber(
        var selectStepNumber: Int? = null,
        var isSignUp: Boolean? = null
)
{

    override fun toString(): String {
        return "SignUpSubscriber(selectStepNumber=$selectStepNumber, isSignUp=$isSignUp)"
    }
}