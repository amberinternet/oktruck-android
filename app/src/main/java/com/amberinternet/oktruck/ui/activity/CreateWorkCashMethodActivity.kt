package com.amberinternet.oktruck.ui.activity

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.model.Work
import com.amberinternet.oktruck.ui.base.BaseActivity
import com.amberinternet.oktruck.ui.factory.CreateWorkCashMethodViewModelFactory
import com.amberinternet.oktruck.ui.viewmodel.CreateWorkCashMethodViewModel
import com.amberinternet.oktruck.util.enum.PaymentMethod
import com.amberinternet.oktruck.util.manager.DialogManager
import kotlinx.android.synthetic.main.activity_create_work_cash_method.*
import kotlinx.android.synthetic.main.item_view_additional_service_charge.view.*

class CreateWorkCashMethodActivity : BaseActivity() {

    private lateinit var viewModel: CreateWorkCashMethodViewModel
    private lateinit var dialogManager: DialogManager
    private var work: Work = Work()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_work_cash_method)
        setTitle(R.string.title_top_up)
        getVariable()
        initVariable()
        initView()
        initOnClick()
    }

    private fun getVariable() {
        intent.extras?.let {
            work = it.getParcelable(WORK_KEY)
        }
    }

    private fun initVariable() {
        viewModel = ViewModelProviders.of(this, CreateWorkCashMethodViewModelFactory(this)).get(CreateWorkCashMethodViewModel::class.java)
        dialogManager = DialogManager.getInstance(supportFragmentManager)
    }

    private fun initView() {
        for (additionalService in work.additionalServices) {
            // Add additional services to service charges list
            val serviceChargesView = layoutInflater.inflate(R.layout.item_view_additional_service_charge, null)
            serviceChargesView.textView_serviceName.text = additionalService.name
            serviceChargesView.textView_serviceCharge.text = additionalService.getCharge()
            linearLayout_serviceCharges.addView(serviceChargesView)
        }
        textView_transportationFee.text = work.transportationFee
        textView_totalFee.text = work.totalFee
    }

    private fun initOnClick() {
        linearLayout_paySuorce.setOnClickListener {
            checkBox_paySource.isChecked = true
            linearLayout_paySuorce.setBackgroundResource(R.drawable.border_selected)

            checkBox_payDestination.isChecked = false
            linearLayout_payDestination.setBackgroundResource(R.drawable.bg_selector_white)
        }

        linearLayout_payDestination.setOnClickListener {
            checkBox_payDestination.isChecked = true
            linearLayout_payDestination.setBackgroundResource(R.drawable.border_selected)

            checkBox_paySource.isChecked = false
            linearLayout_paySuorce.setBackgroundResource(R.drawable.bg_selector_white)
        }

        button_pay.setOnClickListener {
            when {
                checkBox_paySource.isChecked -> subscription = viewModel.createWorkWithCash(work, PaymentMethod.CASH_SOURCE.status)
                checkBox_payDestination.isChecked -> subscription = viewModel.createWorkWithCash(work, PaymentMethod.CASH_DESTINATION.status)
                else -> dialogManager.showAlert(R.string.please_select_payment_method)
            }
        }
    }

}