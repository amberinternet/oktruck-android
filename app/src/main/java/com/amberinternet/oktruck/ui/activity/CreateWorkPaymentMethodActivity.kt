package com.amberinternet.oktruck.ui.activity

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.model.User
import com.amberinternet.oktruck.model.Work
import com.amberinternet.oktruck.ui.base.BaseActivity
import com.amberinternet.oktruck.ui.factory.CreateWorkPaymentMethodViewModelFactory
import com.amberinternet.oktruck.ui.viewmodel.CreateWorkPaymentMethodViewModel
import com.amberinternet.oktruck.util.manager.DialogManager
import kotlinx.android.synthetic.main.activity_create_work_payment_method.*
import kotlinx.android.synthetic.main.activity_create_work_payment_method.button_pay
import kotlinx.android.synthetic.main.activity_create_work_payment_method.linearLayout_serviceCharges
import kotlinx.android.synthetic.main.activity_create_work_payment_method.textView_totalFee
import kotlinx.android.synthetic.main.activity_create_work_payment_method.textView_transportationFee
import kotlinx.android.synthetic.main.item_view_additional_service_charge.view.*

class CreateWorkPaymentMethodActivity : BaseActivity() {

    private lateinit var viewModel: CreateWorkPaymentMethodViewModel
    private lateinit var dialogManager: DialogManager
    private lateinit var user: User
    private var work: Work = Work()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_work_payment_method)
        setTitle(R.string.title_top_up)
        getVariable()
        initVariable()
        initView()
        initOnClick()
    }

    private fun getVariable() {
        intent.extras?.let {
            work = it.getParcelable(WORK_KEY)
        }
    }

    private fun initVariable() {
        viewModel = ViewModelProviders.of(this, CreateWorkPaymentMethodViewModelFactory(this)).get(CreateWorkPaymentMethodViewModel::class.java)
        dialogManager = DialogManager.getInstance(supportFragmentManager)
        user = User.instance
        subscription = viewModel.getProfile()
    }

    private fun initView() {
        textView_credit.text = user.getCredit()

        editText_userName.setText(work.userName)
        editText_userTelephone.setText(work.userTelephoneNumber)
        editText_note.setText(work.note)

        for (additionalService in work.additionalServices) {
            // Add additional services to service charges list
            val serviceChargesView = layoutInflater.inflate(R.layout.item_view_additional_service_charge, null)
            serviceChargesView.textView_serviceName.text = additionalService.name
            serviceChargesView.textView_serviceCharge.text = additionalService.getCharge()
            linearLayout_serviceCharges.addView(serviceChargesView)
        }
        textView_transportationFee.text = work.transportationFee
        textView_totalFee.text = work.totalFee
    }

    private fun initOnClick() {
//        linearLayout_creditCardMethod.setOnClickListener {
//            if (validate()) {
//                work.userName = editText_userName.text.toString()
//                work.userTelephoneNumber = editText_userTelephone.text.toString()
//                work.note = editText_note.text.toString()
//                val intent = Intent(this, CreateWorkCreditCardMethodActivity::class.java)
//                intent.putExtra(WORK_KEY, work)
//                startActivity(intent)
//            }
//        }

        linearLayout_walletMethod.setOnClickListener {
            checkBox_wallet.toggle()
            work.userName = editText_userName.text.toString()
            work.userTelephoneNumber = editText_userTelephone.text.toString()
            work.note = editText_note.text.toString()
            if (checkBox_wallet.isChecked) linearLayout_walletMethod.setBackgroundResource(R.drawable.border_selected)
            else linearLayout_walletMethod.setBackgroundResource(R.drawable.bg_selector_white)
        }

        linearLayout_cashMethod.setOnClickListener {
            if (validate()) {
                work.userName = editText_userName.text.toString()
                work.userTelephoneNumber = editText_userTelephone.text.toString()
                work.note = editText_note.text.toString()
                val intent = Intent(this, CreateWorkCashMethodActivity::class.java)
                intent.putExtra(WORK_KEY, work)
                startActivity(intent)
            }
        }

        button_pay.setOnClickListener {
            if (validate()) {
                if (checkBox_wallet.isChecked) {
                    work.userName = editText_userName.text.toString()
                    work.userTelephoneNumber = editText_userTelephone.text.toString()
                    work.note = editText_note.text.toString()
                    subscription = viewModel.createWorkWithWallet(work)
                } else {
                    dialogManager.showAlert(R.string.please_select_payment_method)
                }
            }
        }
    }

    private fun validate(): Boolean {
        return when {
            editText_userName.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_contact_name)
                false
            }
            editText_userTelephone.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_telephone)
                false
            }
            !editText_userTelephone.isValidTelephone() -> {
                dialogManager.showAlert(R.string.invalid_telephone)
                false
            }
            else -> true
        }
    }

}