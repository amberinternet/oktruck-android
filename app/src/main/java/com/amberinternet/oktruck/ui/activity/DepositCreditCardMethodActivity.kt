package com.amberinternet.oktruck.ui.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import co.omise.android.Client
import co.omise.android.TokenRequest
import co.omise.android.TokenRequestListener
import co.omise.android.models.Token
import co.omise.android.ui.AuthorizingPaymentActivity
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.ui.base.BaseActivity
import com.amberinternet.oktruck.ui.factory.DepositCreditCardMethodViewModelFactory
import com.amberinternet.oktruck.ui.viewmodel.DepositCreditCardMethodViewModel
import com.amberinternet.oktruck.util.manager.DialogManager
import kotlinx.android.synthetic.main.activity_deposit_credit_card_method.*
import kotlinx.android.synthetic.main.activity_deposit_payment_method.*
import java.util.*

class DepositCreditCardMethodActivity : BaseActivity() {

    private lateinit var viewModel: DepositCreditCardMethodViewModel
    private lateinit var dialogManager: DialogManager
    private var price: Double = INITIAL_DOUBLE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deposit_credit_card_method)
        setTitle(R.string.title_pay)
        getVariable()
        initView()
        initOnClick()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTHORIZING_PAYMENT_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val url = data!!.getStringExtra(AuthorizingPaymentActivity.EXTRA_RETURNED_URLSTRING)
            subscription = viewModel.completePaymentOmise()
        }
    }

    private fun getVariable() {
        viewModel = ViewModelProviders.of(this, DepositCreditCardMethodViewModelFactory(this)).get(DepositCreditCardMethodViewModel::class.java)
        viewModel.depositOmise.addOnPropertyChanged {
            showAuthorizingPaymentForm(it.get()!!.authorizeUri)
        }
        dialogManager = DialogManager.getInstance(supportFragmentManager)
        intent.extras?.let {
            price = it.getDouble(PRICE_KEY)
        }
    }

    private fun initView() {
        editText_month.afterTextChanged { string ->
            when (string.length) {
                2 -> editText_year.requestFocus()
            }
        }

        editText_year.afterTextChanged { string ->
            when (string.length) {
                0 -> editText_month.requestFocus()
                4 -> hideKeyboard()
            }
        }
    }

    private fun initOnClick() {
        linearLayout_expirationDate.setOnClickListener {
            editText_month.requestFocus()
        }

        button_confirm.setOnClickListener {
            if (validate()) {
                dialogManager.showLoading(R.string.checking_data)
                val client = Client(OMISE_PUBLIC_KEY)
                val request = TokenRequest()
                request.name = editText_cardName.string()
                request.number = editText_cardNumber.string().replace(" ", "")
                request.expirationMonth = editText_month.string().toInt()
                request.expirationYear = editText_year.string().toInt()
                request.securityCode = editText_cvv.string()
                client.send(request, object : TokenRequestListener {
                    override fun onTokenRequestSucceed(p0: TokenRequest?, p1: Token?) {
                        dialogManager.hideLoading()
                        subscription = viewModel.requestDepositWithOmise(price.toInt(), p1?.id!!)
                    }

                    override fun onTokenRequestFailed(p0: TokenRequest?, p1: Throwable?) {
                        dialogManager.hideLoading()
                        dialogManager.showError(p1?.localizedMessage!!)
                        when (p1.localizedMessage) {
                            "expiration date cannot be in the past" -> dialogManager.showAlert(R.string.credit_card_date_expire)
                            "number is invalid", "number is invalid and brand not supported (unknown)" -> dialogManager.showAlert(R.string.invalid_credit_card_number)
                        }
                    }
                })
            }
        }
    }

    private fun showAuthorizingPaymentForm(url: String) {
        var array = arrayOf("${BASE_API_URL}redirector")
        val intent = Intent(this, AuthorizingPaymentActivity::class.java)
        intent.putExtra(AuthorizingPaymentActivity.EXTRA_AUTHORIZED_URLSTRING, url)
        intent.putExtra(AuthorizingPaymentActivity.EXTRA_EXPECTED_RETURN_URLSTRING_PATTERNS, array)
        startActivityForResult(intent, AUTHORIZING_PAYMENT_REQUEST_CODE)
    }

    private fun validate(): Boolean {
        return when {
            editText_cardNumber.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_credit_card_number)
                return false
            }
            editText_month.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_expired_date)
                return false
            }
            editText_month.string().toInt() !in 1..12 -> {
                dialogManager.showAlert(R.string.invalid_date)
                return false
            }
            editText_year.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_expired_date)
                return false
            }
            editText_year.string().toInt() < Calendar.getInstance().getYear() -> {
                dialogManager.showAlert(R.string.invalid_date)
                return false
            }
            editText_cvv.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_cvv)
                return false
            }
            editText_cvv.length() < 3 || editText_cvv.length() > 4 -> {
                dialogManager.showAlert(R.string.invalid_cvv)
                return false
            }
            editText_cardName.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_credit_card_name)
                return false
            }
            else -> true
        }
    }

}