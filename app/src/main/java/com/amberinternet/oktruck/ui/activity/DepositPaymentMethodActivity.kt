package com.amberinternet.oktruck.ui.activity

import android.content.Intent
import android.os.Bundle
import com.amberinternet.oktruck.INITIAL_DOUBLE
import com.amberinternet.oktruck.PRICE_KEY
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.roundDecimalFormat
import com.amberinternet.oktruck.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_deposit_payment_method.*

class DepositPaymentMethodActivity : BaseActivity() {

    private var price: Double = INITIAL_DOUBLE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deposit_payment_method)
        setTitle(R.string.title_top_up)
        getVariable()
        initView()
        initOnClick()
    }

    private fun getVariable() {
        intent.extras?.let {
            price = it.getDouble(PRICE_KEY)
        }
    }

    private fun initView() {
        textView_price.text = price.roundDecimalFormat()
    }

    private fun initOnClick() {
//        linearLayout_creditCardMethod.setOnClickListener {
//            val intent = Intent(this, DepositCreditCardMethodActivity::class.java)
//            intent.putExtra(PRICE_KEY, price)
//            startActivity(intent)
//        }

        linearLayout_transferMethod.setOnClickListener {
            val intent = Intent(this, DepositTransferMethodActivity::class.java)
            intent.putExtra(PRICE_KEY, price)
            startActivity(intent)
        }
    }

}