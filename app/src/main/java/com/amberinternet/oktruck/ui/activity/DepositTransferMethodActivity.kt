package com.amberinternet.oktruck.ui.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProviders
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.adapter.TransactionRequestSpinnerAdapter
import com.amberinternet.oktruck.model.User
import com.amberinternet.oktruck.ui.base.BaseActivity
import com.amberinternet.oktruck.ui.viewmodel.DepositTransferMethodViewModel
import com.amberinternet.oktruck.util.enum.DepositType
import com.amberinternet.oktruck.util.manager.DialogManager
import kotlinx.android.synthetic.main.activity_deposit_transfer_method.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import th.co.growthd.deerandbook.ui.factory.DepositTransferMethodViewModelFactory
import java.io.File
import java.io.FileOutputStream
import java.text.DateFormatSymbols
import java.util.*

class DepositTransferMethodActivity : BaseActivity() {

    private val TYPE_SLIP_IMAGE = 1
    private lateinit var viewModel: DepositTransferMethodViewModel
    private lateinit var dialogManager: DialogManager
    private var price: Double = INITIAL_DOUBLE
    private var slipFile: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deposit_transfer_method)
        setTitle(R.string.title_pay)
        getVariable()
        initView()
        initOnClick()
    }

    private fun getVariable() {
        viewModel = ViewModelProviders.of(this, DepositTransferMethodViewModelFactory(this)).get(DepositTransferMethodViewModel::class.java)
        dialogManager = DialogManager.getInstance(supportFragmentManager)
        intent.extras?.let {
            price = it.getDouble(PRICE_KEY)
        }
    }

    private fun initView() {
        spinner_day.apply {
            adapter = TransactionRequestSpinnerAdapter().apply { stringArray = resources.getStringArray(R.array.day_list) }
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                }
            }
            setSelection(Calendar.getInstance().getDay() - 1)
        }

        spinner_month.apply {
            adapter = TransactionRequestSpinnerAdapter().apply { stringArray = DateFormatSymbols().months }
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                }
            }
            setSelection(Calendar.getInstance().getMonth() - 1)
        }

        val startYear = Calendar.getInstance().apply { time = User.instance.createdAt.fromDateTime() }.getYear()
        val currentYear = Calendar.getInstance().getYear()
        val buddhistYearList = (startYear..currentYear).map { (it + 543).toString() }
        spinner_year.apply {
            adapter = TransactionRequestSpinnerAdapter().apply { stringArray = buddhistYearList.toTypedArray() }
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                }
            }
            setSelection(currentYear - startYear)
        }

        spinner_hour.apply {
            adapter = TransactionRequestSpinnerAdapter().apply { stringArray = resources.getStringArray(R.array.hour_list) }
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                }
            }
            setSelection(Calendar.getInstance().getHour())
        }

        spinner_minute.apply {
            adapter = TransactionRequestSpinnerAdapter().apply { stringArray = resources.getStringArray(R.array.minute_list) }
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                }
            }
            setSelection(Calendar.getInstance().getMinute())
        }
    }

    private fun initOnClick() {
        linearLayout_attachSlip.setOnClickListener {
            if (checkPermission()) {
                EasyImage.openChooserWithGallery(this, getString(R.string.please_select_silp_image), TYPE_SLIP_IMAGE)
            }
        }

        textView_confirm.setOnClickListener {
            if (validate()) {
                val price = price
                val year = spinner_year.selectedItem.toString().toServerYear().toInt()
                val month = spinner_month.selectedItemPosition + 1
                val day = spinner_day.selectedItemPosition + 1
                val hour = spinner_hour.selectedItemPosition
                val minute = spinner_minute.selectedItemPosition
                val payTime = Calendar.getInstance().setDateTime(year, month, day, hour, minute).toDateTimeServerFormat()
                val typeBody = RequestBody.create(MultipartBody.FORM, DepositType.TRANSFER.value.toString())
                val priceBody = RequestBody.create(MultipartBody.FORM, price.toString())
                val payTimeBody = RequestBody.create(MultipartBody.FORM, payTime)
                val slipImage = RequestBody.create(MediaType.parse("multipart/form-data"), slipFile!!.resize(this, 512))
                subscription = viewModel.requestTransferMethod(typeBody, priceBody, payTimeBody, slipImage)
            }
        }
    }

    private fun validate(): Boolean {
        return when (slipFile) {
            null -> {
                dialogManager.showAlert(R.string.please_upload_transfer_slip)
                return false
            }
            else -> true
        }
    }

    private fun checkPermission(): Boolean {
        return if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            requestPermission()
            false
        }
    }

    private fun requestPermission() {
        requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_CODE_WRITE_PERMISSION)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE_WRITE_PERMISSION -> {
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(this, R.string.enable_write_permission, Toast.LENGTH_SHORT).show()
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, object : DefaultCallback() {
            override fun onImagePicked(imageFile: File?, source: EasyImage.ImageSource?, type: Int) {
                applyRotationIfNeeded(imageFile!!)
                slipFile = imageFile
                imageView_slip.setImageBitmap(BitmapFactory.decodeFile(slipFile!!.absolutePath))
                textView_fileName.text = slipFile?.name
            }

            override fun onImagePickerError(e: Exception?, source: EasyImage.ImageSource?, type: Int) {
                Toast.makeText(this@DepositTransferMethodActivity, R.string.upload_image_error, Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun applyRotationIfNeeded(imageFile: File) {
        val exif = ExifInterface(imageFile.absolutePath)
        val exifRotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)
        val rotation = when(exifRotation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> 90
            ExifInterface.ORIENTATION_ROTATE_180 -> 180
            ExifInterface.ORIENTATION_ROTATE_270 -> 270
            else -> 0
        }
        if (rotation == 0) return
        val bitmap = BitmapFactory.decodeFile(imageFile.absolutePath)
        val matrix = Matrix().apply { postRotate(rotation.toFloat()) }
        val rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
        bitmap.recycle()

        lateinit var out: FileOutputStream
        try {
            out = FileOutputStream(imageFile)
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
        } catch (e: Exception) {

        } finally {
            rotatedBitmap.recycle()
            out.close()
        }
    }

}