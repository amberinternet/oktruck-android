package com.amberinternet.oktruck.ui.activity

import android.content.Intent
import android.os.Bundle
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_explanation.*

class ExplanationActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_explanation)
        setTitle(R.string.explanation)
        initOnClick()
    }

    private fun initOnClick() {
        button_cancel.setOnClickListener {
            onBackPressed()
        }
        button_agree.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
    }
}