package com.amberinternet.oktruck.ui.activity

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.adapter.TransactionRequestSpinnerAdapter
import com.amberinternet.oktruck.model.User
import com.amberinternet.oktruck.model.subscriber.FilterTransactionSubscriber
import com.amberinternet.oktruck.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_filter_transaction.*
import org.greenrobot.eventbus.EventBus
import java.text.DateFormatSymbols
import java.util.*

class FilterTransactionActivity : BaseActivity() {

    private lateinit var yearList: List<Int>
    var month: Int = INITIAL_INT
    var year: Int = INITIAL_INT

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter_transaction)
        setTitle(R.string.title_filter_transaction)
        getVariable()
        initView()
        initOnClick()
    }

    private fun getVariable() {
        month = intent.extras.getInt(MONTH_KEY)
        year = intent.extras.getInt(YEAR_KEY)
    }

    private fun initView() {
        spinner_month.apply {
            adapter = TransactionRequestSpinnerAdapter().apply { stringArray =  DateFormatSymbols().months }
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {}
            }
            setSelection(month - 1)
        }

        val startYear = Calendar.getInstance().apply { time = User.instance.createdAt.fromDateTime() }.getYear()
        val currentYear = Calendar.getInstance().getYear()
        yearList = (startYear..currentYear).map { it }
        val buddhistYearList = (startYear..currentYear).map { (it + 543).toString() }
        spinner_year.apply {
            adapter = TransactionRequestSpinnerAdapter().apply { stringArray = buddhistYearList.toTypedArray() }
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {}
            }
            setSelection(year - startYear)
        }
    }

    private fun initOnClick() {
        button_search.setOnClickListener {
            val subscriber = FilterTransactionSubscriber().apply {
                month = spinner_month.selectedItemPosition + 1
                year = yearList[spinner_year.selectedItemPosition]
            }
            EventBus.getDefault().post(subscriber)
            finish()
        }
    }

}