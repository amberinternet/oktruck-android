package com.amberinternet.oktruck.ui.activity

import android.os.Bundle
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.ui.base.BaseActivity
import com.amberinternet.oktruck.util.manager.UserManager
import kotlinx.android.synthetic.main.activity_full_screen_image.*

class FullScreenImageActivity : BaseActivity() {

    lateinit var imagePath: String
    lateinit var imageName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_screen_image)
        getVariable()
        initView()
    }

    private fun getVariable() {
        imagePath = intent.extras.getString(IMAGE_PATH_KEY)
        imageName = intent.extras.getString(IMAGE_NAME_KEY, INITIAL_STRING)
    }

    private fun initView() {
        setTitle(imageName)
        view_photo.setImageUrl(imagePath, UserManager.getInstance(this).token)
    }
}