package com.amberinternet.oktruck.ui.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.ui.base.BaseActivity
import com.amberinternet.oktruck.ui.factory.LogInViewModelFactory
import com.amberinternet.oktruck.ui.viewmodel.LoginViewModel
import com.amberinternet.oktruck.util.manager.DialogManager
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.klinker.android.link_builder.Link
import com.klinker.android.link_builder.applyLinks
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {

    private lateinit var viewModel: LoginViewModel
    private lateinit var dialogManager: DialogManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        viewModel = ViewModelProviders.of(this, LogInViewModelFactory(this)).get(LoginViewModel::class.java)
        viewModel.logInSuccess.addOnPropertyChanged {
            if (it.get()) {
                MainActivity()
            }
        }
        checkGooglePlayService()
        initView()
        initVariable()
        initOnClick()
    }

    private fun checkGooglePlayService() {
        if (!isGooglePlayServicesAvailable()) {
            dialogManager.showError(getString(R.string.update_google_play_services))
        }
    }

    private fun isGooglePlayServicesAvailable(): Boolean {
        val googleApiAvailability = GoogleApiAvailability.getInstance()
        val status = googleApiAvailability.isGooglePlayServicesAvailable(this)
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(this, status, 2404).show()
            }
            return false
        }
        return true
    }

    private fun initView() {
        val termLink = Link(getString(R.string.terms_and_conditions_of_service)).apply {
            textColor = ContextCompat.getColor(this@LoginActivity, android.R.color.white)
            underlined = true
            setOnClickListener {
                val intent = Intent(this@LoginActivity, TermAndConditionActivity::class.java)
                startActivity(intent)
            }
        }

        val privacyLink = Link(getString(R.string.privacy_policy)).apply {
            textColor = ContextCompat.getColor(this@LoginActivity, android.R.color.white)
            underlined = true
            setOnClickListener {
                val intent = Intent(this@LoginActivity, PrivacyPolicyActivity::class.java)
                startActivity(intent)
            }
        }

        accept_message.applyLinks(termLink, privacyLink)
    }

    private fun initVariable() {
        dialogManager = DialogManager.getInstance(supportFragmentManager)
    }

    private fun initOnClick() {
        button_logIn.setOnClickListener {
            if (validate()) {
                logIn()
            }
        }
        button_register.setOnClickListener {
            val intent = Intent(this, ExplanationActivity::class.java)
            startActivity(intent)
        }
        textView_contact.setOnClickListener {
            DialogManager.getInstance(supportFragmentManager).showContactAdmin()
        }
    }

    private fun validate(): Boolean {
        return when {
            editText_email.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_email)
                false
            }
            !editText_email.isValidEmail() -> {
                dialogManager.showAlert(R.string.invalid_email)
                false
            }

            editText_password.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_password)
                false
            }

            else -> true
        }
    }

    private fun logIn() {
        // TODO: getFirebaseToken
        subscription = viewModel.logIn(editText_email.string(), editText_password.string(), "")
    }

    private fun MainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finishAffinity()
    }

}