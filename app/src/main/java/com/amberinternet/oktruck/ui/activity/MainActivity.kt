package com.amberinternet.oktruck.ui.activity

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.amberinternet.oktruck.NAVIGATION_TAB_KEY
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.WORK_KEY
import com.amberinternet.oktruck.model.Work
import com.amberinternet.oktruck.ui.base.BaseActivity
import com.amberinternet.oktruck.ui.fragment.*
import com.amberinternet.oktruck.util.BottomNavigationViewHelper
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    private var temporaryWork: Work? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        hideBackButton()
        getVariable()
        initNavigationView()
        initView()
    }

    private fun getVariable() {
        temporaryWork = intent.getParcelableExtra(WORK_KEY)
    }

    private fun initView() {
        //loading the default fragment
        navigation.selectedItemId = intent.getIntExtra(NAVIGATION_TAB_KEY, R.id.navi_tab_create_work)
    }

    private fun initNavigationView() {
        BottomNavigationViewHelper.setIconSize(navigation, 30, 21, 4.5f, this)
        navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navi_tab_create_work -> replaceFragment(CreateWorkFragment.newInstance(temporaryWork))
                R.id.navi_tab_news -> replaceFragment(NewsFragment.newInstance())
                R.id.navi_tab_my_work -> replaceFragment(MyWorkFragment.newInstance())
                R.id.navi_tab_wallet -> replaceFragment(WalletFragment.newInstance())
                R.id.navi_tab_setting -> replaceFragment(SettingFragment.newInstance())
            }

            true
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.frameLayout_main, fragment)
            .commitNowAllowingStateLoss()
    }

}