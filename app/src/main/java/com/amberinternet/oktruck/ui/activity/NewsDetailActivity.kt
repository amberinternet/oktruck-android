package com.amberinternet.oktruck.ui.activity

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.model.News
import com.amberinternet.oktruck.ui.base.BaseActivity
import com.amberinternet.oktruck.ui.factory.NewsDetailViewModelFactory
import com.amberinternet.oktruck.ui.viewmodel.NewsDetailViewModel
import kotlinx.android.synthetic.main.activity_news_detail.*

class NewsDetailActivity : BaseActivity() {

    private lateinit var viewModel: NewsDetailViewModel
    private lateinit var news: News

    private val htmlTagOpen = "<html>\n" +
            "      <head>\n" +
            "            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
            "            <style type=\"text/css\">\n" +
            "                  @font-face {\n" +
            "                        font-family: \"Tahoma\";\n" +
            "                        src: url(\"file:///android_asset/fonts/Tahoma-Regular.ttf\")\n" +
            "                  }\n" +
            "                  body {\n" +
            "                        font-family: \"Tahoma\"; \n" +
            "                        margin: auto;\n" +
            "                        background: \"#f0f0f0\"; \n" +
            "                        line-height: \"0.85\"; \n" +
            "                        padding-bottom: 16px; \n" +
            "                  } \n" +
            "                  p {\n" +
            "                        font-size: 16px;\n" +
            "                  }\n" +
            "            </style>\n" +
            "      </head>\n" +
            "      <body>"

    private val htmlTagClose = "</body></html>"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_detail)
        setTitle(R.string.title_news_detail)
        getVariable()
        initViewModel()
    }

    override fun onResume() {
        super.onResume()
        if (viewModel.news.get() == null) {
            subscription = viewModel.getNewsDetail(news.id)
        }
    }

    private fun getVariable() {
        intent.extras?.let {
            news = it.getParcelable(NEWS_KEY)
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, NewsDetailViewModelFactory(this)).get(NewsDetailViewModel::class.java)
        viewModel.isLoading.addOnPropertyChanged {
            if (!it.get()) {
                finish()
            }
        }
        viewModel.news.addOnPropertyChanged {
            initNews(it.get()!!)
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initNews(news: News) {
        val styleWidth = "<img"
        val newStyleWidth = "<img style=\"max-width: 100%;\""
        val iframeWidth = "<iframe"
        val newIframeWidth = "<iframe style=\"max-width: 100%; height: auto;\""
        val content = news.fullContent.replace(styleWidth, newStyleWidth).replace(iframeWidth, newIframeWidth)
        textView_title.text = news.title
        textView_date.text = news.postAt.fromDateTime().toDateTimeAppFormat()
        webView.settings.javaScriptEnabled = true
        webView.apply {
            settings.javaScriptEnabled = true
            settings.useWideViewPort = true
//            setInitialScale(1)
            setBackgroundColor(ContextCompat.getColor(context, android.R.color.white))
            loadDataWithBaseURL(null, htmlTagOpen + content + htmlTagClose, "text/html; charset=utf-8", "UTF-8", null)
        }
    }

}