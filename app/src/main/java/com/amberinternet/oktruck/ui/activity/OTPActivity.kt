package com.amberinternet.oktruck.ui.activity

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.isEmpty
import com.amberinternet.oktruck.model.User
import com.amberinternet.oktruck.string
import com.amberinternet.oktruck.ui.base.BaseActivity
import com.amberinternet.oktruck.ui.viewmodel.OTPViewModel
import com.amberinternet.oktruck.util.manager.DialogManager
import kotlinx.android.synthetic.main.activity_otp.*
import th.co.growthd.deerandbook.ui.factory.OTPViewModelFactory

class OTPActivity : BaseActivity() {

    private lateinit var viewModel: OTPViewModel
    private lateinit var dialogManager: DialogManager
    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)
        viewModel = ViewModelProviders.of(this, OTPViewModelFactory(this)).get(OTPViewModel::class.java)
        initVariable()
        initView()
        initOnClick()
    }

    private fun initVariable() {
        dialogManager = DialogManager.getInstance(supportFragmentManager)
        user = User.instance
    }

    private fun initView() {
        textView_otp.text = getString(R.string.title_otp, user.telephoneNumber)
    }

    private fun initOnClick() {

        button_confirm.setOnClickListener {
            if (validate()) {
                viewModel.submit(editText_otp.string())
            }
        }

        textView_request_otp.setOnClickListener {
            viewModel.requestOTP()
        }

    }

    private fun validate(): Boolean {
        return when {
            editText_otp.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_otp)
                false
            }
            else -> true
        }
    }

    override fun onBackPressed() {
        // super.onBackPressed()
    }

}