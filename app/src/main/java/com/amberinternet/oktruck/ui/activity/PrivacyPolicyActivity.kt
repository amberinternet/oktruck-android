package com.amberinternet.oktruck.ui.activity

import android.os.Bundle
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.ui.base.BaseActivity

class PrivacyPolicyActivity: BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy)
        setTitle(R.string.privacy_policy, 20)
    }
}