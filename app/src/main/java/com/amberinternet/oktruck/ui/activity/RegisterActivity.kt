package com.amberinternet.oktruck.ui.activity

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.model.User
import com.amberinternet.oktruck.ui.base.BaseActivity
import com.amberinternet.oktruck.ui.viewmodel.RegisterViewModel
import com.amberinternet.oktruck.util.manager.DialogManager
import kotlinx.android.synthetic.main.activity_register.*
import th.co.growthd.deerandbook.ui.factory.RegisterViewModelFactory

class RegisterActivity : BaseActivity() {


    private lateinit var viewModel: RegisterViewModel
    private lateinit var dialogManager: DialogManager
    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        viewModel = ViewModelProviders.of(this, RegisterViewModelFactory(this)).get(RegisterViewModel::class.java)
        initVariable()
        initOnClick()
    }

    private fun initVariable() {
        dialogManager = DialogManager.getInstance(supportFragmentManager)
        user = User.instance
    }

    private fun initOnClick() {

        imageButton_back.setOnClickListener {
            onBackPressed()
        }

        button_register.setOnClickListener {
            if (validate()) {
                user.firstName = editText_firstName.string()
                user.lastName = editText_lastName.string()
                user.email = editText_email.string()
                user.telephoneNumber = editText_telephone.string()
                user.company.name = editText_company.string()
                user.password = editText_password.string()
                user.confirmPassword = editText_confirm_Password.string()
                viewModel.signUp()
            }
        }

    }

    private fun validate(): Boolean {
        return when {
            editText_firstName.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_first_name)
                false
            }
            editText_lastName.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_last_name)
                false
            }
            editText_email.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_email)
                false
            }
            !editText_email.isValidEmail() -> {
                dialogManager.showAlert(R.string.invalid_email)
                false
            }
            editText_telephone.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_telephone)
                false
            }
            !editText_telephone.isValidTelephone() -> {
                dialogManager.showAlert(R.string.invalid_telephone)
                false
            }
            editText_password.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_password)
                false
            }
            editText_confirm_Password.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_confirm_password)
                false
            }
            editText_confirm_Password.string() != editText_password.string() -> {
                dialogManager.showAlert(R.string.password_not_match)
                false
            }

            else -> true
        }
    }
}