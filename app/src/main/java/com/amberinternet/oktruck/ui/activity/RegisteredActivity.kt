package com.amberinternet.oktruck.ui.activity

import android.content.Intent
import android.os.Bundle
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_registered.*

class RegisteredActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registered)
        initOnClick()
    }

    private fun initOnClick() {
        button_back.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        // super.onBackPressed()
    }

}