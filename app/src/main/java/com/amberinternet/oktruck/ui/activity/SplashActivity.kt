package com.amberinternet.oktruck.ui.activity

import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.model.User
import com.amberinternet.oktruck.ui.base.BaseActivity
import com.amberinternet.oktruck.util.manager.DialogManager
import com.amberinternet.oktruck.util.manager.UserManager
import com.amberinternet.oktruck.util.network.RxNetwork
import com.amberinternet.oktruck.util.network.response.UserResponse
import com.amberinternet.oktruck.util.network.service.UserService
import kotlinx.android.synthetic.main.activity_splash.*
import rx.Observable

class SplashActivity : BaseActivity() {

    lateinit var dialogManager: DialogManager
    lateinit var userManager: UserManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        initVariable()
        initAccount()
    }

    private fun initVariable() {
        userManager = UserManager(this)
        dialogManager = DialogManager.getInstance(supportFragmentManager)
    }

    private fun initAccount() {
        if (userManager.hasToken()) {
            loadingIndicator.visibility = View.VISIBLE
            val frameAnimation = loadingIndicator.drawable as AnimationDrawable
            frameAnimation.start()
            refreshTokenWithStartMainActivity()
        } else {
            loadingIndicator.visibility = View.GONE
            startLogInActivity()
        }
    }

    private fun refreshTokenWithStartMainActivity() {
        var userService = UserService.getInstance(this)
        val observable: Observable<UserResponse> = userService.refreshToken().flatMap {
            UserManager.getInstance(applicationContext).storeAccessToken(it.accessToken)
            return@flatMap userService.getProfile()
        }

        subscription = RxNetwork<UserResponse>(this).request(observable, onSuccess = { response ->
            User.instance.init(response.user)
            if (response.user.isVerify == 1) {
                startTabBarActivity()
            } else {
                dialogManager.showAlert(R.string.alert_user_not_verify)
            }
        }, onFailure = {
            startLogInActivity()
        })
    }

    private fun startTabBarActivity() {
        Handler().postDelayed({
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finishAffinity()
        }, 2000)
    }

    private fun startLogInActivity() {
        Handler().postDelayed({
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finishAffinity()
        }, 2000)
    }

}