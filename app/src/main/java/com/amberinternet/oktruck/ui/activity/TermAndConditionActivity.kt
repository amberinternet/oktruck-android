package com.amberinternet.oktruck.ui.activity

import android.os.Bundle
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.ui.base.BaseActivity

class TermAndConditionActivity: BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms)
        setTitle(R.string.terms_and_conditions_of_service, 20)
    }
}