package com.amberinternet.oktruck.ui.activity

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProviders
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.model.Work
import com.amberinternet.oktruck.ui.base.BaseActivity
import com.amberinternet.oktruck.ui.factory.WorkDetailViewModelFactory
import com.amberinternet.oktruck.ui.viewmodel.WorkDetailViewModel
import com.amberinternet.oktruck.util.enum.WorkStatus
import com.amberinternet.oktruck.util.manager.DialogManager
import com.amberinternet.oktruck.util.manager.UserManager
import kotlinx.android.synthetic.main.activity_work_detail.*
import kotlinx.android.synthetic.main.view_action_bar.*

class WorkDetailActivity : BaseActivity() {

    private lateinit var viewModel: WorkDetailViewModel
    private lateinit var dialogManager: DialogManager
    private lateinit var temporaryWork: Work

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_work_detail)
        initVariable()
        initRefreshLayout()
        initView()
        initOnClick()
    }

    private fun initVariable() {
        dialogManager = DialogManager.getInstance(supportFragmentManager)

        intent.extras?.let {
            temporaryWork = it.getParcelable(WORK_KEY)
        }

        viewModel = ViewModelProviders.of(this, WorkDetailViewModelFactory(this)).get(WorkDetailViewModel::class.java)
        viewModel.work = temporaryWork
        viewModel.status.addOnPropertyChanged {
            initView()
        }
        viewModel.isRefresh.addOnPropertyChanged {
            if (viewModel.work != temporaryWork) {
                temporaryWork = viewModel.work
                initView()
            }
            refreshLayout.isRefreshing = it.get()
        }
    }

    private fun initRefreshLayout() {
        refreshLayout.setColorSchemeColors(resources.getColor(R.color.colorPrimary))
        refreshLayout.setOnRefreshListener {
            viewModel.isRefresh.set(true)
            viewModel.getWorkDetail()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initView() {
        setTitle("${getString(R.string.work)} ${viewModel.work.getReadableWorkId()}")
        when (viewModel.work.status) {
            WorkStatus.FIND_TRUCK.status -> {
                textView_rightButton.text = getString(R.string.button_cancelWork)
                textView_rightButton.visibility = View.VISIBLE
            }
            else -> {
                textView_rightButton.visibility = View.GONE
            }
        }

        textView_userName.text = viewModel.work.userName
        textView_userTelephoneNumber.text = viewModel.work.userTelephoneNumber
        initStatusTextView()
        textView_sourceName.text = viewModel.work.source.name
        textView_sourceContact.text = viewModel.work.source.contactName
        textView_sourceDate.text = viewModel.work.source.contactTelephoneNumber
        if (viewModel.work.source.signatureFileName.isNotEmpty()) {
            imageView_sourceSignature.setImageUrl(viewModel.work.source.getSignatureImagePath(), UserManager.getInstance(this).token)
            imageView_sourceSignature.visibility = View.VISIBLE
        } else {
            imageView_sourceSignature.visibility = View.GONE
        }
        textView_destinationName.text = viewModel.work.destination.name
        textView_destinationContact.text = viewModel.work.destination.contactName
        textView_destinationDate.text = viewModel.work.destination.contactTelephoneNumber
        if (viewModel.work.destination.signatureFileName.isNotEmpty()) {
            imageView_destinationSignature.setImageUrl(viewModel.work.destination.getSignatureImagePath(), UserManager.getInstance(this).token)
            imageView_destinationSignature.visibility = View.VISIBLE
        } else {
            imageView_destinationSignature.visibility = View.GONE
        }
        textView_itemName.text = viewModel.work.item.name
        textView_itemQuantity.text = viewModel.work.item.quantity
        textView_itemWeight.text = viewModel.work.item.weight
        imageView_item.setImageUrl(viewModel.work.item.getImagePath(), UserManager.getInstance(this).token)
        if (viewModel.work.note.isNotEmpty()) { textView_note.text = viewModel.work.note }
        else { textView_note.text = "-" }
        textView_name.text = viewModel.work.truckType.name
        textView_licensePlate.text = viewModel.work.driver?.truck?.getFullLicensePlate()
        textView_driverName.text = viewModel.work.driver?.user?.getFullName()
        when (viewModel.work.status) {
            WorkStatus.COMPLETE.status, WorkStatus.CANCEL.status, WorkStatus.PAYMENT_FAIL.status, WorkStatus.FIND_TRUCK.status -> linearLayout_contactDriver.visibility = View.GONE
            else -> linearLayout_contactDriver.visibility = View.VISIBLE
        }
        if (viewModel.work.additionalServices.isNotEmpty()) {
            textView_additionalServices.text = viewModel.work.additionalServices.first().name
            if (viewModel.work.additionalServices.count() > 1) {
                for (additionalService in viewModel.work.additionalServices.subList(1, viewModel.work.additionalServices.count()-1)) {
                    textView_additionalServices.text = textView_additionalServices.text.toString() + " / ${additionalService.name}"
                }
            }
        } else {
            textView_additionalServices.visibility = View.GONE
        }
        textView_paymentMethod.text = viewModel.work.getReadablePaymentMethod()
        textView_totalFee.text = viewModel.work.totalFee.toDouble().roundDecimalFormat()
    }

    private fun initStatusTextView() {
        textView_status.text = viewModel.work.getReadableWorkStatus()
        when (viewModel.work.status) {
            WorkStatus.COMPLETE.status -> {
                textView_status.setTextColor(getColor(WorkStatus.COMPLETE.statusColor))
            }
            WorkStatus.CANCEL.status, WorkStatus.PAYMENT_FAIL.status -> {
                textView_status.setTextColor(getColor(WorkStatus.CANCEL.statusColor))
            }
            else -> {
                textView_status.setTextColor(getColor(WorkStatus.FIND_TRUCK.statusColor))
            }
        }
    }

    private fun initOnClick() {
        textView_rightButton.setOnClickListener {   // Cancel Button
            subscription = viewModel.cancelWork()
        }

        imageView_item.setOnClickListener {
            val intent = Intent(this, FullScreenImageActivity::class.java)
            intent.putExtra(IMAGE_PATH_KEY, viewModel.work.item.getImagePath())
            intent.putExtra(IMAGE_NAME_KEY, viewModel.work.item.name)
            startActivity(intent)
        }

        linearLayout_contactDriver.setOnClickListener {
            if (checkPermission()) {
                val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel: ${viewModel.work.driver?.user!!.telephoneNumber}"))
                startActivity(intent)
            }
        }

        button_recall.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            intent.putExtra(NAVIGATION_TAB_KEY, R.id.navi_tab_create_work)
            intent.putExtra(WORK_KEY, viewModel.work)
            startActivity(intent)
        }
    }

    private fun checkPermission(): Boolean {
        return if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            requestPermission()
            false
        }
    }

    private fun requestPermission() {
        requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), REQUEST_CODE_CALL_PERMISSION)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE_CALL_PERMISSION -> {
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(this, R.string.enable_call_permission, Toast.LENGTH_SHORT).show()
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

}