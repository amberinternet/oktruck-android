package com.amberinternet.oktruck.ui.base

import android.content.Context
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.amberinternet.oktruck.R
import kotlinx.android.synthetic.main.view_action_bar.*
import rx.Subscription
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


abstract class BaseActivity : AppCompatActivity() {

    var subscription: Subscription? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.run {
            setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM)
            setDisplayShowCustomEnabled(true)
            setCustomView(R.layout.view_action_bar)

            imageButton_back.setOnClickListener { onBackPressed() }
            textView_rightButton.visibility = View.GONE
        }
    }

    override fun setTitle(titleId: Int) {
        textView_title.setText(titleId)
    }

    protected fun setTitle(titleId: Int, size: Int) {
        textView_title.setTextSize(TypedValue.COMPLEX_UNIT_SP, size.toFloat())
        textView_title.setText(titleId)
    }

    protected fun setTitle(title: String) {
        textView_title.setText(title)
    }

    protected fun setTitle(title: String, size: Int) {
        textView_title.setTextSize(TypedValue.COMPLEX_UNIT_SP, size.toFloat())
        textView_title.setText(title)
    }

    protected fun hideBackButton() {
        imageButton_back.setVisibility(View.GONE)
    }

    fun resetRightButton() {
        textView_rightButton.setVisibility(View.GONE)
        textView_rightButton.setOnClickListener(null)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onDestroy() {
        super.onDestroy()
        subscription?.unsubscribe()
    }
}