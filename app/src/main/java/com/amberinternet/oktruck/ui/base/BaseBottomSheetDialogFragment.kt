package com.amberinternet.oktruck.ui.base

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.amberinternet.oktruck.R
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import rx.Subscription

abstract class BaseBottomSheetDialogFragment : BottomSheetDialogFragment() {

    var subscription: Subscription? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)

        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window?.navigationBarColor = resources.getColor(android.R.color.white)

        dialog.setOnShowListener {
            // set height
            val bottomSheetBehavior = BottomSheetBehavior.from(view?.parent as View)
            bottomSheetBehavior.peekHeight = view!!.measuredHeight

            isCancelable = false    // disable dragging
            dialog.window?.findViewById<View>(R.id.touch_outside)?.setOnClickListener { dismiss() }    // set touch outside to dismiss
        }

        return dialog
    }

    override fun onStop() {
        subscription?.unsubscribe()
        if (!this.isStateSaved) {
            dismiss()
        }
        super.onStop()
    }

}