package com.amberinternet.oktruck.ui.base

import android.os.Bundle
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import rx.Subscription

abstract class BaseDialogFragment : DialogFragment() {

    var subscription: Subscription? = null

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        isCancelable = false
        super.onCreate(savedInstanceState)
    }

    override fun onStop() {
        subscription?.unsubscribe()
        if (!this.isStateSaved) {
            dismiss()
        }
        super.onStop()
    }
}