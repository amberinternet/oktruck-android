package com.amberinternet.oktruck.ui.base

import androidx.fragment.app.Fragment
import rx.Subscription

abstract class BaseFragment : Fragment() {

    var subscription: Subscription? = null

    override fun onPause() {
        super.onPause()
        subscription?.unsubscribe()
    }
}