package com.amberinternet.oktruck.ui.bottomsheetdialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.amberinternet.oktruck.ADDITIONAL_SERVICES_KEY
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.SELECTED_ADDITIONAL_SERVICES_KEY
import com.amberinternet.oktruck.adapter.AdditionalServicesAdapter
import com.amberinternet.oktruck.adapter.callback.OnRecyclerTouchListener
import com.amberinternet.oktruck.model.AdditionalService
import com.amberinternet.oktruck.ui.base.BaseBottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_dialog_fragment_additional_services.*
import kotlinx.android.synthetic.main.item_view_additional_service_checklist.view.*

class AdditionalServicesBottomSheetDialogFragment : BaseBottomSheetDialogFragment() {

    private lateinit var additionalServices: Array<AdditionalService>
    private lateinit var isSelectedAdditionalServices: BooleanArray
    lateinit var onSelect: (additionalServices: ArrayList<AdditionalService>) -> Unit?
    var selectedAdditionalServices: ArrayList<AdditionalService> = arrayListOf()

    companion object {
        fun newInstance(additionalServices: Array<AdditionalService>, selectedAdditionalServices: ArrayList<AdditionalService>?): AdditionalServicesBottomSheetDialogFragment {
            val fragment = AdditionalServicesBottomSheetDialogFragment()
            val bundle = Bundle()
            bundle.putParcelableArray(ADDITIONAL_SERVICES_KEY, additionalServices)
            bundle.putParcelableArrayList(SELECTED_ADDITIONAL_SERVICES_KEY, selectedAdditionalServices)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVariable()
    }

    private fun initVariable() {
        arguments?.let {
            additionalServices = it.getParcelableArray(ADDITIONAL_SERVICES_KEY) as Array<AdditionalService>
            selectedAdditionalServices = it.getParcelableArrayList(SELECTED_ADDITIONAL_SERVICES_KEY) ?: arrayListOf()
        }
        isSelectedAdditionalServices = BooleanArray(additionalServices.size)
        for (i in additionalServices.indices) {
            isSelectedAdditionalServices[i] = selectedAdditionalServices.contains(additionalServices[i])
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bottom_sheet_dialog_fragment_additional_services, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = AdditionalServicesAdapter(additionalServices, selectedAdditionalServices)
            addOnItemTouchListener(OnRecyclerTouchListener(context!!) { position ->
                val selectedItemView = findViewHolderForAdapterPosition(position)!!.itemView
                selectedItemView.checkBox.toggle()

                isSelectedAdditionalServices[position] = selectedItemView.checkBox.isChecked
                button_select.isEnabled = true
            })
        }
    }

    private fun initOnClick() {
        button_cancel.setOnClickListener {
            dismiss()
        }

        button_select.setOnClickListener {
            selectedAdditionalServices.clear()
            if (isSelectedAdditionalServices.contains(true)) {
                for (i in additionalServices.indices) {
                    if (isSelectedAdditionalServices[i]) {
                        selectedAdditionalServices.add(additionalServices[i])
                    }
                }
            }
            onSelect.invoke(selectedAdditionalServices)
            dismiss()
        }
    }

}