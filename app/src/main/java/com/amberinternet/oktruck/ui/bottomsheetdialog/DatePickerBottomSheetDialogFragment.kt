package com.amberinternet.oktruck.ui.bottomsheetdialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.ui.base.BaseBottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_dialog_fragment_date_picker.*
import java.util.*
import kotlin.properties.Delegates

class DatePickerBottomSheetDialogFragment : BaseBottomSheetDialogFragment() {

    private lateinit var title: String
    private lateinit var startDate: String
    lateinit var onSelect: (date: String, time: String) -> Unit?
    private var titleRes: Int by Delegates.notNull()
    private var showDateList = mutableListOf<String>()
    private var dateList = mutableListOf<String>()
    private var timeList = mutableListOf<String>()

    var selectedDate: String? = null
    var selectedTime: String? = null

    companion object {
        fun newInstance(titleRes: Int, startDate: String): DatePickerBottomSheetDialogFragment {
            val fragment = DatePickerBottomSheetDialogFragment()
            val bundle = Bundle()
            bundle.putInt(TITLE_RES_KEY, titleRes)
            bundle.putString(DATE_KEY, startDate)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            titleRes = it.getInt(TITLE_RES_KEY)
            title = context!!.getString(titleRes)
            startDate =  it.getString(DATE_KEY)
        }
        initVariable()
    }

    private fun initVariable() {
        timeList.add(resources.getString(R.string.anytime))
        val durationDays: Int
        if (title == resources.getString(R.string.pick_up_date)) {
            if (Calendar.getInstance().getHour() <= 19) {
                showDateList.add("${resources.getString(R.string.today)} (${startDate.fromDate().toDateWithFullDayAppFormat()})")
            } else {
                startDate = Calendar.getInstance().apply {
                    time = startDate.fromDate()
                    add(Calendar.DATE, 1)
                }.time.toDateServerFormat()
                showDateList.add("${resources.getString(R.string.tomorrow)} (${startDate.fromDate().toDateWithFullDayAppFormat()})")
            }

            durationDays = 7
            timeList.addAll(resources.getStringArray(R.array.hour_time_list))
        } else {
            if (startDate.fromDate().toDay().toInt() == Calendar.getInstance().getDay()) {
                showDateList.add("${resources.getString(R.string.today)} (${startDate.fromDate().toDateWithFullDayAppFormat()})")
            } else {
                showDateList.add(startDate.fromDate().toDateWithFullDayAppFormat())
            }

            durationDays = 2
            timeList.addAll(resources.getStringArray(R.array.duration_time_list))
        }

        dateList.add(startDate)
        for (i in 1..durationDays) {
            val tomorrow = Calendar.getInstance().apply {
                time = startDate.fromDate()
                add(Calendar.DATE, 1)
            }.time.toDateServerFormat()
            startDate = tomorrow
            dateList.add(startDate)
            showDateList.add(startDate.fromDate().toDateWithFullDayAppFormat())
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bottom_sheet_dialog_fragment_date_picker, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        wheelPicker_date.apply {
            data = showDateList
            setOnItemSelectedListener { picker, data, position ->
                selectedDate = dateList[position]

                if (title == resources.getString(R.string.pick_up_date)) {
                    if (position == 0) {
                        val startHour = if (Calendar.getInstance().getHour() <= 19) {
                            Calendar.getInstance().getHour() + 4
                        } else {
                            Calendar.getInstance().getHour() - 20
                        }
                        wheelPicker_time.data = mutableListOf(timeList[0]) + timeList.subList(startHour+1, timeList.size)
                    } else {
                        wheelPicker_time.data = timeList
                    }
                }
            }
            selectedDate = dateList[0]
        }

        wheelPicker_time.apply {
            if (title == resources.getString(R.string.pick_up_date)) {
                val startHour = if (Calendar.getInstance().getHour() <= 19) {
                    Calendar.getInstance().getHour() + 4
                } else {
                    Calendar.getInstance().getHour() - 20
                }
                data = mutableListOf(timeList[0]) + timeList.subList(startHour+1, timeList.size)
            } else {
                wheelPicker_time.data = timeList
            }
            setOnItemSelectedListener { picker, data, position ->
                selectedTime = if (position == 0) "เวลาใดก็ได้" else timeList[position]
            }
            selectedTime = "เวลาใดก็ได้"
        }
    }

    private fun initOnClick() {
        button_cancel.setOnClickListener {
            dismiss()
        }

        button_select.setOnClickListener {
            onSelect.invoke(selectedDate!!, selectedTime!!)
            dismiss()
        }
    }

}