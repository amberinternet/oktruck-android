package com.amberinternet.oktruck.ui.bottomsheetdialog

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.amberinternet.oktruck.INITIAL_DOUBLE
import com.amberinternet.oktruck.INITIAL_INT
import com.amberinternet.oktruck.PRICE_KEY
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.adapter.CreditAdapter
import com.amberinternet.oktruck.adapter.callback.OnRecyclerTouchListener
import com.amberinternet.oktruck.ui.activity.DepositPaymentMethodActivity
import com.amberinternet.oktruck.ui.base.BaseBottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_dialog_fragment_deposit.*

class DepositBottomSheetDialogFragment : BaseBottomSheetDialogFragment() {

    private lateinit var priceArray: IntArray
    private var selectedPrice: Double = INITIAL_DOUBLE

    companion object {
        fun newInstance(): DepositBottomSheetDialogFragment {
            return DepositBottomSheetDialogFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVariable()
    }

    private fun initVariable() {
        priceArray = resources.getIntArray(R.array.deposit_amount_list)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bottom_sheet_dialog_fragment_deposit, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        button_select.isEnabled = false
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = CreditAdapter(priceArray)
            addOnItemTouchListener(OnRecyclerTouchListener(context!!) { position ->
                selectedPrice = priceArray[position].toDouble()
                button_select.isEnabled = true

                val selectedItemView = findViewHolderForAdapterPosition(position)!!.itemView
                selectedItemView.isSelected = true
                for (i in 0 until childCount) {
                    if (i != position) {
                        val otherView = findViewHolderForAdapterPosition(i)!!.itemView
                        otherView.isSelected = false
                    }
                }
            })
        }
    }

    private fun initOnClick() {
        button_cancel.setOnClickListener { dismiss() }

        button_select.setOnClickListener {
            dismiss()
            val intent = Intent(activity, DepositPaymentMethodActivity::class.java)
            intent.putExtra(PRICE_KEY, selectedPrice)
            startActivity(intent)
        }
    }

}