package com.amberinternet.oktruck.ui.bottomsheetdialog

import android.annotation.SuppressLint
import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.adapter.callback.OnRecyclerTouchListener
import com.amberinternet.oktruck.ui.base.BaseBottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_dialog_fragment_image_picker.*
import pl.aprilapps.easyphotopicker.DefaultCallback
import android.content.Intent
import android.database.Cursor
import android.graphics.Matrix
import android.media.ExifInterface
import android.os.AsyncTask
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import com.amberinternet.oktruck.adapter.ImagesAdapter
import pl.aprilapps.easyphotopicker.EasyImage
import java.io.File
import java.io.FileOutputStream
import java.util.*

class ImagePickerBottomSheetDialogFragment : BaseBottomSheetDialogFragment() {

    private val ITEM_IMAGE = 1
    private var imagePathList: ArrayList<String>? = null
    private var imagesBitmap = ArrayList<Bitmap>()
    lateinit var onSelect: (imageUri: Uri) -> Unit

    companion object {
        fun newInstance(): ImagePickerBottomSheetDialogFragment {
            return ImagePickerBottomSheetDialogFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bottom_sheet_dialog_fragment_image_picker, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initVariable()
        initView()
        initOnClick()
    }

    private fun initVariable() {
        LoadPhotosTask().execute()
    }

    private fun initView() {
        recyclerView_images.apply {
            addItemDecoration(
                DividerItemDecoration(
                    activity,
                    DividerItemDecoration.VERTICAL
                )
            ) // add line between item
            layoutManager = LinearLayoutManager(
                activity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            itemAnimator = DefaultItemAnimator()
            isNestedScrollingEnabled = false
            adapter = ImagesAdapter(imagesBitmap)
            addOnItemTouchListener(OnRecyclerTouchListener(context!!) { position ->
                if (position == 0) {
                    EasyImage.openCamera(this@ImagePickerBottomSheetDialogFragment, ITEM_IMAGE)
                } else {
                    onSelect.invoke(Uri.fromFile(File(imagePathList!![position-1])))
                    dismiss()
                }
            })
        }
    }

    private fun initOnClick() {
        relativeLayout_allImages.setOnClickListener {
            EasyImage.openChooserWithGallery(this, getString(R.string.please_select_item_image), ITEM_IMAGE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EasyImage.handleActivityResult(requestCode, resultCode, data, activity, object : DefaultCallback() {
            override fun onImagePicked(imageFile: File?, source: EasyImage.ImageSource?, type: Int) {
                applyRotationIfNeeded(imageFile!!)
                onSelect.invoke(Uri.fromFile(imageFile))
                dismiss()
            }

            override fun onImagePickerError(e: Exception?, source: EasyImage.ImageSource?, type: Int) {
                Toast.makeText(context, R.string.upload_image_error, Toast.LENGTH_LONG).show()
            }
        })
    }

    @SuppressLint("StaticFieldLeak")
    private inner class LoadPhotosTask : AsyncTask<Void, Int, Void>() {
        override fun onPreExecute() {
            super.onPreExecute()
            relativeLayout_loadingImages.visibility = View.VISIBLE
        }

        override fun doInBackground(vararg params: Void): Void? {
            // get path of all photos
            imagePathList = getAllShownImagesPath(activity!!)
            imagePathList!!.reverse()

            // show 10 recently photos
            var i = 0
            while (i < 5 && i < imagePathList!!.size) {
                val photoPath = imagePathList!![i]

                //load photo
                val bitmap = BitmapFactory.decodeFile(photoPath)

                if (bitmap != null) {
                    //create thumbnail
                    imagesBitmap.add(bitmap)
                } else {
                    //remove cannot load photo
                    imagePathList!!.removeAt(i)
                    i--
                }
                i++
            }

            Log.i("imagesData", imagePathList!!.size.toString())
            return null
        }

        override fun onPostExecute(aVoid: Void?) {
            super.onPostExecute(aVoid)
            recyclerView_images.adapter = ImagesAdapter(imagesBitmap)
            relativeLayout_loadingImages.visibility = View.GONE
        }
    }

    private fun getAllShownImagesPath(activity: Activity): ArrayList<String> {
        val uri: Uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val cursor: Cursor?
        val column_index_data: Int
        val column_index_folder_name: Int
        val listOfAllImages = ArrayList<String>()
        var absolutePathOfImage: String? = null

        val projection =
            arrayOf(MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME)

        cursor = activity.contentResolver.query(uri, projection, null, null, null)

        column_index_data = cursor!!.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
        column_index_folder_name = cursor
            .getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME)
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data)

            listOfAllImages.add(absolutePathOfImage)
        }
        return listOfAllImages
    }

    private fun applyRotationIfNeeded(imageFile: File) {
        val exif = ExifInterface(imageFile.absolutePath)
        val exifRotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)
        val rotation = when(exifRotation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> 90
            ExifInterface.ORIENTATION_ROTATE_180 -> 180
            ExifInterface.ORIENTATION_ROTATE_270 -> 270
            else -> 0
        }
        if (rotation == 0) return
        val bitmap = BitmapFactory.decodeFile(imageFile.absolutePath)
        val matrix = Matrix().apply { postRotate(rotation.toFloat()) }
        val rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
        bitmap.recycle()

        lateinit var out: FileOutputStream
        try {
            out = FileOutputStream(imageFile)
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
        } catch (e: Exception) {

        } finally {
            rotatedBitmap.recycle()
            out.close()
        }
    }

}