package com.amberinternet.oktruck.ui.bottomsheetdialog

import android.Manifest
import android.content.ContentValues.TAG
import android.content.Context
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.drawable.AnimationDrawable
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.adapter.DestinationAddressHistoryAdapter
import com.amberinternet.oktruck.adapter.PlaceAutocompleteRecyclerViewAdapter
import com.amberinternet.oktruck.adapter.SourceAddressHistoryAdapter
import com.amberinternet.oktruck.adapter.callback.OnRecyclerTouchListener
import com.amberinternet.oktruck.model.Destination
import com.amberinternet.oktruck.model.Source
import com.amberinternet.oktruck.ui.base.BaseBottomSheetDialogFragment
import com.amberinternet.oktruck.util.manager.DialogManager
import com.amberinternet.oktruck.util.manager.UserManager
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.libraries.places.compat.GeoDataClient
import com.google.android.libraries.places.compat.Places
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import kotlinx.android.synthetic.main.bottom_sheet_dialog_fragment_search_address.*
import kotlinx.android.synthetic.main.bottom_sheet_dialog_fragment_search_address.loadingIndicator
import kotlinx.android.synthetic.main.bottom_sheet_dialog_fragment_search_address.textView_title
import kotlinx.android.synthetic.main.view_select_address.*
import java.io.IOException
import java.util.*
import kotlin.properties.Delegates

class SearchAddressBottomSheetDialogFragment : BaseBottomSheetDialogFragment() {

    private lateinit var title: String
    lateinit var onCompleteSource: (source: Source) -> Unit?
    lateinit var onCompleteDestination: (destination: Destination) -> Unit?
    private lateinit var userManager: UserManager
    private lateinit var dialogManager: DialogManager
    private lateinit var inputMethodManager: InputMethodManager
    private var titleRes: Int by Delegates.notNull()

    private val REQUEST_CHECK_SETTINGS = 0x1
    private var googleApiClient: GoogleApiClient? = null

    private val BOUNDS_GREATER_BANGKOK = LatLngBounds(LatLng(13.407500, 99.810528), LatLng(14.315028, 100.996722))
    private lateinit var geoDataClient: GeoDataClient
    private lateinit var locationManager: LocationManager
    private lateinit var placeAutocompleteAdapter: PlaceAutocompleteRecyclerViewAdapter

    private var selectedPoint: LatLng? = null

    private var selectedSource: Source? = null
    private var selectedDestination: Destination? = null

    companion object {
        fun newInstance(titleRes: Int): SearchAddressBottomSheetDialogFragment {
            val fragment = SearchAddressBottomSheetDialogFragment()
            val bundle = Bundle()
            bundle.putInt(TITLE_RES_KEY, titleRes)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            titleRes = it.getInt(TITLE_RES_KEY)
            title = context!!.getString(titleRes)
        }
        initVariable()
    }

    private fun initVariable() {
        userManager = UserManager(context!!)
        dialogManager = DialogManager.getInstance(childFragmentManager)

        // Construct a InputMethodManager (soft keyboard)
        inputMethodManager = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        initGoogleAPIClient()
        // Construct a GeoDataClient for the Google Places API for Android.
        geoDataClient = Places.getGeoDataClient(activity!!)
        // Construct a LocationManager for get current location.
        locationManager = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    private fun initGoogleAPIClient() {
        //Without Google API Client Auto Location Dialog will not work
        googleApiClient = GoogleApiClient.Builder(activity!!).addApi(LocationServices.API).build()
        googleApiClient!!.connect()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bottom_sheet_dialog_fragment_search_address, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mapView.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        textView_title.text = title
        initHistoryView()
        initSearchView()
        initSelectAddressView()
    }

    private fun initHistoryView() {
        recyclerView_addressHistory.apply {
            layoutManager = LinearLayoutManager(activity)
            itemAnimator = DefaultItemAnimator()
            isNestedScrollingEnabled = false

            if (title == resources.getString(R.string.title_source)) {
                val addressHistoryList = userManager.getSourceAddressHistoryList()
                adapter = SourceAddressHistoryAdapter(addressHistoryList)
                addOnItemTouchListener(OnRecyclerTouchListener(context!!) { position ->
                    val address = addressHistoryList[position]
                    onCompleteSource.invoke(address)
                    dismiss()
                })
            } else {
                val addressHistoryList = userManager.getDestinationAddressHistoryList()
                adapter = DestinationAddressHistoryAdapter(addressHistoryList)
                addOnItemTouchListener(OnRecyclerTouchListener(context!!) { position ->
                    val address = addressHistoryList[position]
                    onCompleteDestination.invoke(address)
                    dismiss()
                })
            }
        }
    }

    private fun initSearchView() {
        // Set up the adapter that will retrieve suggestions from the Places Geo Data Client.
        placeAutocompleteAdapter = PlaceAutocompleteRecyclerViewAdapter(activity, geoDataClient, BOUNDS_GREATER_BANGKOK, null)

        recyclerView_placeAutocomplete.apply {
            layoutManager = LinearLayoutManager(activity)
            itemAnimator = DefaultItemAnimator()
            isNestedScrollingEnabled = false
            adapter = placeAutocompleteAdapter
            addOnItemTouchListener(OnRecyclerTouchListener(context!!) { position ->
                val item = placeAutocompleteAdapter.getItem(position)
                val placeId = item.placeId
                val locationName = item.getPrimaryText(null).toString()

                // get place by id
                geoDataClient.getPlaceById(placeId)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            val places = task.result
                            val place = places!!.get(0)
                            selectedPoint = place.latLng

                            mapView.getMapAsync(OnMapReadyCallback())

                            Log.i(TAG, "Place found at " + place.latLng.toString())
                            places.release()
                        } else {
                            Log.i(TAG, "Place not found.")
                        }
                    }

                //set EditText & change view
                editText_searchAddress.setText(locationName)

                linearLayout_currentLocation.visibility = View.GONE
                recyclerView_addressHistory.visibility = View.GONE
                recyclerView_placeAutocomplete.visibility = View.GONE
                view_selectAddress.visibility = View.VISIBLE

                //hide soft keyboard
                inputMethodManager.hideSoftInputFromWindow(recyclerView_placeAutocomplete.windowToken, 0)

                Log.i(TAG, "Clicked: " + item.getPrimaryText(null))
                Log.i(TAG, "Called getPlaceById to get Place details for " + item.placeId!!)
            })

            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                        //hide soft keyboard
                        inputMethodManager.hideSoftInputFromWindow(recyclerView.getWindowToken(), 0)
                    }
                }
            })
        }

        editText_searchAddress.apply {
            addTextChangedListener(object : TextWatcher {
                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    placeAutocompleteAdapter.filter.filter(s.toString())
                    recyclerView_placeAutocomplete.adapter = placeAutocompleteAdapter
                    recyclerView_placeAutocomplete.visibility = View.VISIBLE

                    recyclerView_addressHistory.visibility = View.GONE
                }

                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
                override fun afterTextChanged(s: Editable) {}
            })
            setOnKeyListener(View.OnKeyListener { view, keyCode, event ->
                if (keyCode == KeyEvent.KEYCODE_ENTER) {

                    //change view
                    linearLayout_currentLocation.visibility = View.VISIBLE
                    recyclerView_addressHistory.visibility = View.GONE
                    recyclerView_placeAutocomplete.visibility = View.VISIBLE
                    view_selectAddress.visibility = View.GONE

                    //hide soft keyboard
                    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
                    return@OnKeyListener true
                }
                false
            })
        }
    }

    private fun initSelectAddressView() {

    }

    private fun initOnClick() {
        button_cancel.setOnClickListener { dismiss() }

        // clear search address EditText
        button_clearText.setOnClickListener {
            editText_searchAddress.text.clear()

            //change view
            linearLayout_currentLocation.visibility = View.VISIBLE
            recyclerView_addressHistory.visibility = View.VISIBLE
            recyclerView_placeAutocomplete.visibility = View.GONE
            view_selectAddress.visibility = View.GONE

            //show soft keyboard
            inputMethodManager.showSoftInput(editText_searchAddress, InputMethodManager.SHOW_IMPLICIT)
        }

        linearLayout_currentLocation.setOnClickListener {
            //Check Location Permission
            if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                checkPermissions()
                return@setOnClickListener
            }
            //Check GPS is turned ON or OFF
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                Log.i("About GPS", "GPS is Enabled in your device")
            } else {
                //If GPS turned OFF show Location Setting Dialog
                Handler().postDelayed({ showSettingDialog() }, 10)
                Log.e("About GPS", "GPS is Disabled in your device")
                return@setOnClickListener
            }

            // get current location
            locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, LocationListener(),null)
            val providers = locationManager.getProviders(true)
            var bestLocation: Location? = null

            loadingIndicator.setColorFilter(ContextCompat.getColor(context!!, R.color.greyTextColor), android.graphics.PorterDuff.Mode.SRC_IN)
            val frameAnimation: AnimationDrawable = loadingIndicator.drawable as AnimationDrawable
            frameAnimation.start()
            loadingIndicator.visibility = View.VISIBLE

            do {
                for (provider in providers) {
                    val location = locationManager.getLastKnownLocation(provider) ?: continue
                    if (bestLocation == null || location.accuracy < bestLocation.accuracy) {
                        // Found best last known location
                        bestLocation = location
                    }
                }
            } while (bestLocation == null)

            loadingIndicator.clearAnimation()
            loadingIndicator.visibility = View.GONE

            selectedPoint = LatLng(bestLocation.latitude, bestLocation.longitude)
            Log.i(TAG, selectedPoint.toString())

            // marker on map view
            mapView.getMapAsync(OnMapReadyCallback())

            //set EditText
            // get place by point
            val geocoder = Geocoder(activity, Locale.getDefault())
            try {
                val addresses =
                    geocoder.getFromLocation(selectedPoint!!.latitude, selectedPoint!!.longitude, 1)
                if (addresses.isEmpty()) {
                    Toast.makeText(activity, getText(R.string.warning_valid_current_location), Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
                val address = addresses[0].getAddressLine(0)
                editText_searchAddress.setText(address)
            } catch (e: IOException) {
                e.printStackTrace()
            }


            //change view
            linearLayout_currentLocation.visibility = View.GONE
            recyclerView_addressHistory.visibility = View.GONE
            recyclerView_placeAutocomplete.visibility = View.GONE
            view_selectAddress.visibility = View.VISIBLE

            //hide soft keyboard
            inputMethodManager.hideSoftInputFromWindow(view!!.windowToken, 0)
        }

        linearLayout_place.setOnClickListener {
            editText_placeName.requestFocus()
            //show soft keyboard
            inputMethodManager.showSoftInput(editText_placeName, InputMethodManager.SHOW_IMPLICIT)
        }

        linearLayout_landmark.setOnClickListener {
            editText_landmark.requestFocus()
            //show soft keyboard
            inputMethodManager.showSoftInput(editText_landmark, InputMethodManager.SHOW_IMPLICIT)
        }

        linearLayout_contact.setOnClickListener {
            editText_contactName.requestFocus()
            //show soft keyboard
            inputMethodManager.showSoftInput(editText_contactName, InputMethodManager.SHOW_IMPLICIT)
        }

        linearLayout_tel.setOnClickListener {
            editText_contactTelephone.requestFocus()
            //show soft keyboard
            inputMethodManager.showSoftInput(editText_contactTelephone, InputMethodManager.SHOW_IMPLICIT)
        }

        button_select.setOnClickListener {
            if (validate()) {
                // get place by point
                val geocoder = Geocoder(activity, Locale.getDefault())
                try {
                    val addresses = geocoder.getFromLocation(
                        selectedPoint!!.latitude,
                        selectedPoint!!.longitude,
                        1
                    )

                    val address = addresses[0]

                    val fullAddress = address.getAddressLine(0)
                    val subdistrict: String
                    val district: String
                    val province: String

                    if (address.adminArea == "กรุงเทพมหานคร" || address.adminArea == "Krung Thep Maha Nakhon") {
                        province = address.adminArea ?: "-"
                        district = address.subLocality ?: "-"

                        subdistrict = if (!address.thoroughfare.isNullOrEmpty()) {
                            fullAddress.split(district).first().split(address.thoroughfare).last().trim { it <= ' ' }
                        } else {
                            fullAddress.split(district).first()
                        }
                    } else {
                        province = address.adminArea ?: "-"
                        district = address.subAdminArea ?: "-"
                        subdistrict = address.locality ?: address.subLocality ?: "-"
                    }

                    println("address: ${address}")
                    
                    println("fullAddress: ${fullAddress}")
                    println("subdistrict: ${subdistrict}")
                    println("district: ${district}")
                    println("province: ${province}")

                    if (title == resources.getString(R.string.title_source)) {
                        selectedSource = Source()
                        selectedSource?.let {
                            it.name = editText_placeName.text.toString()
                            it.landmark = editText_landmark.text.toString()
                            it.contactName = editText_contactName.text.toString()
                            it.contactTelephoneNumber = editText_contactTelephone.text.toString()

                            it.address = fullAddress
                            it.subdistrict = subdistrict
                            it.district = district
                            it.province = province

                            it.latitude = selectedPoint!!.latitude
                            it.longitude = selectedPoint!!.longitude

                            onCompleteSource.invoke(it)
                        }
                    } else {
                        selectedDestination = Destination()
                        selectedDestination?.let {
                            it.name = editText_placeName.text.toString()
                            it.landmark = editText_landmark.text.toString()
                            it.contactName = editText_contactName.text.toString()
                            it.contactTelephoneNumber = editText_contactTelephone.text.toString()

                            it.address = fullAddress
                            it.subdistrict = subdistrict
                            it.district = district
                            it.province = province

                            it.latitude = selectedPoint!!.latitude
                            it.longitude = selectedPoint!!.longitude

                            onCompleteDestination.invoke(it)
                        }
                    }

                    dismiss()

                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun validate(): Boolean {
        return when {
            editText_placeName.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_place_name)
                false
            }
            editText_contactName.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_contact_name)
                false
            }
            editText_contactTelephone.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_contact_telephone)
                false
            }
            !editText_contactTelephone.isValidTelephone() -> {
                dialogManager.showAlert(R.string.invalid_telephone)
                false
            }
            else -> true
        }
    }

    /* On Request permission method to check the permission is granted or not for Marshmallow+ Devices  */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_LOCATION_PERMISSION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    //If permission granted show location dialog if APIClient is not null
                    if (googleApiClient == null) {
                        initGoogleAPIClient()
                        showSettingDialog()
                    } else {
                        showSettingDialog()
                    }

                } else {
                    Toast.makeText(activity, "Location Permission denied.", Toast.LENGTH_SHORT)
                        .show()
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }
        }
    }

    /* Check Location Permission for Marshmallow Devices */
    private fun checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                requestLocationPermission()
            else
                showSettingDialog()
        } else
            showSettingDialog()
    }

    /* Show Popup to access User Permission  */
    private fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                activity!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            ActivityCompat.requestPermissions(
                activity!!,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_CODE_LOCATION_PERMISSION
            )

        } else {
            ActivityCompat.requestPermissions(
                activity!!,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_CODE_LOCATION_PERMISSION
            )
        }
    }

    /* Show Location Access Dialog */
    private fun showSettingDialog() {
        val locationRequest = LocationRequest.create()
        locationRequest.priority =
            LocationRequest.PRIORITY_HIGH_ACCURACY//Setting priority of Location request to high
        locationRequest.interval = (30 * 1000).toLong()
        locationRequest.fastestInterval =
            (5 * 1000).toLong()//5 sec Time interval for location update
        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)
        builder.setAlwaysShow(true) //this is the key ingredient to show dialog always when GPS is off

        val result =
            LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
        result.setResultCallback { result ->
            val status = result.status
            val state = result.locationSettingsStates
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                    // Location settings are not satisfied. But could be fixed by showing the user
                    // a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS)
                    } catch (e: IntentSender.SendIntentException) {
                        e.printStackTrace()
                        // Ignore the error.
                    }

                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }// All location settings are satisfied. The client can initialize location
            // requests here.
            // Location settings are not satisfied. However, we have no way to fix the
            // settings so we won't show the dialog.
        }
    }

    override fun onResume() {
        mapView.onResume()
        super.onResume()
    }

    override fun onPause() {
        mapView.onPause()
        super.onPause()
    }

//    override fun onDestroy() {
//        mapView.onDestroy()
//        super.onDestroy()
//    }

    override fun onLowMemory() {
        mapView.onLowMemory()
        super.onLowMemory()
    }

    inner class LocationListener : android.location.LocationListener {
        override fun onLocationChanged(location: Location) {
            // your code and required methods go here...
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {

        }

        override fun onProviderEnabled(provider: String) {

        }

        override fun onProviderDisabled(provider: String) {

        }
    }

    inner class OnMapReadyCallback : com.google.android.gms.maps.OnMapReadyCallback {
        private var map: GoogleMap? = null

        override fun onMapReady(googleMap: GoogleMap) {
            map = googleMap
            map!!.uiSettings.isMyLocationButtonEnabled = false

            //Check Location Permission
            if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                checkPermissions()
            } else
                map!!.isMyLocationEnabled = true

            map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(selectedPoint, 16f))

            map!!.setOnMapClickListener {
                //hide soft keyboard
                inputMethodManager.hideSoftInputFromWindow(mapView.windowToken, 0)
            }

            map!!.setOnCameraMoveStartedListener { reason ->
                if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
                    //hide soft keyboard
                    inputMethodManager.hideSoftInputFromWindow(mapView.windowToken, 0)
                }
            }

            map!!.setOnCameraIdleListener(GoogleMap.OnCameraIdleListener {
                val newSelectedPoint = map!!.cameraPosition.target

                // get place by point
                val geocoder = Geocoder(context, Locale.getDefault())
                try {
                    val addresses = geocoder.getFromLocation(
                        newSelectedPoint.latitude,
                        newSelectedPoint.longitude,
                        1
                    )

                    // if valid location
                    if (addresses.isEmpty()) {
                        editText_searchAddress.setText(getString(R.string.warning_valid_location))
                        Toast.makeText(
                            context,
                            R.string.warning_select_new_location,
                            Toast.LENGTH_LONG
                        ).show()
                        Handler().postDelayed({
                            // move map camera to previous point after 3s = 3000ms
                            map!!.animateCamera(CameraUpdateFactory.newLatLng(selectedPoint))
                        }, 3000)
                        return@OnCameraIdleListener
                    } else {
                        selectedPoint = newSelectedPoint
                    }

                    val address = addresses[0].getAddressLine(0)
                    editText_searchAddress.setText(address)
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                println(selectedPoint)
            })
        }
    }

}