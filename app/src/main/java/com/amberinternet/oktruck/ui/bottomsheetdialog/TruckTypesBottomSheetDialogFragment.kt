package com.amberinternet.oktruck.ui.bottomsheetdialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.TRUCK_TYPES_KEY
import com.amberinternet.oktruck.TRUCK_TYPE_KEY
import com.amberinternet.oktruck.adapter.TruckTypesAdapter
import com.amberinternet.oktruck.adapter.callback.OnRecyclerTouchListener
import com.amberinternet.oktruck.model.TruckType
import com.amberinternet.oktruck.ui.base.BaseBottomSheetDialogFragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.bottom_sheet_dialog_fragment_truck_types.*

class TruckTypesBottomSheetDialogFragment : BaseBottomSheetDialogFragment() {

    private lateinit var truckTypes: Array<TruckType>
    lateinit var onSelect: (truckType: TruckType) -> Unit?
    var selectedTruckType: TruckType? = null

    companion object {
        fun newInstance(truckTypes: Array<TruckType>, selectedTruckType: TruckType?): TruckTypesBottomSheetDialogFragment {
            val fragment = TruckTypesBottomSheetDialogFragment()
            val bundle = Bundle()
            bundle.putParcelableArray(TRUCK_TYPES_KEY, truckTypes)
            bundle.putParcelable(TRUCK_TYPE_KEY, selectedTruckType)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVariable()
    }

    private fun initVariable() {
        arguments?.let {
            truckTypes = it.getParcelableArray(TRUCK_TYPES_KEY) as Array<TruckType>
            selectedTruckType = it.getParcelable(TRUCK_TYPE_KEY)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bottom_sheet_dialog_fragment_truck_types, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        button_select.isEnabled = (selectedTruckType != null)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = TruckTypesAdapter(truckTypes, selectedTruckType)
            addOnItemTouchListener(OnRecyclerTouchListener(context!!) { position ->
                selectedTruckType = truckTypes[position]
                button_select.isEnabled = true

                val selectedItemView = findViewHolderForAdapterPosition(position)!!.itemView
                selectedItemView.isSelected = true
                for (i in 0 until childCount) {
                    if (i != position) {
                        val otherView = findViewHolderForAdapterPosition(i)!!.itemView
                        otherView.isSelected = false
                    }
                }
            })
        }
    }

    private fun initOnClick() {
        button_cancel.setOnClickListener {
            dismiss()
        }

        button_select.setOnClickListener {
            selectedTruckType?.let(onSelect)
            dismiss()
        }
    }

}