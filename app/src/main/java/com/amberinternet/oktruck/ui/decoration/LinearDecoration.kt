package com.amberinternet.oktruck.ui.decoration

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class LinearDecoration(private val verticalSpace: Int, private val horizontalSpace: Int = 0, private val topSpace: Int = 0) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {

        if (parent.getChildLayoutPosition(view) == 0) {
            outRect.top = topSpace
        }

        outRect.left = horizontalSpace
        outRect.right = horizontalSpace

        if (parent.getChildLayoutPosition(view) == ((parent.adapter?.itemCount?: 0) - 1)) {
            outRect.bottom = 0
        } else {
            outRect.bottom = verticalSpace
        }

    }
}
