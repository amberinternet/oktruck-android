package com.amberinternet.oktruck.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.databinding.DialogFragmentAlertBinding
import com.amberinternet.oktruck.ui.base.BaseDialogFragment
import kotlin.properties.Delegates

class AlertDialogFragment : BaseDialogFragment() {

    lateinit var binding: DialogFragmentAlertBinding
    private var titleRes: Int by Delegates.notNull()
    private var messageRes: Int by Delegates.notNull()
    private var actionRes: Int by Delegates.notNull()
    var onDismiss: (() -> Unit)? = null
    var title: String? = null
    var message: String? = null
    var action: String? = null

    companion object {
        fun newInstance(titleRes: Int, messageRes: Int, actionRes: Int): AlertDialogFragment {
            val fragment = AlertDialogFragment()
            val bundle = Bundle()
            bundle.putInt(TITLE_RES_KEY, titleRes)
            bundle.putInt(MESSAGE_RES_KEY, messageRes)
            bundle.putInt(ACTION_RES_KEY, actionRes)
            fragment.arguments = bundle
            return fragment
        }

        fun newInstance(title: String, message: String, action: String): AlertDialogFragment {
            val fragment = AlertDialogFragment()
            val bundle = Bundle()
            bundle.putString(TITLE_KEY, title)
            bundle.putString(MESSAGE_KEY, message)
            bundle.putString(ACTION_KEY, action)
            fragment.arguments = bundle
            return fragment
        }

        fun newInstance(messageRes: Int): AlertDialogFragment {
            val fragment = AlertDialogFragment()
            val bundle = Bundle()
            bundle.putInt(MESSAGE_RES_KEY, messageRes)
            fragment.arguments = bundle
            return fragment
        }

        fun newInstance(message: String): AlertDialogFragment {
            val fragment = AlertDialogFragment()
            val bundle = Bundle()
            bundle.putString(MESSAGE_KEY, message)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            if (it.containsKey(TITLE_RES_KEY)) {
                titleRes = it.getInt(TITLE_RES_KEY, R.string.alert)
                title = context!!.getString(titleRes)
            }

            if (it.containsKey(TITLE_KEY)) {
                title = it.getString(TITLE_KEY)
            }

            if (it.containsKey(MESSAGE_RES_KEY)) {
                messageRes = it.getInt(MESSAGE_RES_KEY)
                message = context!!.getString(messageRes)
            }

            if (it.containsKey(MESSAGE_KEY)) {
                message = it.getString(MESSAGE_KEY)
            }

            if (it.containsKey(ACTION_RES_KEY)) {
                actionRes = it.getInt(ACTION_RES_KEY, R.string.ok)
                action = context!!.getString(actionRes)
            }

            if (it.containsKey(ACTION_KEY)) {
                action = it.getString(ACTION_KEY)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_alert, container, false)
        initView()
        return binding.root
    }

    private fun initView() {
        title?.let {
            binding.titleTextView.text = it
        }
        message?.let {
            binding.messageTextView.text = it
        }
        action?.let {
            binding.actionTextView.text = it
        }
        binding.actionTextView.setOnClickListener {
            onDismiss?.invoke()
            dismiss()
        }

        binding.closeImageView.setOnClickListener {
            dismiss()
        }
    }
}