package com.amberinternet.oktruck.ui.dialog

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import com.amberinternet.oktruck.MESSAGE_RES_KEY
import com.amberinternet.oktruck.TITLE_RES_KEY
import com.amberinternet.oktruck.databinding.DialogFragmentContactAdminBinding
import com.amberinternet.oktruck.ui.base.BaseDialogFragment
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.REQUEST_CODE_CALL_PERMISSION


class ContactAdminDialogFragment : BaseDialogFragment() {

    lateinit var binding: DialogFragmentContactAdminBinding
    lateinit var title: String
    lateinit var message: String

    companion object {
        fun newInstance(titleRes: Int, messageRes: Int): ContactAdminDialogFragment {
            val fragment = ContactAdminDialogFragment()
            val bundle = Bundle()
            bundle.putInt(TITLE_RES_KEY, titleRes)
            bundle.putInt(MESSAGE_RES_KEY, messageRes)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            val titleResId = it.getInt(TITLE_RES_KEY, R.string.please_contact_admin)
            val messageResId = it.getInt(MESSAGE_RES_KEY, R.string.need_help_from_admin)
            title = getString(titleResId)
            message = getString(messageResId)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_contact_admin, container, false)
        initView()
        initOnClick()
        return binding.root
    }

    private fun initView() {
        binding.messageTextView.text = message
    }

    private fun initOnClick() {
        binding.closeImageView.setOnClickListener {
            dismiss()
        }

        binding.callLayout.setOnClickListener {
            if (checkPermission()) {
                val telephone = getString(R.string.admin_telephone)
                val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel: $telephone"))
                startActivity(intent)
            }
        }
    }

    private fun checkPermission(): Boolean {
        return if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            requestPermission()
            false
        }
    }

    private fun requestPermission() {
        requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), REQUEST_CODE_CALL_PERMISSION)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE_CALL_PERMISSION -> {
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(activity!!, R.string.enable_call_permission, Toast.LENGTH_LONG).show()
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }
}