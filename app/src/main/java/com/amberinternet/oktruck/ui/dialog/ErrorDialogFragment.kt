package com.amberinternet.oktruck.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.MESSAGE_KEY
import com.amberinternet.oktruck.databinding.DialogFragmentErrorBinding
import com.amberinternet.oktruck.ui.base.BaseDialogFragment
import com.amberinternet.oktruck.ui.dialog.listener.ErrorDialogListener

class ErrorDialogFragment : BaseDialogFragment() {

    lateinit var binding: DialogFragmentErrorBinding
    lateinit var message: String
    var errorDialogListener: ErrorDialogListener? = null

    companion object {
        fun newInstance(error: String): ErrorDialogFragment {
            val fragment = ErrorDialogFragment()
            val bundle = Bundle()
            bundle.putString(MESSAGE_KEY, error)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            message = it.getString(MESSAGE_KEY)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_error, container, false)
        initView()
        return binding.root
    }

    private fun initView() {
        binding.messageTextView.text = message
        binding.closeImageView.setOnClickListener {
            dismiss()
            errorDialogListener?.onDismiss()
        }
    }
}