package com.amberinternet.oktruck.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.amberinternet.oktruck.ui.base.BaseDialogFragment
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.MESSAGE_KEY
import com.amberinternet.oktruck.MESSAGE_RES_KEY
import com.amberinternet.oktruck.databinding.DialogFragmentProgressBarBinding
import kotlin.properties.Delegates

class ProgressBarDialogFragment : BaseDialogFragment() {

    lateinit var binding: DialogFragmentProgressBarBinding
    lateinit var message: String
    private var messageRes: Int by Delegates.notNull()

    companion object {
        fun newInstance(message: String): ProgressBarDialogFragment {
            val fragment = ProgressBarDialogFragment()
            val bundle = Bundle()
            bundle.putString(MESSAGE_KEY, message)
            fragment.arguments = bundle
            return fragment
        }

        fun newInstance(messageRes: Int): ProgressBarDialogFragment {
            val fragment = ProgressBarDialogFragment()
            val bundle = Bundle()
            bundle.putInt(MESSAGE_RES_KEY, messageRes)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            when {
                it.containsKey(MESSAGE_KEY) -> message = it.getString(MESSAGE_KEY)
                it.containsKey(MESSAGE_RES_KEY) -> {
                    messageRes = it.getInt(MESSAGE_RES_KEY)
                    message = context!!.getString(messageRes)
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_progress_bar, container, false)
        isCancelable = false
        initView()
        return binding.root
    }

    private fun initView() {
        binding.messageTextView.text = message
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onPause() {
        super.onPause()
        dismiss()
    }
}