package th.co.growthd.oktrucker.ui.dialog

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.amberinternet.oktruck.MESSAGE_KEY
import com.amberinternet.oktruck.MESSAGE_RES_KEY
import com.amberinternet.oktruck.TITLE_KEY
import com.amberinternet.oktruck.TITLE_RES_KEY
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.databinding.DialogFragmentSuccessBinding
import com.amberinternet.oktruck.ui.base.BaseDialogFragment
import com.amberinternet.oktruck.ui.dialog.AlertDialogFragment
import kotlin.properties.Delegates

class SuccessDialogFragment : BaseDialogFragment() {

    lateinit var binding: DialogFragmentSuccessBinding
    private var titleRes: Int by Delegates.notNull()
    private var messageRes: Int by Delegates.notNull()
    var onDismiss: (() -> Unit)? = null
    var title: String? = null
    lateinit var message: String

    companion object {
        fun newInstance(titleRes: Int, messageRes: Int): AlertDialogFragment {
            val fragment = AlertDialogFragment()
            val bundle = Bundle()
            bundle.putInt(TITLE_RES_KEY, titleRes)
            bundle.putInt(MESSAGE_RES_KEY, messageRes)
            fragment.arguments = bundle
            return fragment
        }

        fun newInstance(title: String, message: String): AlertDialogFragment {
            val fragment = AlertDialogFragment()
            val bundle = Bundle()
            bundle.putString(TITLE_KEY, title)
            bundle.putString(MESSAGE_KEY, message)
            fragment.arguments = bundle
            return fragment
        }

        fun newInstance(messageRes: Int): AlertDialogFragment {
            val fragment = AlertDialogFragment()
            val bundle = Bundle()
            bundle.putInt(MESSAGE_RES_KEY, messageRes)
            fragment.arguments = bundle
            return fragment
        }

        fun newInstance(message: String): AlertDialogFragment {
            val fragment = AlertDialogFragment()
            val bundle = Bundle()
            bundle.putString(MESSAGE_KEY, message)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            if (it.containsKey(TITLE_RES_KEY)) {
                titleRes = it.getInt(TITLE_RES_KEY, R.string.alert)
                title = context!!.getString(titleRes)
            }

            if (it.containsKey(TITLE_KEY)) {
                title = it.getString(TITLE_KEY)
            }

            if (it.containsKey(MESSAGE_RES_KEY)) {
                messageRes = it.getInt(MESSAGE_RES_KEY)
                message = context!!.getString(messageRes)
            }

            if (it.containsKey(MESSAGE_KEY)) {
                message = it.getString(MESSAGE_KEY)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_success, container, false)
        initView()
        return binding.root
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onDismiss?.invoke()
    }

    private fun initView() {
        title?.let {
            binding.titleTextView.text = it
        }
        message?.let {
            binding.messageTextView.text = it
        }
        binding.closeTextView.setOnClickListener {
            dismiss()
        }
    }
}