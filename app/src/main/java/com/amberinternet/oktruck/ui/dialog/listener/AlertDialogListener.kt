package com.amberinternet.oktruck.ui.dialog.listener

interface AlertDialogListener {
    fun onDismiss()
}