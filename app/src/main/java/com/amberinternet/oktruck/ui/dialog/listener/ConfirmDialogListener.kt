package com.amberinternet.oktruck.ui.dialog.listener

interface ConfirmDialogListener {

    fun onConfirm()
    fun onCancel()
}