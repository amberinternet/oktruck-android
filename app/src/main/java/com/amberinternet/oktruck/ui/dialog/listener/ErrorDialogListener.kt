package com.amberinternet.oktruck.ui.dialog.listener

interface ErrorDialogListener {

    fun onDismiss()
}