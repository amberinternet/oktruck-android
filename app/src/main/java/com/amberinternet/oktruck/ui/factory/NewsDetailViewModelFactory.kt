package com.amberinternet.oktruck.ui.factory

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.amberinternet.oktruck.ui.viewmodel.NewsDetailViewModel

class NewsDetailViewModelFactory (private val fragmentActivity: FragmentActivity) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NewsDetailViewModel(fragmentActivity) as T
    }
}