package th.co.growthd.deerandbook.ui.factory

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.amberinternet.oktruck.ui.viewmodel.OTPViewModel

class OTPViewModelFactory (private val fragmentActivity: FragmentActivity) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return OTPViewModel(fragmentActivity) as T
    }
}