package com.amberinternet.oktruck.ui.factory

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.amberinternet.oktruck.ui.viewmodel.WorkDetailViewModel

class WorkDetailViewModelFactory (private val fragmentActivity: FragmentActivity) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return WorkDetailViewModel(fragmentActivity) as T
    }
}