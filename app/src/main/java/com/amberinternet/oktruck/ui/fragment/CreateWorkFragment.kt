package com.amberinternet.oktruck.ui.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModelProviders
import com.amberinternet.oktruck.model.*
import com.amberinternet.oktruck.ui.base.BaseFragment
import com.amberinternet.oktruck.ui.factory.CreateWorkViewModelFactory
import com.amberinternet.oktruck.ui.viewmodel.CreateWorkViewModel
import com.amberinternet.oktruck.util.manager.BottomSheetDialogManager
import kotlinx.android.synthetic.main.fragment_create_work.*
import kotlinx.android.synthetic.main.item_view_additional_service_checklist.view.*
import kotlinx.android.synthetic.main.item_view_additional_service_checklist.view.icon_next
import kotlinx.android.synthetic.main.item_view_truck_type_button.view.*
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.ui.activity.CreateWorkPaymentMethodActivity
import com.amberinternet.oktruck.util.manager.DialogManager
import com.amberinternet.oktruck.util.manager.UserManager
import kotlin.collections.ArrayList
import kotlin.math.round


class CreateWorkFragment : BaseFragment() {

    private lateinit var viewModel: CreateWorkViewModel
    private lateinit var bottomSheetDialogManager: BottomSheetDialogManager
    private lateinit var dialogManager: DialogManager
    private lateinit var userManager: UserManager
    private var temporaryWork: Work? = null

    private lateinit var work: Work
    private var truckType = ObservableField<TruckType>()
    private var source: Source? = null
    private var destination: Destination? = null
    private var itemImageUri: Uri? = null
    private var additionalServices = ObservableField<ArrayList<AdditionalService>>()
    private var pickUpDate: String? = null
    private var pickUpTime: String? = null
    private var dropOffDate: String? = null
    private var dropOffTime: String? = null

    companion object {
        fun newInstance(work: Work?): CreateWorkFragment {
            val fragment = CreateWorkFragment()
            val bundle = Bundle().apply { putParcelable(WORK_KEY, work) }
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_create_work, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.setTitle(R.string.title_delivery)
        initVariable()
        initViewModel()
        initView()
        initOnClick()
    }

    private fun initVariable() {
        bottomSheetDialogManager = BottomSheetDialogManager.getInstance(childFragmentManager)
        dialogManager = DialogManager.getInstance(childFragmentManager)
        userManager = UserManager(context!!)

        arguments?.let {
            temporaryWork = it.getParcelable(WORK_KEY)
        }

        truckType.addOnPropertyChanged {
            it.get()?.let {
                val selectedTruckTypeView = layoutInflater.inflate(R.layout.item_view_truck_type_button, null)
                selectedTruckTypeView.setBackgroundResource(android.R.color.transparent)
                selectedTruckTypeView.textView_name.text = it.name
                selectedTruckTypeView.textView_description.text = it.description
                selectedTruckTypeView.imageView_truck.setTruckTypeImage(it.getImagePath())

                relativeLayout_selectTruck.removeAllViews()
                relativeLayout_selectTruck.addView(selectedTruckTypeView)

                linearLayout_additionalServices.visibility = when (it.additionalServices.isNotEmpty()) {
                    true -> View.VISIBLE
                    false -> View.GONE
                }
                additionalServices.set(null)
            }
        }

        additionalServices.addOnPropertyChanged {
            it.get()?.let {
                linearLayout_additionalService_checklist.removeAllViews()
                linearLayout_serviceCharges.removeAllViews()
                if (it.isEmpty()) {
                    val additionalServicesButtonView = layoutInflater.inflate(R.layout.view_additional_services_button, null)
                    additionalServicesButtonView.layoutParams = RelativeLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, convertDpToPixel(
                            40f,
                            activity!!
                        ).toInt()
                    )
                    linearLayout_additionalService_checklist.addView(additionalServicesButtonView)
                } else {
                    for (additionalService in it) {
                        val selectedAdditionalServicesView = layoutInflater.inflate(R.layout.item_view_additional_service_checklist, null)
                        selectedAdditionalServicesView.layoutParams = RelativeLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT, convertDpToPixel(
                                40f,
                                activity!!
                            ).toInt()
                        )
                        selectedAdditionalServicesView.textView_serviceName.text = additionalService.name
                        selectedAdditionalServicesView.textView_serviceCharge.text = additionalService.getCharge()
                        selectedAdditionalServicesView.checkBox.isChecked = true
                        selectedAdditionalServicesView.linearLayout_serviceCharge.visibility = View.GONE
                        selectedAdditionalServicesView.icon_next.visibility = View.VISIBLE
                        linearLayout_additionalService_checklist.addView(selectedAdditionalServicesView)

                        // Add additional services to service charges list
                        val serviceChargesView = layoutInflater.inflate(R.layout.item_view_additional_service_charge, null)
                        serviceChargesView.textView_serviceName.text = additionalService.name
                        serviceChargesView.textView_serviceCharge.text = additionalService.getCharge()
                        linearLayout_serviceCharges.addView(serviceChargesView)
                    }
                }
            }
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, CreateWorkViewModelFactory(activity!!)).get(CreateWorkViewModel::class.java)
        subscription = viewModel.getTruckType()
    }

    private fun initView() {
        linearLayout_additionalServices.visibility = View.GONE

        Handler().postDelayed({
            temporaryWork?.let {
                for (truckType in viewModel.truckTypes) {
                    if (truckType.id == it.truckType.id) {
                        this.truckType.set(truckType)
                        break
                    }
                }
                source = it.source
                textView_sourceAddress.text = source!!.name
                textView_sourceAddress.setTextColor(resources.getColor(R.color.textColor))
                textView_sourceContact.text = getText(R.string.title_contact).toString() + ": " + source!!.contactName + " " + source!!.contactTelephoneNumber
                textView_sourceContact.visibility = View.VISIBLE
                destination = it.destination
                textView_destinationAddress.text = destination!!.name
                textView_destinationAddress.setTextColor(resources.getColor(R.color.textColor))
                textView_destinationContact.text = getText(R.string.title_contact).toString() + ": " + destination!!.contactName + " " + destination!!.contactTelephoneNumber
                textView_destinationContact.visibility = View.VISIBLE
                editText_itemName.setText(it.item.name)
                editText_quantity.setText(it.item.quantity)
                editText_weight.setText(it.item.weight)
                additionalServices.set(ArrayList(it.additionalServices))
                textView_transportationFee.text = it.transportationFee.toDouble().roundDecimalFormat()
                textView_totalFee.text = it.totalFee.toDouble().roundDecimalFormat()
            }
        }, 500)
    }

    @SuppressLint("SetTextI18n")
    private fun initOnClick() {
        relativeLayout_selectTruck.setOnClickListener {
            bottomSheetDialogManager.showTruckTypes(viewModel.truckTypes, truckType.get()) {
                truckType.set(it)
                calculateTotalFees()
            }
        }

        linearLayout_source.setOnClickListener {
            bottomSheetDialogManager.showSearchSourcesAddress { address ->
                source = address
                textView_sourceAddress.text = address.name
                textView_sourceAddress.setTextColor(resources.getColor(R.color.textColor))
                textView_sourceContact.text = getText(R.string.title_contact).toString() + ": " + address.contactName + " " + address.contactTelephoneNumber
                textView_sourceContact.visibility = View.VISIBLE

                if (source != null && destination != null) {
                    subscription = viewModel.getDistance(source!!.latitude, source!!.longitude, destination!!.latitude, destination!!.longitude)
                    calculateTotalFees()
                }
            }
        }

        linearLayout_destination.setOnClickListener {
            bottomSheetDialogManager.showSearchDestinationAddress { address ->
                destination = address
                textView_destinationAddress.text = address.name
                textView_destinationAddress.setTextColor(resources.getColor(R.color.textColor))
                textView_destinationContact.text = getText(R.string.title_contact).toString() + ": " + address.contactName + " " + address.contactTelephoneNumber
                textView_destinationContact.visibility = View.VISIBLE

                if (source != null && destination != null) {
                    subscription = viewModel.getDistance(source!!.latitude, source!!.longitude, destination!!.latitude, destination!!.longitude)
                    calculateTotalFees()
                }
            }
        }

        linearLayout_image.setOnClickListener {
            if (checkPermission()) {
                bottomSheetDialogManager.showImagePicker { imageUri ->
                    itemImageUri = imageUri
                    imageView_image.setImageURI(imageUri)
                    imageView_image.visibility = View.VISIBLE
                    textView_attachImage.visibility = View.GONE
                }
            }
        }

        linearLayout_additionalServices.setOnClickListener {
            bottomSheetDialogManager.showAdditionalServices(truckType.get()!!.additionalServices.toTypedArray(), additionalServices.get()) {
                additionalServices.set(null)
                additionalServices.set(it)
                calculateTotalFees()
            }
        }

        linearLayout_receiptDate.setOnClickListener {
            bottomSheetDialogManager.showReceiptDatePicker() { date: String, time: String ->
                pickUpDate = date
                pickUpTime = time
                textView_receiptDate.text = "${pickUpDate!!.fromDate().toDateWithFullDayAppFormat()} | $pickUpTime"
                textView_receiptDate.setTextColor(resources.getColor(R.color.textColor))

                dropOffDate = null
                dropOffTime = null
                textView_deliveryDate.setText(R.string.hint_pick_up_date)
                textView_deliveryDate.setTextColor(resources.getColor(R.color.textColorHint))
            }
        }

        linearLayout_deliveryDate.setOnClickListener {
            if (!pickUpDate.isNullOrEmpty()) {
                bottomSheetDialogManager.showDeliveryDatePicker(pickUpDate!!) { date: String, time: String ->
                    dropOffDate = date
                    dropOffTime = time
                    textView_deliveryDate.text = "${dropOffDate!!.fromDate().toDateWithFullDayAppFormat()} | $dropOffTime"
                    textView_deliveryDate.setTextColor(resources.getColor(R.color.textColor))
                }
            } else {
                dialogManager.showAlert(R.string.please_select_pick_up_date)
            }
        }

        button_pay.setOnClickListener {
            if (validate()) {
                if (temporaryWork == null) { work = Work() }
                else { work = temporaryWork!! }
                work.truckType.id = truckType.get()!!.id
                work.item.name = editText_itemName.text.toString()
                work.item.quantity = editText_quantity.text.toString()
                work.item.weight = editText_weight.text.toString()
                additionalServices.get()?.let { work.additionalServices = it }
                work.item.imageUri = itemImageUri!!
                work.source = source!!
                work.source.date = pickUpDate!!
                work.source.time = pickUpTime!!
                work.destination = destination!!
                work.destination.date = dropOffDate!!
                work.destination.time = dropOffTime!!

                work.transportationFee = textView_transportationFee.text.toString()
                work.totalFee = textView_totalFee.text.toString()

                userManager.saveSourceAddressHistory(work.source)
                userManager.saveDestinationAddressHistory(work.destination)

                val intent = Intent(activity, CreateWorkPaymentMethodActivity::class.java)
                intent.putExtra(WORK_KEY, work)
                startActivity(intent)
            }
        }
    }

    private fun validate(): Boolean {
        return when {
            truckType.get() == null -> {
                dialogManager.showAlert(R.string.please_select_truck_type)
                false
            }
            source == null -> {
                dialogManager.showAlert(R.string.please_select_source)
                false
            }
            destination == null -> {
                dialogManager.showAlert(R.string.please_select_destination)
                false
            }
            editText_itemName.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_item_name)
                false
            }
            editText_quantity.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_quantity)
                false
            }
            editText_weight.isEmpty() -> {
                dialogManager.showAlert(R.string.please_fill_out_weight)
                false
            }
            itemImageUri == null -> {
                dialogManager.showAlert(R.string.please_select_item_image)
                false
            }
            pickUpDate == null -> {
                dialogManager.showAlert(R.string.please_select_pick_up_date)
                false
            }
            dropOffDate == null -> {
                dialogManager.showAlert(R.string.please_select_drop_off_date)
                false
            }
            else -> true
        }
    }

    private fun calculateTotalFees() {
        var transportationFee = 0.0
        var additionalServicesCharge = 0.0
        var totalFee = 0.0

        if (viewModel.distance != null && truckType.get() != null && viewModel.distance!! > 0) {
            var distance = viewModel.distance!!
            val truckType = truckType.get()!!

            // 0 - 3000 m
            if (distance - 3000 > 0) {
                transportationFee += truckType.priceFirst3Km
                distance -= 3000

                // 3001 - 15,000 m
                if (distance - 12000 > 0) {
                    transportationFee += 12 * truckType.price3To15Km
                    distance -= 12000

                    // 15,001 - 100,000 m
                    if (distance - 85000 > 0) {
                        transportationFee += 85 * truckType.price15To100Km
                        distance -= 85000

                        // >100,000 m
                        transportationFee += round(distance / 1000f) * truckType.price100UpKm

                    } else {
                        transportationFee += round(distance / 1000f) * truckType.price15To100Km
                    }

                } else {
                    transportationFee += round(distance / 1000f) * truckType.price3To15Km
                }

            } else {
                transportationFee += truckType.priceFirst3Km
            }
        }

        additionalServices.get()?.let {
            for (additionalService in it) {
                additionalServicesCharge += additionalService.getChargeValue()
            }
        }

        totalFee = transportationFee + additionalServicesCharge

        textView_transportationFee.text = transportationFee.roundDecimalFormat()
        textView_totalFee.text = totalFee.roundDecimalFormat()
    }

    private fun checkPermission(): Boolean {
        return if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            requestPermission()
            false
        }
    }

    private fun requestPermission() {
        requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_CODE_WRITE_PERMISSION)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE_WRITE_PERMISSION -> {
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(activity!!, R.string.enable_write_permission, Toast.LENGTH_SHORT).show()
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun convertDpToPixel(dp: Float, context: Context): Float {
        val resources = context.resources
        val metrics = resources.displayMetrics
        return dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }
}