package com.amberinternet.oktruck.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.WORK_DETAIL_REQUEST
import com.amberinternet.oktruck.WORK_KEY
import com.amberinternet.oktruck.adapter.CompleteWorkAdapter
import com.amberinternet.oktruck.addOnPropertyChanged
import com.amberinternet.oktruck.model.subscriber.LoadMoreSubscriber
import com.amberinternet.oktruck.ui.base.BaseFragment
import com.amberinternet.oktruck.ui.decoration.LinearDecoration
import com.amberinternet.oktruck.ui.viewmodel.MyCompleteWorkViewModel
import kotlinx.android.synthetic.main.fragment_my_completed_work.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import com.amberinternet.oktruck.ui.factory.MyCompleteWorkViewModelFactory
import com.amberinternet.oktruck.adapter.callback.OnRecyclerTouchListener
import com.amberinternet.oktruck.ui.activity.WorkDetailActivity

class MyCompleteWorkFragment : BaseFragment() {

    private lateinit var viewModel: MyCompleteWorkViewModel
    private lateinit var completeWorkAdapter: CompleteWorkAdapter

    companion object {
        fun newInstance() = MyCompleteWorkFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        EventBus.getDefault().register(this)
        return inflater.inflate(R.layout.fragment_my_completed_work, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViewModel()
        initView()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == WORK_DETAIL_REQUEST) {
            viewModel.isRefresh.set(true)
            subscription = viewModel.getMyCompleteWorkList(1)
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, MyCompleteWorkViewModelFactory(activity!!)).get(MyCompleteWorkViewModel::class.java)
        completeWorkAdapter = CompleteWorkAdapter(viewModel.workList)
        viewModel.workPagination.addOnPropertyChanged {
            it.get()?.let {
                completeWorkAdapter.workPagination = it
                completeWorkAdapter.notifyDataSetChanged()
            }
        }
        viewModel.isRefresh.addOnPropertyChanged {
            refreshLayout.isRefreshing = it.get()
        }
    }

    private fun initView() {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = completeWorkAdapter
            addItemDecoration(
                LinearDecoration(resources.getDimension(R.dimen.margin_small).toInt())
            )
            addOnItemTouchListener(OnRecyclerTouchListener(context!!) { position ->
                if (viewModel.workList.isNotEmpty()) {
                    val intent = Intent(activity, WorkDetailActivity::class.java)
                    intent.putExtra(WORK_KEY, viewModel.workList[position])
                    startActivityForResult(intent, WORK_DETAIL_REQUEST)
                }
            })
        }

        refreshLayout.setColorSchemeColors(context!!.resources.getColor(R.color.colorPrimary))
        refreshLayout.setOnRefreshListener {
            viewModel.isRefresh.set(true)
            subscription = viewModel.getMyCompleteWorkList(1)
        }
    }

    @Subscribe
    fun onLoadMoreMyCompleteWork(loadMoreSubscriber: LoadMoreSubscriber) {
        if (loadMoreSubscriber.isLoadMoreMyCompleteWork) {
            subscription = viewModel.getMyCompleteWorkList(loadMoreSubscriber.page)
        }
    }

    override fun onDestroyView() {
        EventBus.getDefault().unregister(this)
        super.onDestroyView()
    }

}