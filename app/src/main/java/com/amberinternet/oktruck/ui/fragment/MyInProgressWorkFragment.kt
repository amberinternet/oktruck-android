package com.amberinternet.oktruck.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.WORK_DETAIL_REQUEST
import com.amberinternet.oktruck.WORK_KEY
import com.amberinternet.oktruck.adapter.InProgressWorkAdapter
import com.amberinternet.oktruck.addOnPropertyChanged
import com.amberinternet.oktruck.model.subscriber.LoadMoreSubscriber
import com.amberinternet.oktruck.ui.base.BaseFragment
import com.amberinternet.oktruck.ui.decoration.LinearDecoration
import com.amberinternet.oktruck.ui.viewmodel.MyInProgressWorkViewModel
import kotlinx.android.synthetic.main.fragment_my_in_progress_work.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import com.amberinternet.oktruck.ui.factory.MyInProgressWorkViewModelFactory
import com.amberinternet.oktruck.adapter.callback.OnRecyclerTouchListener
import com.amberinternet.oktruck.ui.activity.WorkDetailActivity

class MyInProgressWorkFragment : BaseFragment() {

    private lateinit var viewModel: MyInProgressWorkViewModel
    private lateinit var inProgressWorkAdapter: InProgressWorkAdapter

    companion object {
        fun newInstance() = MyInProgressWorkFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        EventBus.getDefault().register(this)
        return inflater.inflate(R.layout.fragment_my_in_progress_work, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViewModel()
        initView()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == WORK_DETAIL_REQUEST) {
            viewModel.isRefresh.set(true)
            subscription = viewModel.getMyInProgressWorkList(1)
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, MyInProgressWorkViewModelFactory(activity!!)).get(MyInProgressWorkViewModel::class.java)
        inProgressWorkAdapter = InProgressWorkAdapter(viewModel.workList)
        viewModel.workPagination.addOnPropertyChanged {
            it.get()?.let {
                inProgressWorkAdapter.workPagination = it
                inProgressWorkAdapter.notifyDataSetChanged()
            }
        }
        viewModel.isRefresh.addOnPropertyChanged {
            refreshLayout.isRefreshing = it.get()
        }
    }

    private fun initView() {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = inProgressWorkAdapter
            addItemDecoration(
                LinearDecoration(resources.getDimension(R.dimen.margin_small).toInt())
            )
            addOnItemTouchListener(OnRecyclerTouchListener(context!!) { position ->
                if (viewModel.workList.isNotEmpty()) {
                    val intent = Intent(activity, WorkDetailActivity::class.java)
                    intent.putExtra(WORK_KEY, viewModel.workList[position])
                    startActivityForResult(intent, WORK_DETAIL_REQUEST)
                }
            })
        }

        refreshLayout.setColorSchemeColors(context!!.resources.getColor(R.color.colorPrimary))
        refreshLayout.setOnRefreshListener {
            viewModel.isRefresh.set(true)
            subscription = viewModel.getMyInProgressWorkList(1)
        }
    }

    @Subscribe
    fun onLoadMoreMyInProgressWork(loadMoreSubscriber: LoadMoreSubscriber) {
        if (loadMoreSubscriber.isLoadMoreMyInProgressWork) {
            subscription = viewModel.getMyInProgressWorkList(loadMoreSubscriber.page)
        }
    }

    override fun onDestroyView() {
        EventBus.getDefault().unregister(this)
        super.onDestroyView()
    }

}