package com.amberinternet.oktruck.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.ui.base.BaseFragment
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_my_work.*

class MyWorkFragment : BaseFragment() {

    companion object {
        fun newInstance() = MyWorkFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_my_work, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.setTitle(R.string.title_schedule)
        initView()
    }

    private fun initView() {
        viewPager.adapter = TabPagerAdapter(childFragmentManager)
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    inner class TabPagerAdapter(fragmentManager: FragmentManager): FragmentPagerAdapter(fragmentManager) {

        override fun getItem(position: Int): Fragment {
            when (position) {
                0 -> return MyInProgressWorkFragment.newInstance()
                else -> return MyCompleteWorkFragment.newInstance()
            }
        }

        override fun getCount(): Int {
            return tabLayout.tabCount
        }
    }
}

