package com.amberinternet.oktruck.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.amberinternet.oktruck.NEWS_KEY
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.adapter.NewsAdapter
import com.amberinternet.oktruck.addOnPropertyChanged
import com.amberinternet.oktruck.model.subscriber.LoadMoreSubscriber
import com.amberinternet.oktruck.ui.activity.NewsDetailActivity
import com.amberinternet.oktruck.ui.base.BaseFragment
import com.amberinternet.oktruck.ui.decoration.LinearDecoration
import com.amberinternet.oktruck.ui.factory.NewsViewModelFactory
import com.amberinternet.oktruck.ui.viewmodel.NewsViewModel
import kotlinx.android.synthetic.main.fragment_news.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import com.amberinternet.oktruck.adapter.callback.OnRecyclerTouchListener

class NewsFragment : BaseFragment() {

    private lateinit var newsAdapter: NewsAdapter
    private lateinit var viewModel: NewsViewModel

    companion object {
        fun newInstance() = NewsFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        EventBus.getDefault().register(this)
        return inflater.inflate(R.layout.fragment_news, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.setTitle(R.string.title_news)
        viewModel = ViewModelProviders.of(this, NewsViewModelFactory(activity!!)).get(NewsViewModel::class.java)
        initViewModel()
        initView()
    }

    private fun initViewModel() {
        viewModel.newsPagination.addOnPropertyChanged {
            it.get()?.let {
                newsAdapter.newsPagination = it
                newsAdapter.notifyDataSetChanged()
            }
        }
        viewModel.isRefresh.addOnPropertyChanged {
            refreshLayout.isRefreshing = it.get()
        }
    }

    private fun initView() {
        newsAdapter = NewsAdapter(viewModel.newsList)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = newsAdapter
            addItemDecoration(
                LinearDecoration(resources.getDimension(R.dimen.margin_news).toInt())
            )
            addOnItemTouchListener(OnRecyclerTouchListener(context!!) { position ->
                if (viewModel.newsList.isNotEmpty()) {
                    val intent = Intent(activity, NewsDetailActivity::class.java)
                    intent.putExtra(NEWS_KEY, viewModel.newsList[position])
                    startActivity(intent)
                }
            })

            refreshLayout.setColorSchemeColors(context!!.resources.getColor(R.color.colorPrimary))
            refreshLayout.setOnRefreshListener {
                viewModel.isRefresh.set(true)
                viewModel.getNewsList()
            }
        }
    }

    @Subscribe
    fun onLoadMoreNews(loadMoreSubscriber: LoadMoreSubscriber) {
        if (loadMoreSubscriber.isLoadMoreNews) {
            subscription = viewModel.getNewsList(loadMoreSubscriber.page)
        }
    }

    override fun onDestroyView() {
        EventBus.getDefault().unregister(this)
        super.onDestroyView()
    }

}

