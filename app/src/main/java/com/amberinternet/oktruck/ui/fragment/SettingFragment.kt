package com.amberinternet.oktruck.ui.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.model.User
import com.amberinternet.oktruck.ui.base.BaseFragment
import com.amberinternet.oktruck.ui.viewmodel.SettingViewModel
import com.amberinternet.oktruck.util.manager.DialogManager
import kotlinx.android.synthetic.main.fragment_setting.*
import th.co.growthd.deerandbook.ui.factory.SettingViewModelFactory

class SettingFragment : BaseFragment() {

    private lateinit var viewModel: SettingViewModel
    private lateinit var dialogManager: DialogManager
    private lateinit var user: User

    companion object {
        fun newInstance() = SettingFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVariable()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_setting, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, SettingViewModelFactory(activity!!)).get(SettingViewModel::class.java)
        activity?.setTitle(R.string.title_user)
        initView()
        initOnClick()
    }

    private fun initVariable() {
        user = User.instance
        dialogManager = DialogManager.getInstance(childFragmentManager)
    }

    @SuppressLint("SetTextI18n")
    private fun initView() {
        textView_build_version.text = "${getString(R.string.version)}: ${BuildConfig.VERSION_NAME} build ${BuildConfig.VERSION_CODE}"
        imageView_profile.setProfileImage(user.profileImageURI)
        textView_user_id.text = user.readableUserId
        textView_full_name.text = user.getFullName()
        textView_email.text = user.email
        textView_telephone.text = user.telephoneNumber
        textView_joined.text = user.createdAt.fromDateTime().toDateAppFormat()
    }

    private fun initOnClick() {
        button_logOut.setOnClickListener {
            dialogManager.showConfirm(R.string.confirm_log_out, R.string.message_confirm_log_out) {
                viewModel.logOut()
            }
        }
    }
}