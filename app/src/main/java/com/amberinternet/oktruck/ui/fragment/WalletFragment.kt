package com.amberinternet.oktruck.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.adapter.TransactionAdapter
import com.amberinternet.oktruck.model.User
import com.amberinternet.oktruck.model.subscriber.FilterTransactionSubscriber
import com.amberinternet.oktruck.model.subscriber.LoadMoreSubscriber
import com.amberinternet.oktruck.ui.activity.FilterTransactionActivity
import com.amberinternet.oktruck.ui.activity.MainActivity
import com.amberinternet.oktruck.ui.base.BaseFragment
import com.amberinternet.oktruck.ui.factory.WalletViewModelFactory
import com.amberinternet.oktruck.ui.viewmodel.WalletViewModel
import com.amberinternet.oktruck.util.manager.BottomSheetDialogManager
import kotlinx.android.synthetic.main.fragment_wallet.*
import kotlinx.android.synthetic.main.view_action_bar.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*

class WalletFragment : BaseFragment() {

    private lateinit var viewModel: WalletViewModel
    private lateinit var transactionAdapter: TransactionAdapter
    private lateinit var bottomSheetDialogManager: BottomSheetDialogManager
    private lateinit var user: User
    private var monthQuery: Int = INITIAL_INT
    private var yearQuery: Int = INITIAL_INT

    companion object {
        fun newInstance() = WalletFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVariable()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        EventBus.getDefault().register(this)
        return inflater.inflate(R.layout.fragment_wallet, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.setTitle(R.string.title_wallet)
        initViewModel()
        initView()
        initOnClick()
    }

    private fun initVariable() {
        bottomSheetDialogManager = BottomSheetDialogManager.getInstance(childFragmentManager)
        user = User.instance
        monthQuery = Calendar.getInstance().getMonth()
        yearQuery = Calendar.getInstance().getYear()
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, WalletViewModelFactory(activity!!)).get(WalletViewModel::class.java)

        subscription = viewModel.getProfile()
        viewModel.isLoadedProfile.addOnPropertyChanged {
            textView_credit.text = user.getCredit()
            viewModel.isLoadedProfile.set(false)
        }
        viewModel.transactionPagination.addOnPropertyChanged {
            it.get()?.let {
                transactionAdapter.transactionPagination = it
                transactionAdapter.notifyDataSetChanged()
            }
        }
        viewModel.isRefresh.addOnPropertyChanged {
            refreshLayout.isRefreshing = it.get()
        }
    }

    private fun initView() {
        activity?.textView_rightButton?.visibility = View.VISIBLE
        activity?.textView_rightButton?.text = getString(R.string.search)

        textView_credit.text = user.getCredit()
        transactionAdapter = TransactionAdapter(viewModel.transactionList)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = transactionAdapter
        }
        refreshLayout.setColorSchemeColors(context!!.resources.getColor(R.color.colorPrimary))
        refreshLayout.setOnRefreshListener {
            viewModel.isRefresh.set(true)
            subscription = viewModel.getTransactionList(1, getMonthYearFilter())
            subscription = viewModel.getProfile()
        }
    }

    private fun initOnClick() {
        activity?.textView_rightButton?.setOnClickListener {
            val intent = Intent(activity, FilterTransactionActivity::class.java)
            intent.putExtra(MONTH_KEY, monthQuery)
            intent.putExtra(YEAR_KEY, yearQuery)
            startActivity(intent)
        }
        relativeLayout_deposit.setOnClickListener {
            bottomSheetDialogManager.showDeposit()
        }
    }

    private fun getMonthYearFilter(): String {
        val year = yearQuery
        val month = "%02d".format(monthQuery)
        return "$year-$month"
    }

    @Subscribe
    fun onFilter(filterTransactionSubscriber: FilterTransactionSubscriber) {
        viewModel.isRefresh.set(true)
        monthQuery = filterTransactionSubscriber.month
        yearQuery = filterTransactionSubscriber.year
        subscription = viewModel.getTransactionList(1, getMonthYearFilter())
        viewModel.transactionList.clear()
    }

    @Subscribe
    fun onLoadMoreTransaction(loadMoreSubscriber: LoadMoreSubscriber) {
        if (loadMoreSubscriber.isLoadMoreTransaction) {
            subscription = viewModel.getTransactionList(loadMoreSubscriber.page, getMonthYearFilter())
        }
    }

    override fun onDestroyView() {
        (activity as MainActivity).resetRightButton()
        EventBus.getDefault().unregister(this)
        super.onDestroyView()
    }

}