package com.amberinternet.oktruck.ui.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.amberinternet.oktruck.model.AdditionalService
import kotlinx.android.synthetic.main.item_view_additional_service_checklist.view.*

class AdditionalServiceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    lateinit var additionalService: AdditionalService

    fun initView() {
        itemView.textView_serviceName.text = additionalService.name
        itemView.textView_serviceCharge.text = additionalService.getCharge()
    }

    fun highlight() {
        itemView.checkBox.isChecked = true
    }

}