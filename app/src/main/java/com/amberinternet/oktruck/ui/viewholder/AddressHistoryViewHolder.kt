package com.amberinternet.oktruck.ui.viewholder

import android.annotation.SuppressLint
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.amberinternet.oktruck.MyApplication.Companion.resources
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.model.Destination
import com.amberinternet.oktruck.model.Source
import kotlinx.android.synthetic.main.item_view_address_history.view.*

@SuppressLint("SetTextI18n")
class AddressHistoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    lateinit var source: Source
    lateinit var destination: Destination

    fun initSourceView() {
        itemView.textView_locationName.text = source.name
        itemView.textView_contact.text = "${resources.getString(R.string.contact)}: ${source.contactName} ${source.contactTelephoneNumber}"
    }

    fun initDestinationView() {
        itemView.textView_locationName.text = destination.name
        itemView.textView_contact.text = "${resources.getString(R.string.contact)}: ${destination.contactName} ${destination.contactTelephoneNumber}"
    }

}