package com.amberinternet.oktruck.ui.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_view_credit.view.*

class CreditViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    lateinit var credit: String

    fun initView() {
        itemView.textView_credit.text = credit
        itemView.isClickable = true
    }
}