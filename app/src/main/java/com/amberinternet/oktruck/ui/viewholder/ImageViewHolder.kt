package com.amberinternet.oktruck.ui.viewholder

import android.graphics.Bitmap
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_view_image.view.*

class ImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    lateinit var bitmap: Bitmap

    fun initView() {
        itemView.imageView.setImageBitmap(bitmap)
    }
}