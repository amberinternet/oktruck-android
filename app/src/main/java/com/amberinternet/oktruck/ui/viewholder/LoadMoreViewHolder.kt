package com.amberinternet.oktruck.ui.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_view_load_more.view.*

class LoadMoreViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun hideProgressBar() {
        itemView.progressBar.visibility = View.GONE
    }

    fun showProgressBar() {
        itemView.progressBar.visibility = View.VISIBLE
    }
}