package com.amberinternet.oktruck.ui.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.amberinternet.oktruck.fromDateTime
import com.amberinternet.oktruck.model.News
import com.amberinternet.oktruck.toDateTimeAppFormat
import kotlinx.android.synthetic.main.item_view_news.view.*

class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    lateinit var news: News

    fun initView() {
        itemView.textView_title.text = news.title
        itemView.textView_shortContent.text = news.shortContent
        itemView.textView_date.text = news.postAt.fromDateTime().toDateTimeAppFormat()
        itemView.icon_pin.visibility = if (news.isPinned == 1) View.VISIBLE else View.GONE
    }
}