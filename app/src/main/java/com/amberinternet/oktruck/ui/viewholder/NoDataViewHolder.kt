package com.amberinternet.oktruck.ui.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class NoDataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun initView() {}

}