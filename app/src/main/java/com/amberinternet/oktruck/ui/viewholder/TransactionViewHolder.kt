package com.amberinternet.oktruck.ui.viewholder

import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.amberinternet.oktruck.fromDateTime
import com.amberinternet.oktruck.model.Transaction
import com.amberinternet.oktruck.toDateTimeAppFormat
import com.amberinternet.oktruck.toSpanned
import kotlinx.android.synthetic.main.item_view_transaction.view.*

class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    lateinit var transaction: Transaction

    fun initView() {
        itemView.textView_name.text = transaction.getReadableType().toSpanned()
        itemView.textView_date.text = transaction.createdAt.fromDateTime().toDateTimeAppFormat()
        itemView.textView_credit.apply {
            text = transaction.getCredit()
            setTextColor(ContextCompat.getColor(context, transaction.getCreditTextColor()))
            itemView.textView_bathSign.setTextColor(ContextCompat.getColor(context, transaction.getCreditTextColor()))
        }
    }
}