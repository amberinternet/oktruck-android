package com.amberinternet.oktruck.ui.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.amberinternet.oktruck.model.TruckType
import com.amberinternet.oktruck.setImageUrl
import com.amberinternet.oktruck.setTruckTypeImage
import com.amberinternet.oktruck.util.manager.UserManager
import kotlinx.android.synthetic.main.item_view_truck_type.view.*

class TruckTypeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    lateinit var truckType: TruckType

    fun initView() {
        itemView.textView_name.text = truckType.name
        itemView.textView_description.text = truckType.description
        itemView.imageView_truck.setTruckTypeImage(truckType.getImagePath())
    }

    fun highlight() {
        itemView.isSelected = true
    }

}