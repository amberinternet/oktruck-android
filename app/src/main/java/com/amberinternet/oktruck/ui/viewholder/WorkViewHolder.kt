package com.amberinternet.oktruck.ui.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.amberinternet.oktruck.MyApplication
import com.amberinternet.oktruck.MyApplication.Companion.resources
import com.amberinternet.oktruck.model.Work
import com.amberinternet.oktruck.util.enum.WorkStatus
import kotlinx.android.synthetic.main.item_view_work.view.*

class WorkViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    lateinit var work: Work

    fun initView() {
        itemView.textView_sourceAddress.text = work.source.getShortAddress()
        itemView.textView_sourceDate.text = work.source.getArrivalDateTime()
        itemView.textView_destinationAddress.text = work.destination.getShortAddress()
        itemView.textView_destinationDate.text = work.destination.getArrivalDateTime()
        itemView.textView_name.text = work.truckType.name
        itemView.textView_workId.text = work.getReadableWorkId()
        initStatusTextView()
    }

    private fun initStatusTextView() {
        itemView.textView_status.text = work.getReadableWorkStatus()
        when (work.status) {
            WorkStatus.COMPLETE.status -> {
                itemView.textView_status.setTextColor(resources.getColor(WorkStatus.COMPLETE.statusColor))
            }
            WorkStatus.CANCEL.status, WorkStatus.PAYMENT_FAIL.status -> {
                itemView.textView_status.setTextColor(resources.getColor(WorkStatus.CANCEL.statusColor))
            }
            else -> {
                itemView.textView_status.setTextColor(resources.getColor(WorkStatus.FIND_TRUCK.statusColor))
            }
        }
    }
}