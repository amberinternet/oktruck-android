package com.amberinternet.oktruck.ui.viewmodel

import android.content.Intent
import androidx.databinding.ObservableField
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.model.Work
import com.amberinternet.oktruck.model.WorkOmise
import com.amberinternet.oktruck.ui.activity.MainActivity
import com.amberinternet.oktruck.util.enum.PaymentMethod
import com.amberinternet.oktruck.util.manager.DialogManager
import com.amberinternet.oktruck.util.network.RxNetwork
import com.amberinternet.oktruck.util.network.response.SuccessResponse
import com.amberinternet.oktruck.util.network.response.WorkOmiseResponse
import com.amberinternet.oktruck.util.network.service.UserService
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import rx.Subscription
import java.io.File

class CreateWorkCreditCardMethodViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var dialogManager: DialogManager
    private var userService: UserService
    var workOmise = ObservableField<WorkOmise>()

    init {
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
        userService = UserService.getInstance(fragmentActivity)
    }

    fun requestCreateWorkWithOmise(work: Work, token: String): Subscription? {
        val additionalServiceHashMap = HashMap<String, RequestBody>()
        for (additionalService in work.additionalServices) {
            additionalServiceHashMap["has_service_${additionalService.id}"] = RequestBody.create(MultipartBody.FORM, "1")
        }
        return RxNetwork<WorkOmiseResponse>(fragmentActivity).request(userService.createWorkWithOmise(
            RequestBody.create(MultipartBody.FORM, work.truckType.id.toString()),
            RequestBody.create(MultipartBody.FORM, PaymentMethod.OMISE.status.toString()),
            RequestBody.create(MultipartBody.FORM, token),
            RequestBody.create(MultipartBody.FORM, BASE_URL),
            RequestBody.create(MultipartBody.FORM, work.userName),
            RequestBody.create(MultipartBody.FORM, work.userTelephoneNumber),
            RequestBody.create(MultipartBody.FORM, work.note),
            RequestBody.create(MultipartBody.FORM, work.item.name),
            RequestBody.create(MultipartBody.FORM, work.item.quantity),
            RequestBody.create(MultipartBody.FORM, work.item.weight),
            RequestBody.create(MediaType.parse("multipart/form-data"), File(work.item.imageUri!!.path).resize(fragmentActivity, 1600)),
            RequestBody.create(MultipartBody.FORM, work.source.latitude.toString()),
            RequestBody.create(MultipartBody.FORM, work.source.longitude.toString()),
            RequestBody.create(MultipartBody.FORM, work.source.address),
            RequestBody.create(MultipartBody.FORM, work.source.subdistrict),
            RequestBody.create(MultipartBody.FORM, work.source.district),
            RequestBody.create(MultipartBody.FORM, work.source.province),
            RequestBody.create(MultipartBody.FORM, work.source.contactName),
            RequestBody.create(MultipartBody.FORM, work.source.contactTelephoneNumber),
            RequestBody.create(MultipartBody.FORM, work.source.name),
            RequestBody.create(MultipartBody.FORM, work.source.landmark),
            RequestBody.create(MultipartBody.FORM, work.source.date),
            RequestBody.create(MultipartBody.FORM, work.source.time),
            RequestBody.create(MultipartBody.FORM, work.destination.latitude.toString()),
            RequestBody.create(MultipartBody.FORM, work.destination.longitude.toString()),
            RequestBody.create(MultipartBody.FORM, work.destination.address),
            RequestBody.create(MultipartBody.FORM, work.destination.subdistrict),
            RequestBody.create(MultipartBody.FORM, work.destination.district),
            RequestBody.create(MultipartBody.FORM, work.destination.province),
            RequestBody.create(MultipartBody.FORM, work.destination.contactName),
            RequestBody.create(MultipartBody.FORM, work.destination.contactTelephoneNumber),
            RequestBody.create(MultipartBody.FORM, work.destination.name),
            RequestBody.create(MultipartBody.FORM, work.destination.landmark),
            RequestBody.create(MultipartBody.FORM, work.destination.date),
            RequestBody.create(MultipartBody.FORM, work.destination.time),
            additionalServiceHashMap
        ),
            onSuccess = { response ->
                workOmise.set(response.workOmise)
            },
            onFailure = { error ->
                dialogManager.showError(error)
            },
            onLoading = {
                dialogManager.showLoading(R.string.sending_data)
            },
            onLoaded = {
                dialogManager.hideLoading()
            }
        )
    }

    fun completePaymentOmise(): Subscription? {
        return RxNetwork<SuccessResponse>(fragmentActivity).request(userService.createWorkCompletePaymentOmise(workOmise.get()!!.work.id),
            onSuccess = {
                dialogManager.showSuccess(R.string.payment_successfully) {
                    val intent = Intent(fragmentActivity, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    intent.putExtra(NAVIGATION_TAB_KEY, R.id.navi_tab_create_work)
                    fragmentActivity.startActivity(intent)
                }
            },
            onFailure = { error ->
                dialogManager.showError(error)
            },
            onLoading = {
                dialogManager.showLoading(R.string.checking_payment)
            },
            onLoaded = {
                dialogManager.hideLoading()
            })
    }
}