package com.amberinternet.oktruck.ui.viewmodel

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import com.amberinternet.oktruck.INITIAL_DOUBLE
import com.amberinternet.oktruck.model.TruckType
import com.amberinternet.oktruck.util.manager.DialogManager
import com.amberinternet.oktruck.util.network.RxNetwork
import com.amberinternet.oktruck.util.network.response.DistanceResponse
import com.amberinternet.oktruck.util.network.response.TruckTypesResponse
import com.amberinternet.oktruck.util.network.service.UserService
import rx.Subscription

class CreateWorkViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var userService: UserService
    private var dialogManager: DialogManager
    var truckTypes = arrayOf<TruckType>()
    var distance: Double? = null

    init {
        userService = UserService.getInstance(fragmentActivity)
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
    }

    fun getTruckType() : Subscription? {
        return RxNetwork<TruckTypesResponse>(fragmentActivity).request(userService.getTruckTypes(), onSuccess = { response ->
            truckTypes = response.truckTypes.subList(2, response.truckTypes.lastIndex).reversed().toTypedArray()
        }, onFailure = { error ->
            dialogManager.showError(error)
            fragmentActivity.finish()
        })
    }

    fun getDistance(sourceLatitude: Double, sourceLongitude: Double, destinationLatitude: Double, destinationLongitude: Double) : Subscription? {
        return RxNetwork<DistanceResponse>(fragmentActivity).request(userService.getDistance(sourceLatitude, sourceLongitude, destinationLatitude, destinationLongitude), onSuccess = { response ->
            distance = response.distance.value
            println("Distance: ${response.distance.value}m")
        }, onFailure = {})
    }
}