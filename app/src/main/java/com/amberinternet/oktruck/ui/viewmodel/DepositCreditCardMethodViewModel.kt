package com.amberinternet.oktruck.ui.viewmodel

import android.content.Intent
import androidx.databinding.ObservableField
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import com.amberinternet.oktruck.NAVIGATION_TAB_KEY
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.model.DepositOmise
import com.amberinternet.oktruck.ui.activity.MainActivity
import com.amberinternet.oktruck.util.enum.DepositType
import com.amberinternet.oktruck.util.manager.DialogManager
import com.amberinternet.oktruck.util.network.RxNetwork
import com.amberinternet.oktruck.util.network.response.DepositOmiseResponse
import com.amberinternet.oktruck.util.network.response.SuccessResponse
import com.amberinternet.oktruck.util.network.service.UserService
import rx.Subscription

class DepositCreditCardMethodViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var dialogManager: DialogManager
    private var userService: UserService
    var depositOmise = ObservableField<DepositOmise>()

    init {
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
        userService = UserService.getInstance(fragmentActivity)
    }

    fun requestDepositWithOmise(price: Int, token: String): Subscription? {
        return RxNetwork<DepositOmiseResponse>(fragmentActivity).request(userService.depositWithOmise(
            DepositType.OMISE.value, price, token, 1),
                onSuccess = { response ->
                    depositOmise.set(response.depositOmise)
                },
                onFailure = { error ->
                    dialogManager.showError(error)
                },
                onLoading = {
                    dialogManager.showLoading(R.string.sending_request)
                },
                onLoaded = {
                    dialogManager.hideLoading()
                }
        )
    }

    fun completePaymentOmise(): Subscription? {
        return RxNetwork<SuccessResponse>(fragmentActivity).request(userService.depositCompletePaymentOmise(depositOmise.get()!!.deposit.id),
                onSuccess = {
                    dialogManager.showSuccess(R.string.payment_successfully) {
                        val intent = Intent(fragmentActivity, MainActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        intent.putExtra(NAVIGATION_TAB_KEY, R.id.navi_tab_wallet)
                        fragmentActivity.startActivity(intent)
                    }
                },
                onFailure = { error ->
                    dialogManager.showError(error)
                },
                onLoading = {
                    dialogManager.showLoading(R.string.checking_payment)
                },
                onLoaded = {
                    dialogManager.hideLoading()
                })
    }
}