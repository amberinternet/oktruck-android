package com.amberinternet.oktruck.ui.viewmodel

import android.content.Intent
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import com.amberinternet.oktruck.NAVIGATION_TAB_KEY
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.ui.activity.MainActivity
import com.amberinternet.oktruck.util.manager.DialogManager
import com.amberinternet.oktruck.util.network.RxNetwork
import com.amberinternet.oktruck.util.network.response.SuccessResponse
import com.amberinternet.oktruck.util.network.service.UserService
import okhttp3.RequestBody
import rx.Subscription

class DepositTransferMethodViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var userService: UserService
    private var dialogManager: DialogManager

    init {
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
        userService = UserService.getInstance(fragmentActivity)
    }

    fun requestTransferMethod(type: RequestBody, price: RequestBody, payTime: RequestBody, slipImage: RequestBody): Subscription? {
        return RxNetwork<SuccessResponse>(fragmentActivity).request(userService.depositWithTransferSlip(type, price, payTime, slipImage),
                onSuccess = {
                    dialogManager.showSuccess(R.string.send_request_success) {
                        val intent = Intent(fragmentActivity, MainActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        intent.putExtra(NAVIGATION_TAB_KEY, R.id.navi_tab_wallet)
                        fragmentActivity.startActivity(intent)
                    }
                },
                onFailure = { error ->
                    dialogManager.showError(error)
                },
                onLoading = {
                    dialogManager.showLoading(R.string.sending_request)
                },
                onLoaded = {
                    dialogManager.hideLoading()
                }
        )
    }
}