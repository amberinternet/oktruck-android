package com.amberinternet.oktruck.ui.viewmodel

import android.graphics.drawable.AnimationDrawable
import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import rx.Observable
import rx.Subscription
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.model.User
import com.amberinternet.oktruck.util.enum.UserType
import com.amberinternet.oktruck.util.manager.DialogManager
import com.amberinternet.oktruck.util.manager.UserManager
import com.amberinternet.oktruck.util.network.RxNetwork
import com.amberinternet.oktruck.util.network.response.UserResponse
import com.amberinternet.oktruck.util.network.service.AuthService
import com.amberinternet.oktruck.util.network.service.UserService
import kotlinx.android.synthetic.main.activity_splash.loadingIndicator

class LoginViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var userManager: UserManager
    private var dialogManager: DialogManager
    private var userService: UserService
    private var authService: AuthService
    private var user: User
    var logInSuccess = ObservableBoolean()

    init {
        user = User.instance
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
        userService = UserService.getInstance(fragmentActivity)
        authService = AuthService.getInstance(fragmentActivity)
        userManager = UserManager.getInstance(fragmentActivity)
    }

    fun getFirebaseToken(callback: (token: String) -> Unit) {
        // TODO: getFirebaseToken
    }

    fun logIn(email: String, password: String, firebaseToken: String): Subscription? {
        val observable: Observable<UserResponse> = authService.logIn(
                email, password, firebaseToken, UserType.USER.type).flatMap {
            userManager.storeAccessToken(it.accessToken)
            return@flatMap userService.getProfile()
        }

        return RxNetwork<UserResponse>(fragmentActivity).request(observable,
                onSuccess = { response ->
                    user.init(response.user)
                    if (user.isVerify == 1) {
                        logInSuccess.set(true)
                    } else {
                        dialogManager.showAlert(R.string.alert_user_not_verify)
                    }
                },
                onFailure = { error ->
                    dialogManager.showError(error)
                },
                onLoading = {
                    val frameAnimation: AnimationDrawable = fragmentActivity.loadingIndicator.drawable as AnimationDrawable
                    frameAnimation.start()
                    fragmentActivity.loadingIndicator.visibility = View.VISIBLE
                },
                onLoaded = {
                    fragmentActivity.loadingIndicator.clearAnimation()
                    fragmentActivity.loadingIndicator.visibility = View.GONE
                }
        )
    }
}