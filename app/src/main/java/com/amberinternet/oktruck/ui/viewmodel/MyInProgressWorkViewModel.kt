package com.amberinternet.oktruck.ui.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import com.amberinternet.oktruck.model.Work
import com.amberinternet.oktruck.models.WorkPagination
import com.amberinternet.oktruck.util.enum.WorkQuery
import com.amberinternet.oktruck.util.manager.DialogManager
import com.amberinternet.oktruck.util.network.RxNetwork
import com.amberinternet.oktruck.util.network.response.WorkListResponse
import com.amberinternet.oktruck.util.network.service.UserService
import rx.Subscription

class MyInProgressWorkViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var userService: UserService
    private var dialogManager: DialogManager
    var workList = mutableListOf<Work>()
    var workPagination = ObservableField<WorkPagination>()
    var isRefresh = ObservableBoolean(false)

    init {
        userService = UserService.getInstance(fragmentActivity)
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
    }

    fun getMyInProgressWorkList(page: Int = 1): Subscription? {
        return RxNetwork<WorkListResponse>(fragmentActivity).request(userService.getMyWork(page, WorkQuery.STATUS_IN_PROGRESS.value, "", WorkQuery.SORT_DESC.value), onSuccess = { response ->
            if (isRefresh.get()) {
                workList.clear()
                isRefresh.set(false)
            }
            workList.addAll(response.workPagination.workList)
            workPagination.set(response.workPagination)
        }, onFailure = { error ->
            dialogManager.showError(error)
            if (isRefresh.get()) {
                isRefresh.set(false)
            }
        })
    }
}