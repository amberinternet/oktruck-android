package com.amberinternet.oktruck.ui.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import com.amberinternet.oktruck.model.News
import com.amberinternet.oktruck.util.manager.DialogManager
import com.amberinternet.oktruck.util.network.RxNetwork
import com.amberinternet.oktruck.util.network.response.NewsResponse
import com.amberinternet.oktruck.util.network.service.UserService
import rx.Subscription

class NewsDetailViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var userService: UserService
    private var dialogManager: DialogManager
    var news = ObservableField<News>()
    var isLoading = ObservableBoolean()

    init {
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
        userService = UserService.getInstance(fragmentActivity)
    }

    fun getNewsDetail(newsId: Int): Subscription? {
        return RxNetwork<NewsResponse>(fragmentActivity).request(userService.getNewsDetail(newsId),
                onSuccess = { response ->
                    news.set(response.news)
                },
                onFailure = {
                    isLoading.set(false)
                },
                onLoading = {
                    dialogManager.showLoading()
                },
                onLoaded = {
                    dialogManager.hideLoading()
                }
        )
    }
}