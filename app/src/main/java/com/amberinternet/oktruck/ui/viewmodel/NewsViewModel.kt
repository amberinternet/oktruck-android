package com.amberinternet.oktruck.ui.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import com.amberinternet.oktruck.model.News
import com.amberinternet.oktruck.models.NewsPagination
import com.amberinternet.oktruck.util.manager.DialogManager
import com.amberinternet.oktruck.util.network.RxNetwork
import com.amberinternet.oktruck.util.network.response.NewsListResponse
import com.amberinternet.oktruck.util.network.service.UserService
import rx.Subscription

class NewsViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var userService: UserService
    private var dialogManager: DialogManager
    var newsList = mutableListOf<News>()
    var newsPagination = ObservableField<NewsPagination>()
    var isRefresh = ObservableBoolean(false)

    init {
        userService = UserService.getInstance(fragmentActivity)
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
    }

    fun getNewsList(page: Int = 1) : Subscription? {
        return RxNetwork<NewsListResponse>(fragmentActivity).request(userService.getNews(page), onSuccess = { response ->
            if (isRefresh.get()) {
                newsList.clear()
                isRefresh.set(false)
            }
            newsList.addAll(response.newsPagination.newsList)
            newsPagination.set(response.newsPagination)
        }, onFailure = { error ->
            dialogManager.showError(error)
            if (isRefresh.get()) {
                isRefresh.set(false)
            }
        })
    }
}