package com.amberinternet.oktruck.ui.viewmodel

import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.databinding.ObservableBoolean
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import rx.Observable
import rx.Subscription
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.model.User
import com.amberinternet.oktruck.ui.activity.OTPActivity
import com.amberinternet.oktruck.ui.activity.RegisteredActivity
import com.amberinternet.oktruck.util.enum.UserType
import com.amberinternet.oktruck.util.manager.DialogManager
import com.amberinternet.oktruck.util.manager.UserManager
import com.amberinternet.oktruck.util.network.RxNetwork
import com.amberinternet.oktruck.util.network.response.SuccessResponse
import com.amberinternet.oktruck.util.network.response.UserResponse
import com.amberinternet.oktruck.util.network.service.AuthService
import com.amberinternet.oktruck.util.network.service.UserService
import kotlinx.android.synthetic.main.activity_splash.loadingIndicator

class OTPViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var dialogManager: DialogManager
    private var authService: AuthService
    private var user: User

    init {
        user = User.instance
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
        authService = AuthService.getInstance(fragmentActivity)
    }

    fun submit(otp: String): Subscription? {
        return RxNetwork<SuccessResponse>(fragmentActivity).request(authService.submitOTP(user.telephoneNumber, otp), onSuccess = {
            val intent = Intent(fragmentActivity, RegisteredActivity::class.java)
            fragmentActivity.startActivity(intent)
            user.clear()
        }, onFailure = { error ->
            dialogManager.showError(error)
        }, onLoading = {
            dialogManager.showLoading(R.string.checking_data)
        }, onLoaded = {
            dialogManager.hideLoading()
        })
    }

    fun requestOTP(): Subscription? {
        return RxNetwork<SuccessResponse>(fragmentActivity).request(authService.requestOTP(user.email, user.telephoneNumber), onSuccess = {
            Toast.makeText(fragmentActivity, R.string.sent_otp, Toast.LENGTH_LONG).show()
        }, onFailure = {error ->
            dialogManager.showError(error)
        })
    }
}