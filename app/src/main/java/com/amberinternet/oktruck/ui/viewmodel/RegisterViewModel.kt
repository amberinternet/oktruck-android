package com.amberinternet.oktruck.ui.viewmodel

import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.view.View
import androidx.core.content.ContextCompat.startActivity
import androidx.databinding.ObservableBoolean
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import rx.Observable
import rx.Subscription
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.model.User
import com.amberinternet.oktruck.ui.activity.OTPActivity
import com.amberinternet.oktruck.util.enum.UserType
import com.amberinternet.oktruck.util.manager.DialogManager
import com.amberinternet.oktruck.util.manager.UserManager
import com.amberinternet.oktruck.util.network.RxNetwork
import com.amberinternet.oktruck.util.network.response.SuccessResponse
import com.amberinternet.oktruck.util.network.response.UserResponse
import com.amberinternet.oktruck.util.network.service.AuthService
import com.amberinternet.oktruck.util.network.service.UserService
import kotlinx.android.synthetic.main.activity_splash.loadingIndicator

class RegisterViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var dialogManager: DialogManager
    private var authService: AuthService
    private var user: User

    init {
        user = User.instance
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
        authService = AuthService.getInstance(fragmentActivity)
    }

    fun signUp(): Subscription? {
        return RxNetwork<SuccessResponse>(fragmentActivity).request(authService.signUp(
            user.firstName,
            user.lastName,
            user.telephoneNumber,
            user.email,
            user.password,
            user.confirmPassword,
            user.company.name
        ), onSuccess = {
            requestOTP()
        }, onFailure = { error ->
            dialogManager.showError(error)
        }, onLoading = {
            dialogManager.showLoading(R.string.signing_up)
        }, onLoaded = {
            dialogManager.hideLoading()
        })
    }

    fun requestOTP(): Subscription? {
        return RxNetwork<SuccessResponse>(fragmentActivity).request(authService.requestOTP(user.email, user.telephoneNumber), onSuccess = {
            val intent = Intent(fragmentActivity, OTPActivity::class.java)
            fragmentActivity.startActivity(intent)
        }, onFailure = {error ->
            dialogManager.showError(error)
        })
    }
}