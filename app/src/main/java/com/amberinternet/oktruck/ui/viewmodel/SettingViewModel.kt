package com.amberinternet.oktruck.ui.viewmodel

import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.databinding.ObservableBoolean
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import rx.Observable
import rx.Subscription
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.model.User
import com.amberinternet.oktruck.ui.activity.LoginActivity
import com.amberinternet.oktruck.ui.activity.OTPActivity
import com.amberinternet.oktruck.ui.activity.RegisteredActivity
import com.amberinternet.oktruck.util.enum.UserType
import com.amberinternet.oktruck.util.manager.DialogManager
import com.amberinternet.oktruck.util.manager.UserManager
import com.amberinternet.oktruck.util.network.RxNetwork
import com.amberinternet.oktruck.util.network.response.SuccessResponse
import com.amberinternet.oktruck.util.network.response.UserResponse
import com.amberinternet.oktruck.util.network.service.AuthService
import com.amberinternet.oktruck.util.network.service.UserService
import kotlinx.android.synthetic.main.activity_splash.loadingIndicator

class SettingViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var userService: UserService
    private var userManager: UserManager
    private var user: User

    init {
        user = User.instance
        userService = UserService.getInstance(fragmentActivity)
        userManager = UserManager.getInstance(fragmentActivity)
    }

    private fun getFirebaseToken(callback: (token: String) -> Unit) {
        // TODO: getFirebaseToken
    }

    fun logOut() {
        user.clear()
        userManager.destroySharePreferences()
        val intent = Intent(fragmentActivity, LoginActivity::class.java)
        fragmentActivity.startActivity(intent)
        fragmentActivity.finish()
        RxNetwork<SuccessResponse>(fragmentActivity).request(userService.destroyToken(""))
    }
}