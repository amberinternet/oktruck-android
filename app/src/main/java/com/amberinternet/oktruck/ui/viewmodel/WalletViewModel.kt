package com.amberinternet.oktruck.ui.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import com.amberinternet.oktruck.model.Transaction
import com.amberinternet.oktruck.model.User
import com.amberinternet.oktruck.models.TransactionPagination
import com.amberinternet.oktruck.util.manager.DialogManager
import com.amberinternet.oktruck.util.network.RxNetwork
import com.amberinternet.oktruck.util.network.response.TransactionsListResponse
import com.amberinternet.oktruck.util.network.response.UserResponse
import com.amberinternet.oktruck.util.network.service.UserService
import rx.Subscription

class WalletViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var userService: UserService
    private var user: User
    var dialogManager: DialogManager
    var transactionList = mutableListOf<Transaction>()
    var transactionPagination = ObservableField<TransactionPagination>()
    var isRefresh = ObservableBoolean(false)
    var isLoadedProfile = ObservableBoolean(false)

    init {
        user = User.instance
        userService = UserService.getInstance(fragmentActivity)
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
    }

    fun getTransactionList(page: Int = 1, date: String): Subscription? {
        return RxNetwork<TransactionsListResponse>(fragmentActivity).request(userService.getTransaction(page, date), onSuccess = { response ->
            if (isRefresh.get()) {
                transactionList.clear()
                isRefresh.set(false)
            }
            transactionList.addAll(response.transactionPagination.transactionList)
            transactionPagination.set(response.transactionPagination)
        }, onFailure = {error ->
            dialogManager.showError(error)
            if (isRefresh.get()) {
                isRefresh.set(false)
            }
        })
    }

    fun getProfile(): Subscription? {
        return RxNetwork<UserResponse>(fragmentActivity).request(userService.getProfile(), onSuccess = { response ->
            user.init(response.user)
            isLoadedProfile.set(true)
        }, onFailure = { error ->

        })
    }
}