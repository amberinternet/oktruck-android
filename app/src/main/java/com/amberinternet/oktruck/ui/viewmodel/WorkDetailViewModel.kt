package com.amberinternet.oktruck.ui.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableInt
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.model.Work
import com.amberinternet.oktruck.util.manager.DialogManager
import com.amberinternet.oktruck.util.network.RxNetwork
import com.amberinternet.oktruck.util.network.response.SuccessResponse
import com.amberinternet.oktruck.util.network.response.WorkResponse
import com.amberinternet.oktruck.util.network.service.UserService
import rx.Subscription

class WorkDetailViewModel(val fragmentActivity: FragmentActivity) : ViewModel() {

    private var userService: UserService
    private var dialogManager: DialogManager
    lateinit var work: Work
    var status = ObservableInt()
    var isRefresh = ObservableBoolean(false)

    init {
        dialogManager = DialogManager.getInstance(fragmentActivity.supportFragmentManager)
        userService = UserService.getInstance(fragmentActivity)
    }

    fun getWorkDetail(): Subscription? {
        return RxNetwork<WorkResponse>(fragmentActivity).request(userService.getWorkDetail(work.id),
                onSuccess = { response ->
                    print(response.work.toString())
                    if (isRefresh.get()) {
                        work = response.work
                        isRefresh.set(false)
                    }
                },
                onFailure = { error ->
                    dialogManager.showError(error)
                    if (isRefresh.get()) {
                        isRefresh.set(false)
                    }
                })
    }

    fun cancelWork(): Subscription? {
        return RxNetwork<SuccessResponse>(fragmentActivity).request(userService.cancelWork(work.id),
                onSuccess = {
                    dialogManager.showSuccess(R.string.cancel_work_successfully) {
                        fragmentActivity.finish()
                    }
                },
                onFailure = { error ->
                    dialogManager.showError(error)
                },
                onLoading = {
                    dialogManager.showLoading(R.string.sending_data)
                },
                onLoaded = {
                    dialogManager.hideLoading()
                })
    }
}