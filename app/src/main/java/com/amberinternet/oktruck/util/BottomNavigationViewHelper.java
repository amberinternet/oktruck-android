package com.amberinternet.oktruck.util;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.lang.reflect.Field;

public class BottomNavigationViewHelper {

    public static void setIconSize(BottomNavigationView view, int width, int height, float topPaddingDp, Context context) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        for (int i = 0; i < menuView.getChildCount(); i++) {
            final View iconView = menuView.getChildAt(i).findViewById(com.google.android.material.R.id.icon);
            final ViewGroup.LayoutParams layoutParams = iconView.getLayoutParams();
            final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();

            if (i == 0) {   // center menu
                // set your width here
                layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width, displayMetrics);
                // set your height here
                layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (height) + topPaddingDp, displayMetrics);
                iconView.setLayoutParams(layoutParams);

                // set top padding here
                int topPaddingPixel = (int)((topPaddingDp - 2) * displayMetrics.density);
                iconView.setPadding(0,topPaddingPixel,0,0);
            }  if (i == 1) {   // center menu
                // set your width here
                layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width, displayMetrics);
                // set your height here
                layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (height) + topPaddingDp, displayMetrics);
                iconView.setLayoutParams(layoutParams);

                // set top padding here
                int topPaddingPixel = (int)((topPaddingDp) * displayMetrics.density);
                iconView.setPadding(4,topPaddingPixel,0,0);
            } else if (i == 2) {   // center menu
                // set your width here
                layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width + 1, displayMetrics);
                // set your height here
                layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (height + 1) + topPaddingDp, displayMetrics);
                iconView.setLayoutParams(layoutParams);

                // set top padding here
                int topPaddingPixel = (int)((topPaddingDp - 2) * displayMetrics.density);
                iconView.setPadding(0,topPaddingPixel,0,0);
            } else if (i == 3) {   // center menu
                // set your width here
                layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width + 1, displayMetrics);
                // set your height here
                layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (height + 1) + topPaddingDp, displayMetrics);
                iconView.setLayoutParams(layoutParams);

                // set top padding here
                int topPaddingPixel = (int)((topPaddingDp - 1) * displayMetrics.density);
                iconView.setPadding(6,topPaddingPixel,0,0);
            } else {
                // set your width here
                layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width, displayMetrics);
                // set your height here
                layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (height)+ topPaddingDp, displayMetrics);
                iconView.setLayoutParams(layoutParams);

                // set top padding here
                int topPaddingPixel = (int)((topPaddingDp) * displayMetrics.density);
                iconView.setPadding(0,topPaddingPixel,0,0);
            }
        }
    }

    public static void setLabelPadding(BottomNavigationView view, float bottomPaddingDp, Context context) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        for (int i = 0; i < menuView.getChildCount(); i++) {
            final View largeLabel = menuView.getChildAt(i).findViewById(com.google.android.material.R.id.largeLabel);
            final View smallLabel = menuView.getChildAt(i).findViewById(com.google.android.material.R.id.smallLabel);
            final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();

            // set top padding here
            int bottomPaddingPixel = (int)(bottomPaddingDp * displayMetrics.density);
            largeLabel.setPadding(0,0,0,bottomPaddingPixel);
            smallLabel.setPadding(0,0,0,bottomPaddingPixel);
        }
    }
}
