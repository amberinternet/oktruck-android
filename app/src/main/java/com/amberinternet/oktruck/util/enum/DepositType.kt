package com.amberinternet.oktruck.util.enum

import com.amberinternet.oktruck.MyApplication.Companion.resources
import com.amberinternet.oktruck.R

enum class DepositType(val value: Int, val method: String) {
    OMISE(1, resources.getString(R.string.deposit_type_omise)),
    TRANSFER(2, resources.getString(R.string.deposit_type_transfer))
}