package com.amberinternet.oktruck.util.enum

import com.amberinternet.oktruck.MyApplication.Companion.resources
import com.amberinternet.oktruck.R

enum class PaymentMethod(val status: Int, val pay: String, val receive: String) {
    CASH_SOURCE(1, resources.getString(R.string.pay_cash_source), resources.getString(R.string.receive_cash_source)),
    CASH_DESTINATION(2, resources.getString(R.string.pay_cash_destination), resources.getString(R.string.receive_cash_destination)),
    WALLET(3, resources.getString(R.string.pay_wallet), resources.getString(R.string.receive_wallet)),
    OMISE(4, resources.getString(R.string.pay_omise), resources.getString(R.string.receive_omise))
}