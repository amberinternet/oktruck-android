package com.amberinternet.oktruck.util.enum

import com.amberinternet.oktruck.MyApplication.Companion.resources
import com.amberinternet.oktruck.R

enum class TransactionRequestStatus(val status: Int, val depositMessage: String, val withdrawMessage: String) {
    WAITING(0, resources.getString(R.string.deposit_waiting), resources.getString(R.string.withdraw_waiting)),
    APPROVED(1, resources.getString(R.string.deposit_approved), resources.getString(R.string.withdraw_approved)),
    REJECT(2, resources.getString(R.string.deposit_reject), resources.getString(R.string.withdraw_reject))
}