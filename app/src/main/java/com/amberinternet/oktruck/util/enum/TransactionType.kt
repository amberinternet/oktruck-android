package com.amberinternet.oktruck.util.enum

enum class TransactionType(val value: Int, val spinnerValue: Int = 0) {
    DEPOSIT(1, 1),
    CREATE_WORK(2),
    CANCEL_WORK(3),
    ADMIN_ADD(4),
    ADMIN_DELETE(5),
    WITHDRAW(6,2),
    FINISH(7),
    ACCEPT_WORK(8),
    CANCEL_WORK_OMISE(9);
}