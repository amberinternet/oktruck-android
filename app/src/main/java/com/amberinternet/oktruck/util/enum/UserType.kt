package com.amberinternet.oktruck.util.enum

enum class UserType(val type: String) {
    USER("user")
}