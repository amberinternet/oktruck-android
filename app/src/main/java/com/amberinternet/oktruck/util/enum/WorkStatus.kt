package com.amberinternet.oktruck.util.enum

import com.amberinternet.oktruck.MyApplication.Companion.resources
import com.amberinternet.oktruck.R

enum class WorkStatus(val status: Int, val readableStatus: String, val statusColor: Int) {
    PAYMENT_FAIL(0, resources.getString(R.string.work_status_payment_fail), R.color.cancelColor),
    FIND_TRUCK(1, resources.getString(R.string.work_status_find_truck), R.color.inProgressColor),
    WAITING_CONFIRM_WORK(2, resources.getString(R.string.work_status_waiting_confirm_work), R.color.inProgressColor),
    WAITING_RECEIVE_ITEM(3, resources.getString(R.string.work_status_waiting_receive_item), R.color.inProgressColor),
    ARRIVED_SOURCE(4, resources.getString(R.string.work_status_arrived_source), R.color.inProgressColor),
    WAITING_SENT_ITEM(5, resources.getString(R.string.work_status_waiting_sent_item), R.color.inProgressColor),
    ARRIVED_DESTINATION(6, resources.getString(R.string.work_status_arrived_destination), R.color.inProgressColor),
    COMPLETE(7, resources.getString(R.string.work_status_complete), R.color.finishColor),
    CANCEL(8, resources.getString(R.string.work_status_cancel), R.color.cancelColor);
}