package com.amberinternet.oktruck.util.manager

import android.graphics.Bitmap
import android.net.Uri
import androidx.fragment.app.FragmentManager
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.model.AdditionalService
import com.amberinternet.oktruck.model.Destination
import com.amberinternet.oktruck.model.Source
import com.amberinternet.oktruck.model.TruckType
import com.amberinternet.oktruck.toDateServerFormat
import com.amberinternet.oktruck.ui.bottomsheetdialog.*
import java.util.*
import kotlin.collections.ArrayList

class BottomSheetDialogManager private constructor(private val fragmentManager: FragmentManager) {

    companion object {

        @Volatile
        private var INSTANCE: BottomSheetDialogManager? = null

        fun getInstance(fragmentManager: FragmentManager): BottomSheetDialogManager = INSTANCE
                ?: synchronized(this) {
                    INSTANCE ?: BottomSheetDialogManager(fragmentManager)
                }
    }

    fun showTruckTypes(truckTypes: Array<TruckType>, selectedTruckType: TruckType?, onSelect: (truckType: TruckType) -> Unit?) {
        TruckTypesBottomSheetDialogFragment.newInstance(truckTypes, selectedTruckType).apply {
            this.onSelect = onSelect
        }.show(fragmentManager, TruckTypesBottomSheetDialogFragment::class.java.simpleName)
    }

    fun showSearchSourcesAddress(onCompleteSource: (source: Source) -> Unit) {
        SearchAddressBottomSheetDialogFragment.newInstance(R.string.title_source).apply {
            this.onCompleteSource = onCompleteSource
        }.show(fragmentManager, SearchAddressBottomSheetDialogFragment::class.java.simpleName)
    }

    fun showSearchDestinationAddress(onCompleteDestination: (destination: Destination) -> Unit) {
        SearchAddressBottomSheetDialogFragment.newInstance(R.string.title_destination).apply {
            this.onCompleteDestination = onCompleteDestination
        }.show(fragmentManager, SearchAddressBottomSheetDialogFragment::class.java.simpleName)
    }

    fun showImagePicker(onSelect: (imageUri: Uri) -> Unit) {
        ImagePickerBottomSheetDialogFragment.newInstance().apply {
            this.onSelect = onSelect
        }.show(fragmentManager, ImagePickerBottomSheetDialogFragment::class.java.simpleName)
    }

    fun showAdditionalServices(additionalServices: Array<AdditionalService>, selectedAdditionalServices: ArrayList<AdditionalService>?, onSelect: (additionalServices: ArrayList<AdditionalService>) -> Unit?) {
        AdditionalServicesBottomSheetDialogFragment.newInstance(additionalServices, selectedAdditionalServices).apply {
            this.onSelect = onSelect
        }.show(fragmentManager, AdditionalServicesBottomSheetDialogFragment::class.java.simpleName)
    }

    fun showReceiptDatePicker(onSelect: (date: String, time: String) -> Unit?) {
        DatePickerBottomSheetDialogFragment.newInstance(R.string.pick_up_date, Calendar.getInstance().time.toDateServerFormat()).apply {
            this.onSelect = onSelect
        }.show(fragmentManager, DatePickerBottomSheetDialogFragment::class.java.simpleName)
    }

    fun showDeliveryDatePicker(startDate: String, onSelect: (date: String, time: String) -> Unit?) {
        DatePickerBottomSheetDialogFragment.newInstance(R.string.drop_off_date, startDate).apply {
            this.onSelect = onSelect
        }.show(fragmentManager, DatePickerBottomSheetDialogFragment::class.java.simpleName)
    }

    fun showDeposit() {
        DepositBottomSheetDialogFragment.newInstance().show(fragmentManager, DepositBottomSheetDialogFragment::class.java.simpleName)
    }

}