package com.amberinternet.oktruck.util.manager

import androidx.fragment.app.FragmentManager
import com.amberinternet.oktruck.MyApplication.Companion.resources
import com.amberinternet.oktruck.R
import com.amberinternet.oktruck.ui.dialog.*
import com.amberinternet.oktruck.ui.dialog.listener.ErrorDialogListener
import th.co.growthd.oktrucker.ui.dialog.SuccessDialogFragment

class DialogManager private constructor(private val fragmentManager: FragmentManager) {

    companion object {

        @Volatile
        private var INSTANCE: DialogManager? = null

        fun getInstance(fragmentManager: FragmentManager): DialogManager = INSTANCE
                ?: synchronized(this) {
                    INSTANCE ?: DialogManager(fragmentManager)
                }
    }

    fun showContactAdmin(titleRes: Int = R.string.please_contact_admin, messageRes: Int = R.string.need_help_from_admin) {
        ContactAdminDialogFragment.newInstance(titleRes, messageRes).show(fragmentManager, ContactAdminDialogFragment::class.java.simpleName)
    }

    fun showAlert(messageRes: Int) {
        AlertDialogFragment.newInstance(messageRes).show(fragmentManager, AlertDialogFragment::class.java.simpleName)
    }

    fun showAlert(message: String) {
        AlertDialogFragment.newInstance(message).show(fragmentManager, AlertDialogFragment::class.java.simpleName)
    }

    fun showAlert(messageRes: Int, onDismiss: (() -> Unit)? = null) {
        AlertDialogFragment.newInstance(messageRes).apply {
            this.onDismiss = onDismiss
        }.show(fragmentManager, AlertDialogFragment::class.java.simpleName)
    }

    fun showAlert(titleRes: Int, messageRes: Int, actionRes: Int, onDismiss: (() -> Unit)? = null) {
        AlertDialogFragment.newInstance(titleRes, messageRes, actionRes).apply {
            this.onDismiss = onDismiss
        }.show(fragmentManager, AlertDialogFragment::class.java.simpleName)
    }

    fun showAlert(title: String, message: String, action: String, onDismiss: (() -> Unit)? = null) {
        AlertDialogFragment.newInstance(title, message, action).apply {
            this.onDismiss = onDismiss
        }.show(fragmentManager, AlertDialogFragment::class.java.simpleName)
    }

    fun showConfirm(titleRes: Int, messageRes: Int, onConfirm: (() -> Unit)? = null) {
        ConfirmDialogFragment.newInstance(titleRes, messageRes).apply {
            this.onConfirm = onConfirm
        }.show(fragmentManager, ConfirmDialogFragment::class.java.simpleName)
    }

    fun showConfirm(titleRes: Int, messageRes: Int, onConfirm: (() -> Unit)? = null, onCancel: (() -> Unit)? = null) {
        ConfirmDialogFragment.newInstance(titleRes, messageRes).apply {
            this.onConfirm = onConfirm
            this.onCancel = onCancel
        }.show(fragmentManager, ConfirmDialogFragment::class.java.simpleName)
    }

    fun showError(error: String) {
        ErrorDialogFragment.newInstance(error).show(fragmentManager, ErrorDialogFragment::class.java.simpleName)
    }

    fun showError(error: String, listener: ErrorDialogListener) {
        ErrorDialogFragment.newInstance(error).apply {
            errorDialogListener = listener
        }.show(fragmentManager, ErrorDialogFragment::class.java.simpleName)
    }

    fun showSuccess(messageRes: Int, onDismiss: (() -> Unit)? = null) {
        SuccessDialogFragment.newInstance(messageRes).apply {
            this.onDismiss = onDismiss
        }.show(fragmentManager, SuccessDialogFragment::class.java.simpleName)
    }

    fun showSuccess(titleRes: Int, messageRes: Int, onDismiss: (() -> Unit)? = null) {
        SuccessDialogFragment.newInstance(titleRes, messageRes).apply {
            this.onDismiss = onDismiss
        }.show(fragmentManager, SuccessDialogFragment::class.java.simpleName)
    }

    fun showLoading(loading: String = resources.getString(R.string.loading_data)) {
        ProgressBarDialogFragment.newInstance(loading).show(fragmentManager, ProgressBarDialogFragment::class.java.simpleName)
    }

    fun showLoading(loadingRes: Int) {
        ProgressBarDialogFragment.newInstance(loadingRes).show(fragmentManager, ProgressBarDialogFragment::class.java.simpleName)
    }

    fun hideLoading() {
        val fragment = fragmentManager.findFragmentByTag(ProgressBarDialogFragment::class.java.simpleName) as? ProgressBarDialogFragment
        fragment?.let { it.dismiss() }
    }
}