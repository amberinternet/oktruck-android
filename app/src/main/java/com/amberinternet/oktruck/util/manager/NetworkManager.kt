package com.amberinternet.oktruck.util.manager

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import com.amberinternet.oktruck.BASE_API_URL
import com.amberinternet.oktruck.util.network.RequestInterceptor

class NetworkManager private constructor(val context: Context) {

    private val gson: Gson = GsonBuilder().serializeNulls().setPrettyPrinting().create()
    private val headerHttpClient: OkHttpClient = getOKHttpClient(true)
    private val nonHeaderHttpClient: OkHttpClient = getOKHttpClient(false)
    val headerRetrofit: Retrofit = getRetrofit(headerHttpClient)
    val nonHeaderRetrofit: Retrofit = getRetrofit(nonHeaderHttpClient)

    companion object {

        @Volatile
        private var INSTANCE: NetworkManager? = null

        fun getInstance(context: Context): NetworkManager = INSTANCE
                ?: synchronized(this) {
                    INSTANCE ?: NetworkManager(context)
                }
    }

    fun isConnectingInternet(): Boolean {
        val connectivity = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivity?.allNetworkInfo?.let {
            for (i in it.indices)
                if (it[i].state == NetworkInfo.State.CONNECTED) {
                    return true
                }
        }

        return false
    }

    private fun getOKHttpClient(isHeader: Boolean): OkHttpClient {
        val builder = OkHttpClient.Builder()

        if (isHeader) {
            builder.addInterceptor(RequestInterceptor(context))
        }

        return builder.build()
    }

    private fun getRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BASE_API_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
    }
}