package com.amberinternet.oktruck.util.manager

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.amberinternet.oktruck.*
import com.amberinternet.oktruck.model.AccessToken
import com.amberinternet.oktruck.model.Destination
import com.amberinternet.oktruck.model.Source
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken





class UserManager(val context: Context) {

    private val sharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    val token: String get() = sharedPreferences.getString(TOKEN_KEY, INITIAL_STRING)
    val isEnableNotification: Boolean get() = sharedPreferences.getBoolean(IS_ENABLE_NOTIFICATION_KEY, true)

    companion object {

        @Volatile
        private var INSTANCE: UserManager? = null

        fun getInstance(context: Context): UserManager = INSTANCE
                ?: synchronized(this) {
                    INSTANCE ?: UserManager(context)
                }
    }

    fun storeAccessToken(accessToken: AccessToken) {
        sharedPreferences.putString(TOKEN_KEY, accessToken.token)
    }

    fun hasToken(): Boolean {
        return sharedPreferences.contains(TOKEN_KEY)
    }

    fun saveSourceAddressHistory(address: Source) {
        val addressHistoryList = getSourceAddressHistoryList()
        addressHistoryList.add(0, address)

        val editor = sharedPreferences.edit()
        val json = Gson().toJson(addressHistoryList)
        editor.putString(SOURCE_ADDRESS_HISTORY_LIST_KEY, json)
        editor.apply()
    }

    fun getSourceAddressHistoryList(): ArrayList<Source> {
        val json = sharedPreferences.getString(SOURCE_ADDRESS_HISTORY_LIST_KEY, "[]")
        val type = object : TypeToken<ArrayList<Source>>() {}.type
        return Gson().fromJson(json, type)
    }

    fun saveDestinationAddressHistory(address: Destination) {
        val addressHistoryList = getDestinationAddressHistoryList()
        addressHistoryList.add(0, address)

        val editor = sharedPreferences.edit()
        val json = Gson().toJson(addressHistoryList)
        editor.putString(DESTINATION_ADDRESS_HISTORY_LIST_KEY, json)
        editor.apply()
    }

    fun getDestinationAddressHistoryList(): ArrayList<Destination> {
        val json = sharedPreferences.getString(DESTINATION_ADDRESS_HISTORY_LIST_KEY, "[]")
        val type = object : TypeToken<ArrayList<Destination>>() {}.type
        return Gson().fromJson(json, type)
    }

    fun setEnableNotification(isEnable: Boolean) {
        sharedPreferences.putBoolean(IS_ENABLE_NOTIFICATION_KEY, isEnable)
    }

    fun destroySharePreferences() {
        sharedPreferences.edit().clear().apply()
    }

}