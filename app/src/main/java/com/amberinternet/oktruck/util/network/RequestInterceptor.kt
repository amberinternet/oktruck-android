package com.amberinternet.oktruck.util.network

import android.content.Context
import android.content.Intent
import okhttp3.*
import org.json.JSONException
import org.json.JSONObject
import com.amberinternet.oktruck.BASE_API_URL
import com.amberinternet.oktruck.HEADER_KEY
import com.amberinternet.oktruck.INITIAL_STRING
import com.amberinternet.oktruck.TOKEN_TYPE
import com.amberinternet.oktruck.model.AccessToken
import com.amberinternet.oktruck.model.User
import com.amberinternet.oktruck.ui.activity.LoginActivity
import com.amberinternet.oktruck.util.manager.UserManager
import com.amberinternet.oktruck.util.network.response.SuccessResponse
import com.amberinternet.oktruck.util.network.service.UserService
import java.io.IOException
import java.net.HttpURLConnection

class RequestInterceptor(val context: Context) : Interceptor {

    private val userManager = UserManager.getInstance(context)

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {

        var request = chain.request()
        request = request.newBuilder()
                .addHeader(HEADER_KEY, TOKEN_TYPE + userManager.token)
                .build()

        var response = chain.proceed(request)

        // token is expired
        if (response.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {

            // try to refresh token
            val responseBody = FormBody.Builder().build()
            val url = BASE_API_URL + "token/refresh"
            val requestToken = Request.Builder()
                    .url(url)
                    .header(HEADER_KEY, TOKEN_TYPE + userManager.token)
                    .post(responseBody)
                    .build()
            response = chain.proceed(requestToken)

            // refresh token is not expired
            if (response.isSuccessful) {
                // get and store new token
                try {
                    val responseString = response.body()?.string()
                    val jsonObject = JSONObject(responseString)
                    val token = jsonObject.getJSONObject("data").getString("token")
                    val accessToken = AccessToken(token)
                    userManager.storeAccessToken(accessToken)

                    // get response from new token
                    val newRequest = request.newBuilder()
                            .removeHeader(HEADER_KEY)
                            .addHeader(HEADER_KEY, TOKEN_TYPE + token).build()
                    response = chain.proceed(newRequest)

                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            } else {
                User.instance.clear()
                userManager.destroySharePreferences()
                val intent = Intent(context, LoginActivity::class.java)
                context.startActivity(intent)
                RxNetwork<SuccessResponse>(context).request(UserService.getInstance(context).destroyToken(INITIAL_STRING))
            }
        }

        return response
    }
}