package com.amberinternet.oktruck.util.network.response

import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.model.AccessToken

data class AccessTokenResponse (@SerializedName("data") var accessToken: AccessToken)