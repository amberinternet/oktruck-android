package com.amberinternet.oktruck.util.network.response

import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.model.DepositOmise

data class DepositOmiseResponse(
        @SerializedName("data") var depositOmise: DepositOmise
)
