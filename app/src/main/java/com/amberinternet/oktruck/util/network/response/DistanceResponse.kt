package com.amberinternet.oktruck.util.network.response

import com.google.gson.annotations.SerializedName

data class DistanceResponse(@SerializedName("data") var distance: Distance)

data class Distance(@SerializedName("distance") var value: Double)