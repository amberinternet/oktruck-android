package com.amberinternet.oktruck.util.network.response

import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.getError

class ErrorDescription {

    @SerializedName("first_name")
    var firstNameList: List<String> = mutableListOf()

    @SerializedName("last_name")
    var lastNameList: List<String> = mutableListOf()

    @SerializedName("username")
    var usernameList: List<String> = mutableListOf()

    @SerializedName("email")
    var emailList: List<String> = mutableListOf()

    @SerializedName("telephone_number")
    var telephoneNumberList: List<String> = mutableListOf()

    @SerializedName("password")
    var passwordList: List<String> = mutableListOf()

    @SerializedName("pay_time")
    var payTimeList: List<String> = mutableListOf()

    @SerializedName("price")
    var priceList: List<String> = mutableListOf()

    @SerializedName("pay_slip_image")
    var slipImageList: List<String> = mutableListOf()

    @SerializedName("autograph")
    var signatureList: List<String> = mutableListOf()

    private var error: String? = ""

    fun getError(): String {
        error += firstNameList.getError()
        error += lastNameList.getError()
        error += usernameList.getError()
        error += emailList.getError()
        error += telephoneNumberList.getError()
        error += passwordList.getError()
        error += payTimeList.getError()
        error += priceList.getError()
        error += slipImageList.getError()
        error += signatureList.getError()
        return if (error != null) error!!.trim { it <= ' ' } else ""
    }
}