package com.amberinternet.oktruck.util.network.response

import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.INITIAL_STRING
import com.amberinternet.oktruck.MyApplication.Companion.resources
import com.amberinternet.oktruck.R

class ErrorResponse {

    @SerializedName("error")
    var error: String = ""
        get() {
            return when (field) {
                "validation_failed", "could_not_create_token" -> resources.getString(R.string.found_error)
                "invalid_credentials" -> resources.getString(R.string.email_password_incorrect)
                "token_invalid", "token_not_provided", "token_expired", "ERROR" -> resources.getString(R.string.token_invalid)
                "user_telephone_number_is_not_verified", "user_is_not_verified" -> resources.getString(R.string.account_not_verify)
                "user_status_is_deactive" -> resources.getString(R.string.account_suspended)
                "unable_to_send_sms" -> resources.getString(R.string.cannot_send_otp)
                "invalid_otp" -> resources.getString(R.string.otp_invalid)
                "otp_is_already_expired" -> resources.getString(R.string.otp_expired)
                "topup_not_found" -> resources.getString(R.string.payment_failed)
                "credit_is_not_enough" -> resources.getString(R.string.credit_not_enough)
                "news_not_found" -> resources.getString(R.string.news_not_found)
                "work_cannot_cancel" -> resources.getString(R.string.work_cannot_cancel)
                else -> field.replace("_", " ")
            }
        }

    @SerializedName("description")
    var errorDescription: ErrorDescription? = null

    fun getErrorDescription(): String {
        return errorDescription?.getError() ?: INITIAL_STRING
    }

    override fun toString(): String {
        return "ErrorResponse(errorDescription=$errorDescription)"
    }
}