package com.amberinternet.oktruck.util.network.response

import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.models.NewsPagination

data class NewsListResponse(
        @SerializedName("data") var newsPagination: NewsPagination
)