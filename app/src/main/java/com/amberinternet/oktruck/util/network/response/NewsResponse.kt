package com.amberinternet.oktruck.util.network.response

import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.model.News

data class NewsResponse(
        @SerializedName("data") var news: News
)