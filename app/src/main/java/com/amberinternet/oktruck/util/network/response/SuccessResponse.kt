package com.amberinternet.oktruck.util.network.response

import com.google.gson.annotations.SerializedName

data class SuccessResponse(@SerializedName("data") var message: String)