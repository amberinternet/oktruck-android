package com.amberinternet.oktruck.util.network.response

import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.models.TransactionPagination

data class TransactionsListResponse(
        @SerializedName("data") var transactionPagination: TransactionPagination
)