package com.amberinternet.oktruck.util.network.response

import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.model.TruckType

data class TruckTypesResponse (@SerializedName("data") var truckTypes: List<TruckType>)