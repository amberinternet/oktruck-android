package com.amberinternet.oktruck.util.network.response

import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.model.User

data class UserResponse(@SerializedName("data") var user: User)