package com.amberinternet.oktruck.util.network.response

import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.models.WorkPagination

data class WorkListResponse(
        @SerializedName("data") var workPagination: WorkPagination
)