package com.amberinternet.oktruck.util.network.response

import com.amberinternet.oktruck.model.WorkOmise
import com.google.gson.annotations.SerializedName

data class WorkOmiseResponse(
    @SerializedName("data") var workOmise: WorkOmise
)