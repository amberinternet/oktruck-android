package com.amberinternet.oktruck.util.network.response

import com.google.gson.annotations.SerializedName
import com.amberinternet.oktruck.model.Work

data class WorkResponse(
        @SerializedName("data") var work: Work
)