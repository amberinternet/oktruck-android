package com.amberinternet.oktruck.util.network.service

import android.content.Context
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import rx.Observable
import com.amberinternet.oktruck.util.manager.NetworkManager
import com.amberinternet.oktruck.util.network.response.AccessTokenResponse
import com.amberinternet.oktruck.util.network.response.SuccessResponse

interface AuthService {

    companion object {
        fun getInstance(context: Context): AuthService {
            return NetworkManager.getInstance(context).nonHeaderRetrofit.create(AuthService::class.java)
        }
    }

    @POST("token")
    @FormUrlEncoded
    fun logIn(@Field("email") email: String, @Field("password") password: String, @Field("token") firebaseToken: String, @Field("type") type: String): Observable<AccessTokenResponse>

    @POST("signup/user")
    @FormUrlEncoded
    fun signUp(
            @Field("first_name") firstName: String,
            @Field("last_name") lastName: String,
            @Field("username") telephoneNumber: String,
            @Field("email") email: String,
            @Field("password") password: String,
            @Field("password_confirmation") confirmPassword: String,
            @Field("company_name") companyName: String
    ): Observable<SuccessResponse>

    @POST("otp")
    @FormUrlEncoded
    fun requestOTP(@Field("email") email: String, @Field("telephone_number") telephoneNumber: String): Observable<SuccessResponse>

    @POST("otp/submit")
    @FormUrlEncoded
    fun submitOTP(@Field("telephone_number") telephoneNumber: String, @Field("otp") otp: String): Observable<SuccessResponse>

}