package com.amberinternet.oktruck.util.network.service

import android.content.Context
import com.amberinternet.oktruck.model.AdditionalService
import okhttp3.RequestBody
import retrofit2.http.*
import rx.Observable
import com.amberinternet.oktruck.util.manager.NetworkManager
import com.amberinternet.oktruck.util.network.response.*
import okhttp3.MultipartBody
import retrofit2.Call
import java.util.HashMap

interface UserService {

    companion object {
        fun getInstance(context: Context): UserService {
            return NetworkManager.getInstance(context).headerRetrofit.create(UserService::class.java)
        }
    }

    @POST("token/refresh")
    fun refreshToken(): Observable<AccessTokenResponse>

    @POST("token/destroy")
    @FormUrlEncoded
    fun destroyToken(@Field("token") firebaseToken: String): Observable<SuccessResponse>

    @GET("me")
    fun getProfile(): Observable<UserResponse>

    @GET("car/type")
    fun getTruckTypes(): Observable<TruckTypesResponse>

    @GET("distance")
    fun getDistance(
        @Query("source_latitude") sourceLatitude: Double,
        @Query("source_longitude") sourceLongitude: Double,
        @Query("destination_latitude") destinationLatitude: Double,
        @Query("destination_longitude") destinationLongitude: Double): Observable<DistanceResponse>

    @Multipart
    @POST("work")
    fun createWork(
        @Part("car_type_id") truckTypeId: RequestBody,
        @Part("payment_method") paymentMethod: RequestBody,
        @Part("user_name") userName: RequestBody,
        @Part("user_telephone_number") userTelephoneNumber: RequestBody,
        @Part("user_note") note: RequestBody,
        @Part("item_name") itemName: RequestBody,
        @Part("item_quantity") quantity: RequestBody,
        @Part("item_weight") weight: RequestBody,
        @Part("item_image\"; filename=\"item_image.jpg\" ") itemImage: RequestBody,
        @Part("source_latitude") sourceLatitude: RequestBody,
        @Part("source_longitude") sourceLongitude: RequestBody,
        @Part("source_full_address") sourceFullAddress: RequestBody,
        @Part("source_address_subdistrict") sourceSubdistrict: RequestBody,
        @Part("source_address_district") sourceDistrict: RequestBody,
        @Part("source_address_province") sourceProvince: RequestBody,
        @Part("source_contact_name") sourceContactName: RequestBody,
        @Part("source_contact_telephone_number") sourceContactTelephoneNumber: RequestBody,
        @Part("source_shop_name") sourceName: RequestBody,
        @Part("source_landmark") sourceLandmark: RequestBody,
        @Part("received_date") receivedDate: RequestBody,
        @Part("received_time") receivedTime: RequestBody,
        @Part("destination_latitude") destinationLatitude: RequestBody,
        @Part("destination_longitude") destinationLongitude: RequestBody,
        @Part("destination_full_address") destinationFullAddress: RequestBody,
        @Part("destination_address_subdistrict") destinationSubdistrict: RequestBody,
        @Part("destination_address_district") destinationDistrict: RequestBody,
        @Part("destination_address_province") destinationProvince: RequestBody,
        @Part("destination_contact_name") destinationContactName: RequestBody,
        @Part("destination_contact_telephone_number") destinationContactTelephoneNumber: RequestBody,
        @Part("destination_shop_name") destinationName: RequestBody,
        @Part("destination_landmark") destinationLandmark: RequestBody,
        @Part("delivered_date") deliveredDate: RequestBody,
        @Part("delivered_time") deliveredTime: RequestBody,
        @PartMap additionalServices: HashMap<String, RequestBody>): Observable<SuccessResponse>

    @Multipart
    @POST("work")
    fun createWorkWithOmise(
        @Part("car_type_id") truckTypeId: RequestBody,
        @Part("payment_method") paymentMethod: RequestBody,
        @Part("omise_token") omiseToken: RequestBody,
        @Part("redirect_uri") redirectUri: RequestBody,
        @Part("user_name") userName: RequestBody,
        @Part("user_telephone_number") userTelephoneNumber: RequestBody,
        @Part("user_note") note: RequestBody,
        @Part("item_name") itemName: RequestBody,
        @Part("item_quantity") quantity: RequestBody,
        @Part("item_weight") weight: RequestBody,
        @Part("item_image\"; filename=\"item_image.jpg\" ") itemImage: RequestBody,
        @Part("source_latitude") sourceLatitude: RequestBody,
        @Part("source_longitude") sourceLongitude: RequestBody,
        @Part("source_full_address") sourceFullAddress: RequestBody,
        @Part("source_address_subdistrict") sourceSubdistrict: RequestBody,
        @Part("source_address_district") sourceDistrict: RequestBody,
        @Part("source_address_province") sourceProvince: RequestBody,
        @Part("source_contact_name") sourceContactName: RequestBody,
        @Part("source_contact_telephone_number") sourceContactTelephoneNumber: RequestBody,
        @Part("source_shop_name") sourceName: RequestBody,
        @Part("source_landmark") sourceLandmark: RequestBody,
        @Part("received_date") receivedDate: RequestBody,
        @Part("received_time") receivedTime: RequestBody,
        @Part("destination_latitude") destinationLatitude: RequestBody,
        @Part("destination_longitude") destinationLongitude: RequestBody,
        @Part("destination_full_address") destinationFullAddress: RequestBody,
        @Part("destination_address_subdistrict") destinationSubdistrict: RequestBody,
        @Part("destination_address_district") destinationDistrict: RequestBody,
        @Part("destination_address_province") destinationProvince: RequestBody,
        @Part("destination_contact_name") destinationContactName: RequestBody,
        @Part("destination_contact_telephone_number") destinationContactTelephoneNumber: RequestBody,
        @Part("destination_shop_name") destinationName: RequestBody,
        @Part("destination_landmark") destinationLandmark: RequestBody,
        @Part("delivered_date") deliveredDate: RequestBody,
        @Part("delivered_time") deliveredTime: RequestBody,
        @PartMap additionalServices: HashMap<String, RequestBody>): Observable<WorkOmiseResponse>

    @POST("work/{work_id}/payment/success")
    fun createWorkCompletePaymentOmise(@Path("work_id") workId: Int): Observable<SuccessResponse>

    @GET("news")
    fun getNews(@Query("page") page: Int): Observable<NewsListResponse>

    @GET("news/{news_id}")
    fun getNewsDetail(@Path("news_id") newsId: Int): Observable<NewsResponse>

    @GET("me/credit-log")
    fun getTransaction(@Query("page") page: Int, @Query("month") date: String): Observable<TransactionsListResponse>

    @GET("work")
    fun getMyWork(@Query("page") page: Int, @Query("status") status: String, @Query("q") search: String, @Query("order_by") sort: String): Observable<WorkListResponse>

    @GET("work/{work_id}")
    fun getWorkDetail(@Path("work_id") workId: Int): Observable<WorkResponse>

    @POST("work/{work_id}/cancel")
    fun cancelWork(@Path("work_id") workId: Int): Observable<SuccessResponse>

    @Multipart
    @POST("me/topup")
    fun depositWithTransferSlip(
            @Part("topup_type") depositType: RequestBody,
            @Part("price") price: RequestBody,
            @Part("pay_time") payTime: RequestBody,
            @Part("pay_slip_image\"; filename=\"pay_slip.jpg\" ") slipImage: RequestBody): Observable<SuccessResponse>

    @POST("me/topup")
    @FormUrlEncoded
    fun depositWithOmise(
            @Field("topup_type") depositType: Int,
            @Field("price") price: Int,
            @Field("omise_token") omiseToken: String,
            @Field("from_app") fromApp: Int): Observable<DepositOmiseResponse>

    @POST("topup/{deposit_id}/success")
    fun depositCompletePaymentOmise(@Path("deposit_id") depositId: Int): Observable<SuccessResponse>

}